"""
Django settings for BBPS_Biller project.
Generated by 'django-admin startproject' using Django 3.1.1.
For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/
For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

import os
from datetime import timedelta
from django.conf.global_settings import DEFAULT_FROM_EMAIL
from drf_jwt_2fa.utils import hash_string
from dotenv import dotenv_values

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
envconfig = dotenv_values("enviornment.env")
SECRET_KEY =  envconfig['SECRET_KEY']
DEBUG = envconfig['DEBUG']
MEDIA_URL =  '/uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, "uploads")
ALLOWED_HOSTS = ['150.242.15.48','bbpsdev.rhythm.works','localhost','127.0.0.1']
CORS_ORIGIN_WHITELIST = ['http://localhost:4200','https://bbpsdev.rhythm.works']
END_USER_URL = [envconfig['END_USER_URL']]
OU_AGENT_ID = [envconfig['OU_AGENT_ID']]
OU_AGENT_KEYWORD = [envconfig['OU_AGENT_KEYWORD']]

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'sslserver', 
    'portal', 
    'corsheaders',
    'rest_framework',
    'django_dbconn_retry',
    'rest_framework_simplejwt.token_blacklist'
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
        'portal.api_role_auth.GenericRolePermission',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'drf_jwt_2fa.authentication.Jwt2faAuthentication',
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'NON_FIELD_ERRORS_KEY': 'global',
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(minutes=0),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,
    'UPDATE_LAST_LOGIN': False,

    'ALGORITHM': 'HS256',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUDIENCE': None,
    'ISSUER': None,
    'JWK_URL': None,
    'LEEWAY': 0,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
    'USER_AUTHENTICATION_RULE': 'rest_framework_simplejwt.authentication.default_user_authentication_rule',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',
    'TOKEN_USER_CLASS': 'rest_framework_simplejwt.models.TokenUser',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(minutes=5),
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'BBPS_Biller.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

WSGI_APPLICATION = 'BBPS_Biller.wsgi.application'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': envconfig['DB_NAME'],
        'USER': envconfig['DB_USER'],
        'PASSWORD': envconfig['DB_PASSWORD'],
        'HOST': envconfig['DB_HOST'],
        'PORT': envconfig['DB_PORT']
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'

# JWT settings
JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    'JWT_EXPIRATION_DELTA': timedelta(hours=1),
    'JWT_PAYLOAD_HANDLER': 'portal.custom_jwt.jwt_payload_handler',
    'JWT_AUTH_HEADER_PREFIX': 'JWT',
}

EMAIL_BACKEND ='django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = '587'
EMAIL_HOST_USER = envconfig["DEFAULT_FROM_EMAIL"]
DEFAULT_FROM_EMAIL= envconfig["DEFAULT_FROM_EMAIL"]
EMAIL_HOST_PASSWORD = envconfig["EMAIL_HOST_PASSWORD"]
EMAIL_USE_TLS = True
EMAIL_USE__SSL = False

JWT2FA_AUTH = {
    'CODE_LENGTH': 7,
    'CODE_CHARACTERS': '0123456789',
    'CODE_TOKEN_SECRET_KEY': hash_string('2fa-code-' + SECRET_KEY),
    'CODE_EXTENSION_SECRET': hash_string('2fa-ext-' + SECRET_KEY),
    'CODE_EXPIRATION_TIME': timedelta(minutes=5),
    'CODE_TOKEN_THROTTLE_RATE': '12/3h',
    'AUTH_TOKEN_RETRY_WAIT_TIME': timedelta(seconds=2),
    'CODE_SENDER': 'drf_jwt_2fa.sending.send_verification_code_via_email',
    'EMAIL_SENDER_FROM_ADDRESS': DEFAULT_FROM_EMAIL,
    'EMAIL_SENDER_SUBJECT_OVERRIDE': None,
    'EMAIL_SENDER_BODY_OVERRIDE': None,
}