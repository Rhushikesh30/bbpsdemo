from .db_postgresql.connection import Connection
import requests

class CheckStatus:
    def __init__(self) -> None:
        self.connection_obj = None
        self.token = None
        self.token_expiry = None

    def get_access_token(self, token_request_ref:str, access_token_url:str, client_id:str, client_secret:str):
        headers = {'Content-Type': 'application/json'}
        data = {
            'clientID': client_id,
            'secret': client_secret
            }
        
        response = requests.request(token_request_ref, access_token_url, headers=headers, json=data)
        response.raise_for_status()            
        token_data = response.json()
        self.token_data = token_data['data']['token']

    def execute_query(self,query:str,returning:bool,commit:bool)->any:
        try:
            result=None
            self.connection_obj=self.con.connection()
            cursor=self.connection_obj.cursor()
            cursor.execute(query)
            if returning:
                result=cursor.fetchall()
            if  commit:    
                self.connection_obj.commit()
            self.con.release_connection(self.connection_obj)
            return result
        except:
            if commit:
                self.connection_obj.rollback()
        finally:
            cursor.close()
    
    def paymentstatus(self) -> None:
        try:
            self.con = Connection("DEFAULT", 1, 10)
            access_token = ''
            paidTransaction = "SELECT utr_number, biller_ref_id_id FROM portal_tbl_bbps_transaction_mst WHERE payment_status = 'Paid'"
            paidTransaction = self.execute_query(paidTransaction, True, False)
            utr_number = [item[0] for item in paidTransaction]
            biller_ids = [item[1] for item in paidTransaction]

            for index, transaction in enumerate(utr_number):
                api_definition = f"SELECT token_request_ref_id_id, access_token_url, agent_id, agent_keyword, end_point_request_ref_id_id, end_point_url FROM portal_tbl_api_definition_mst WHERE tenant_id_id = '{biller_ids[index]}' AND is_active='True' AND is_deleted ='False' AND revision_status = 'Effective' "
                api_definition = self.execute_query(api_definition, True, False)   
                if api_definition:
                    token_request_ref = api_definition[0][0]
                    token_request_ref = f"SELECT master_key FROM portal_tbl_master WHERE id = '{token_request_ref}' "
                    token_request_ref = self.execute_query(token_request_ref, True, False)
                    token_request_ref = token_request_ref[0][0]
                    access_token_url = api_definition[0][1]
                    client_id = api_definition[0][2]
                    client_secret = api_definition[0][3]
                    
                    end_point_request_ref = api_definition[0][4]
                    end_point_request_ref = f"SELECT master_key FROM portal_tbl_master WHERE id = '{end_point_request_ref}' "
                    end_point_request_ref = self.execute_query(end_point_request_ref, True, False)
                    end_point_request_ref = end_point_request_ref[0][0]
                    end_point_url = api_definition[0][5]
                
                    try:
                        headers = {'Authorization': f'Bearer {access_token} ', 'Content-Type': 'application/json'}
                        data = {"txnReferenceId": transaction}
                        response = requests.request(end_point_request_ref, end_point_url, headers=headers, json=data)
                        if response.status_code==404:
                            pass
                        else:
                            actual_api_response = response.json()['data']
                    except:
                        self.get_access_token(token_request_ref, access_token_url, client_id, client_secret)           
                        access_token = self.token_data                    
                        headers = {'Authorization': f'Bearer {access_token} ', 'Content-Type': 'application/json'}
                        data = {"txnReferenceId": transaction}
                        response = requests.request(end_point_request_ref, end_point_url, headers=headers, json=data)
                        if response.status_code==404:
                            pass
                        else:
                            actual_api_response = response.json()['data']

                    if response.status_code==200:
                        paymentStatus = actual_api_response['payment']['status']
                        if paymentStatus == 'SUCCESSFUL':
                            paymentMode = actual_api_response['payment']['paymentMethod']['paymentMode']
                            paymentMode = f"SELECT id FROM public.portal_tbl_master WHERE master_key = '{paymentMode}'"
                            paymentMode = self.execute_query(paymentMode, True, False)
                            paymentMode = tuple(paymentMode)[0][0]
                            success = f"UPDATE public.portal_tbl_bbps_transaction_mst SET payment_mode_ref_id_id = '{paymentMode}', payment_status = '{paymentStatus}' WHERE utr_number = '{transaction}'"
                            success = self.execute_query(success, False, True)

                        elif paymentStatus == 'FAILED':
                            success = f"UPDATE public.portal_tbl_bbps_transaction_mst SET payment_status = '{paymentStatus}' WHERE utr_number = '{transaction}'"
                            success = self.execute_query(success, False, True)
                        elif paymentStatus == 'PROCESSING PENDING':
                            pass
                else:
                    pass
        except:
            print('Exception:')