from dotenv import dotenv_values 
import os
base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#get values from env file
class EnvGetter:
    env_info_dict={}
    @classmethod
    def get(cls,filename) -> dict:
        if filename not in cls.env_info_dict:
            cred_dict=dotenv_values(base_dir+'/'+filename+'.env')
            if not cred_dict:
                raise ValueError(cred_dict)
            cls.env_info_dict.update({filename:cred_dict})
        return cls.env_info_dict.get(filename)  