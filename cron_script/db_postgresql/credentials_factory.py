from .credentials_list import db_name,db_serializer_dict
class CredntailsFactory:
    
    def __init__(self):
        self._builders = {}
        
    def get(self, key):
        builder = self._builders.get(key)
        if not builder:
            if key in db_name:
                self._builders.update({key:db_serializer_dict.get(key)})
                builder= self._builders.get(key)
            else:
                raise ValueError(key)
        return builder