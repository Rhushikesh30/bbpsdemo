from .env_adapter import EnvGetter

class DefaultSerializer:
    
    @classmethod
    def get(cls) -> dict:
        return_dict={}
        cred_dict=EnvGetter.get('enviornment')
        return_dict.update({'user':cred_dict.get('DB_USER'),'password':cred_dict.get('DB_PASSWORD'),'host':cred_dict.get('DB_HOST'),'port':cred_dict.get('DB_PORT'),'database':cred_dict.get('DB_NAME')})
        return return_dict