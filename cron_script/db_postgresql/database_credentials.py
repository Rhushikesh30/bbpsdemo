
import os
from .credentials_factory import CredntailsFactory
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class DataBaseCredentials:
    def __init__(self,database_name) -> None:
        self.credntailsfactory=CredntailsFactory()
        self.cred_dict={}
        self.database_name=database_name 
    def get_credentials(self)->dict:
           return self.credntailsfactory.get(self.database_name)
    