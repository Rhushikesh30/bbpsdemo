from psycopg2.pool import SimpleConnectionPool
from .database_credentials import DataBaseCredentials
class Connection:
    
    def __init__(self,database_name,min_connection_limit,max_connnection_limit) -> None:
        self.credential_dict=dict()
        db_credentials=DataBaseCredentials(database_name)
        self.credential_dict=db_credentials.get_credentials()
        if not  self.credential_dict:
            raise ValueError(self.credential_dict)
        self.postgresql_pool=SimpleConnectionPool(min_connection_limit,max_connnection_limit,**self.credential_dict)
        if (self.postgresql_pool):
            print('connection pulling') 
                      
    def connection(self)-> any:
        return self.postgresql_pool.getconn()
    
    def release_connection(self,connection)-> None:
        print('deleted')
        self.postgresql_pool.putconn(connection)
        
    def close_pool(self)-> None:
            print('closed')
            self.postgresql_pool.closeall()
            