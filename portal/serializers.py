from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.fields import SerializerMethodField
from datetime import datetime, timedelta
from BBPS_Biller.settings import EMAIL_HOST_USER, END_USER_URL
from django.core.mail import send_mail
from django.contrib.auth.hashers import make_password
from django.db.models import F
from django.db import transaction
from django.db import IntegrityError

class LoginMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_login_mst
        fields  = '__all__'

    def get(user):
        return tbl_login_mst.objects.get(user = user)

class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)   #may be required
    class Meta:
        model = User
        fields = '__all__'

    def get(username):
        return User.objects.get(username = username)

class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_role_mst
        fields = '__all__'

    def get(role):
        return tbl_role_mst.objects.get(id = role.id)

class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_channel_mst
        fields = '__all__'

class tbl_company_contact_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_company_contact_details
        fields = '__all__'

class tbl_company_kyc_document_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_kyc_document_details
        fields = '__all__'

class getCompanySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    service_mode = serializers.CharField(source='service_model_ref_id.master_key',read_only=True)
    billing_mode = serializers.CharField(source='billing_mode_ref_id.master_key',read_only=True)
    onboarding_mode = serializers.CharField(source='onboarding_ref_id.master_key',read_only=True)
    settlement_mode = serializers.CharField(source='settlement_type_ref_id.master_key',read_only=True)
    biller_mode = serializers.CharField(source='biller_category_ref_id.cat_name',read_only=True)
    bbpou_bank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name',read_only=True)

    class Meta:
        model = tbl_company_mst
        fields = ['id','company_name','company_shortname','biller_category_ref_id','service_model_ref_id','billing_mode_ref_id','service_mode','billing_mode','onboarding_mode','onboarding_ref_id','settlement_type_ref_id','settlement_mode','biller_mode','bbpou_bank_ref_id','bbpou_bank_name']

class TenentNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_company_mst
        fields = ['id','company_name','biller_category_ref_id','company_shortname']

class CompanySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
    ownership_status = serializers.CharField(source='ownership_status_ref_id.master_key', read_only=True)
    initialItemRow = tbl_company_contact_details_serializer(many=True)
    company_type = serializers.CharField(source='company_type_ref_id.role_name', read_only=True)
    location = serializers.CharField(source='location_ref_id.location_name', read_only=True)
    country = serializers.CharField(source='country_ref_id.name', read_only=True)
    state = serializers.CharField(source='state_ref_id.state', read_only=True)
    city = serializers.CharField(source='city_ref_id.city_name', read_only=True)
    bbpou_bank = serializers.CharField(source = 'bbpou_bank_ref_id.bbpou_bank_name',read_only = True)

    class Meta:
        model = tbl_company_mst
        fields = '__all__'

    def create(self, validated_data):
        try:
            with transaction.atomic():
                parent_company_id = self.context['request'].data.pop('parent_company_id')
                company_role = self.context['request'].data.pop('company_role')
                initialItemRow = validated_data.pop('initialItemRow')
                role_data = tbl_role_mst.objects.get(role_name=company_role,is_deleted='N',is_active='Y')
                companyMaster = tbl_company_mst.objects.create(**validated_data, company_type_ref_id=role_data)
                tbl_entity_relationship_mst.objects.create(entity_ref_id = companyMaster, company_ref_id = companyMaster)
                entityRefId = tbl_entity_relationship_mst.objects.filter( company_ref_id = parent_company_id )
                for item in entityRefId:
                    tbl_entity_relationship_mst.objects.create(entity_ref_id = item.entity_ref_id, company_ref_id=companyMaster)

                # if company_role != 'Aggregator Partner':
                company_shortname = self.context['request'].data.pop('company_shortname')
                email = self.context['request'].data.pop('email')
                application_id = self.context['request'].data.pop('application_id')
                password = make_password('bbpsadmin@123', hasher='default')
                userAuth = User.objects.create(first_name = company_shortname, last_name=company_shortname, username=email,is_superuser=True,is_staff=False,is_active = True, password=password ,email=email)
                tbl_login_mst.objects.create(user = userAuth,role_ref_id=role_data,company_id = companyMaster, application_id=application_id)
                trum = tbl_assign_role_user_mst.objects.create(assigned_to_user = userAuth, company_ref_id=companyMaster, application_id=application_id)
                tbl_assign_role_user_details.objects.create(header_ref_id = trum,assigned_to_role_ref_id = role_data,application_id=application_id)

                subject = 'Welcome to BBPS Rhythmflows'
                message = 'Your login credentials are as follows' + '\n\n' + 'USERNAME: ' + email + '\n' + 'PASSWORD: ' + "bbpsadmin@123"
                recipient = email
                send_mail(subject, message, EMAIL_HOST_USER, [recipient], fail_silently = False)

                for item in initialItemRow:
                    tbl_company_contact_details.objects.create(**item, header_ref_id=companyMaster)
                return companyMaster

        except IntegrityError as e:
            raise serializers.ValidationError({'IntegrityError':e.__cause__})

    def update(self, instance, validated_data):
        try:
            with transaction.atomic():
                object = tbl_company_mst.objects.get(id=validated_data['id'])
                initialItemRow = validated_data.pop('initialItemRow')

                instance.company_id = validated_data.get('company_id', instance.company_id)
                instance.share_id = validated_data.get('share_id', instance.share_id)
                instance.entity_share_id = validated_data.get('entity_share_id', instance.entity_share_id)
                instance.company_name = validated_data.get('company_name', instance.company_name)
                instance.company_shortname = validated_data.get('company_shortname', instance.company_shortname)
                instance.address1 = validated_data.get('address1', instance.address1)
                instance.address2 = validated_data.get('address2', instance.address2)
                instance.address3 = validated_data.get('address3', instance.address3)
                instance.country_ref_id = validated_data.get('country_ref_id', instance.country_ref_id)
                instance.state_ref_id = validated_data.get('state_ref_id', instance.state_ref_id)
                instance.city_ref_id = validated_data.get('city_ref_id', instance.city_ref_id)
                instance.location_ref_id = validated_data.get('location_ref_id', instance.location_ref_id)
                instance.company_type_ref_id = validated_data.get('company_type_ref_id', instance.company_type_ref_id)
                instance.ownership_status_ref_id = validated_data.get('ownership_status_ref_id', instance.ownership_status_ref_id)
                instance.cin = validated_data.get('cin', instance.cin)
                instance.pan = validated_data.get('pan', instance.pan)
                instance.tan = validated_data.get('tan', instance.tan)
                instance.gst = validated_data.get('gst', instance.gst)
                instance.email = validated_data.get('email', instance.email)
                instance.adhar = validated_data.get('adhar', instance.adhar)
                instance.contact_person_name = validated_data.get('contact_person_name', instance.contact_person_name)
                instance.is_holding_company = validated_data.get('is_holding_company', instance.is_holding_company)
                instance.belongs_to_company_id = validated_data.get('belongs_to_company_id', instance.belongs_to_company_id)
                instance.is_this_under_same_management = validated_data.get('is_this_under_same_management', instance.is_this_under_same_management)
                instance.management_belongs_to_company_id = validated_data.get('management_belongs_to_company_id', instance.management_belongs_to_company_id)
                instance.percentage_holding = validated_data.get('percentage_holding', instance.percentage_holding)
                instance.is_group_company = validated_data.get('is_group_company', instance.is_group_company)
                instance.contact_person_mobile_number = validated_data.get('contact_person_mobile_number', instance.contact_person_mobile_number)
                instance.is_this_branch = validated_data.get('is_this_branch', instance.is_this_branch)
                instance.pincode = validated_data.get('pincode', instance.pincode)
                instance.revision_status = validated_data.get('revision_status', instance.revision_status)
                instance.attach_document = validated_data.get('attach_document', instance.attach_document)
                instance.bank_name = validated_data.get('bank_name', instance.bank_name)
                instance.ifsc_code = validated_data.get('ifsc_code', instance.ifsc_code)
                instance.verification_comments = validated_data.get('verification_comments', instance.verification_comments)

                instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
                instance.application_id = validated_data.get('application_id', instance.application_id)
                instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
                instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
                instance.updated_by = validated_data.get('updated_by', instance.updated_by)

                instance.save()
                keep_details = []

                for init in initialItemRow:
                    if "id" in init.keys():
                        if tbl_company_contact_details.objects.filter(id=init['id']).exists():
                            det = tbl_company_contact_details.objects.get(id=init['id'])
                            det.contact_person_name = init.get('contact_person_name',det.contact_person_name)
                            det.designation = init.get('designation',det.designation)
                            det.contact_person_mobile_number = init.get('contact_person_mobile_number',det.contact_person_mobile_number)
                            det.email = init.get('email',det.email)
                            det.created_date_time = init.get('created_date_time',det.created_date_time)
                            det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                            det.application_id = init.get('application_id',det.application_id)
                            det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                            det.updated_by = init.get('updated_by',det.updated_by)

                            det.save()
                            keep_details.append(det.id)
                        else:
                            continue
                    else:
                        det = tbl_company_contact_details.objects.create(**init,header_ref_id=instance)
                        keep_details.append(det.id)

                det = tbl_company_contact_details.objects.filter(header_ref_id=object.id)
                det_id = [d.id for d in det]

                for d in det_id:
                    if d in keep_details:
                        continue
                    else:
                        det_record = tbl_company_contact_details.objects.get(id=d)
                        det_record.is_deleted = 'Y'
                        det_record.save()

                return instance

        except IntegrityError as e:
            raise serializers.ValidationError({'IntegrityError':e.__cause__})

class MasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_master
        fields = '__all__'

class tbl_role_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_role_mst
        fields = '__all__'

    def get(self, request):
        return tbl_role_mst.objects.filter(is_deleted='N').filter(application_id=request.GET['application_id'])

class LocationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='country_id.name', read_only=True)
    state = serializers.CharField(source='state_id.state', read_only=True)
    city_name = serializers.CharField(source='city_id.city_name', read_only=True)
    currency = serializers.CharField(source='country_id.currency_code', read_only=True)
    class Meta:
        model = tbl_location_mst
        fields = '__all__'

class CitySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_city_mst
        fields = '__all__'

class StateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_state_mst
        fields = '__all__'

class CountryCurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model=tbl_country_currency_mst
        fields='__all__'

class UOMSerializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_uom_mst
        fields = '__all__'

class tbl_reason_mst_serializers(serializers.ModelSerializer):
    class Meta:
        model = tbl_reason_code_mst
        fields = '__all__'

class EmployeeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False,allow_null=True)
    class Meta:
        model = tbl_employee_mst
        fields = '__all__'

    def create(self, validated_data):
        try:
            with transaction.atomic():
                user_role = self.context['request'].data.pop('user_role')
                role_data = tbl_role_mst.objects.get(role_name=user_role, is_deleted='N', is_active='Y')
                employeeMaster = tbl_employee_mst.objects.create(**validated_data)
                emp_name = self.context['request'].data.pop('emp_name')
                email = self.context['request'].data.pop('email')
                application_id = self.context['request'].data.pop('application_id')

                userPassword = f'bbps{user_role.replace(" ", "")}@321'
                password = make_password(userPassword, hasher='default')
                userAuth = User.objects.create(first_name = emp_name, last_name=emp_name, username=email, is_superuser=False,is_staff=True, is_active=True, password=password ,email=email)
                tbl_login_mst.objects.create(user = userAuth, role_ref_id=role_data, employee_ref_id=employeeMaster, company_id=employeeMaster.company_ref_id, application_id=application_id)
                trum = tbl_assign_role_user_mst.objects.create(assigned_to_user = userAuth, company_ref_id=employeeMaster.company_ref_id, application_id=application_id)
                tbl_assign_role_user_details.objects.create(header_ref_id = trum, assigned_to_role_ref_id = role_data, application_id=application_id)
                company_name = tbl_company_mst.objects.filter(id=employeeMaster.company_ref_id_id, is_deleted='N', is_active='Y').values_list('company_name', flat=True).first()

                subject = f'Welcome to BBPS-{user_role} Rhythmflows'
                user_pass = f'\n   Username:- {email} \n   Password:- {userPassword}'
                server_url = f'\n   Url:- {END_USER_URL[0]}'
                no_reply = 'This is an auto generated email please do not reply to this e-mail'
                message = f'Company Name:- {company_name}\nYour login credentials are as follows. {user_pass}{server_url} \n\n{no_reply}'
                recipient = email
                send_mail(subject, message, EMAIL_HOST_USER, [recipient], fail_silently = False)                
                return employeeMaster
                
        except IntegrityError as e:
            raise serializers.ValidationError({'IntegrityError':e.__cause__})

class ActivitySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_workflow_activity_mst
        fields = '__all__'

class tbl_workflow_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_workflow_mst
        fields = '__all__'

class ApiDefinitionDetailsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_api_definition_details
        fields = '__all__'

class ApiDefinitionSubDetailsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_api_definition_sub_details
        fields = '__all__'

class tbl_dynamic_table_fields_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_dynamic_table_fields_mst
        fields = '__all__'

class DefineApisMasterSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    company_name = serializers.CharField(source='tenant_id.company_name', read_only=True)
    api_definition_details = ApiDefinitionDetailsSerializer(many=True)
    api_definition_sub_details = ApiDefinitionSubDetailsSerializer(many=True)

    class Meta:
        model = tbl_api_definition_mst
        fields = '__all__'

    def validate(self, data):
        if self.instance==None: 
            if tbl_api_definition_mst.objects.filter(tenant_id=data['tenant_id'], is_active=True, is_deleted=False).exists():
                raise serializers.ValidationError("Biller Already Exists")
        return data 

    def create(self, validated_data):
        api_definition_details = validated_data.pop('api_definition_details')
        api_definition_sub_details = validated_data.pop('api_definition_sub_details')

        api_definition = tbl_api_definition_mst.objects.create(**validated_data)
        for item in api_definition_details:
            tbl_api_definition_details.objects.create(**item, header_ref_id=api_definition)

        for item in api_definition_sub_details:
            tbl_api_definition_sub_details.objects.create(**item, header_ref_id=api_definition)

        return api_definition

    def update(self, instance, validated_data):
        api_definition_details = validated_data.pop('api_definition_details')
        api_definition_sub_details = validated_data.pop('api_definition_sub_details')

        change_to_date = datetime.strptime(str(validated_data['from_date']),'%Y-%m-%d')
        final_to_date =(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
        instance.to_date = final_to_date
        instance.revision_status = 'History'
        instance.save()

        validated_data.pop('id')
        api_definition = tbl_api_definition_mst.objects.create(**validated_data)

        for item in api_definition_details:
            tbl_api_definition_details.objects.create(**item, header_ref_id=api_definition)
        for item1 in api_definition_sub_details:
            tbl_api_definition_sub_details.objects.create(**item1, header_ref_id=api_definition)
        return instance

# Standard API

class tbl_api_definition_standard_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_api_definition_standard_details
        fields = '__all__'

class tbl_api_definition_standard_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    company_name = serializers.CharField(source='company_ref_id.company_name', read_only=True)
    channel_name = serializers.CharField(source='channel_ref_id.channel_name', read_only=True)
    initialItemRow = tbl_api_definition_standard_details_serializer(many=True)

    class Meta:
        model = tbl_api_definition_standard_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        standardApi = tbl_api_definition_standard_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_api_definition_standard_details.objects.create(**item, header_ref_id=standardApi)
        return standardApi

    def update(self, instance, validated_data):
        object = tbl_api_definition_standard_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        if instance.revision_status == 'Effective':
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')
            standardApi = tbl_api_definition_standard_mst.objects.create(**validated_data)

            for item in initialItemRow:
                tbl_api_definition_standard_details.objects.create(**item, header_ref_id=standardApi)
            return instance

        else:
            instance.company_ref_id = validated_data.get('company_ref_id', instance.company_ref_id)
            instance.channel_ref_id = validated_data.get('channel_ref_id', instance.channel_ref_id)
            instance.from_date = validated_data.get('from_date', instance.from_date)
            instance.to_date = validated_data.get('to_date', instance.to_date)
            instance.to_date = validated_data.get('to_date', instance.to_date)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)

            instance.save()
            keep_details = []

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_api_definition_standard_details.objects.filter(id=init['id']).exists():
                        det = tbl_api_definition_standard_details.objects.get(id=init['id'])
                        det.request_type_ref_id = init.get('request_type_ref_id',det.request_type_ref_id)
                        det.api_type_ref_id = init.get('api_type_ref_id',det.api_type_ref_id)
                        det.protocol_type_ref_id = init.get('protocol_type_ref_id',det.protocol_type_ref_id)
                        det.file_format_type_ref_id = init.get('file_format_type_ref_id',det.file_format_type_ref_id)
                        det.authentication_type_ref_id = init.get('authentication_type_ref_id',det.authentication_type_ref_id)
                        det.communication_ref_id = init.get('communication_ref_id',det.communication_ref_id)
                        det.session_key_encryption_type_ref_id = init.get('session_key_encryption_type_ref_id',det.session_key_encryption_type_ref_id)
                        det.encryption_type_ref_id = init.get('encryption_type_ref_id',det.encryption_type_ref_id)
                        det.port = init.get('port',det.port)
                        det.verification_algorithm_ref_id = init.get('verification_algorithm_ref_id',det.verification_algorithm_ref_id)
                        det.created_date_time = init.get('created_date_time',det.created_date_time)
                        det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)

                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_api_definition_standard_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det = tbl_api_definition_standard_details.objects.filter(header_ref_id=object.id)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    continue
                else:
                    det_record = tbl_api_definition_standard_details.objects.get(id=d)
                    det_record.is_deleted = 'Y'
                    det_record.is_active = 'N'
                    det_record.save()

            return instance

# Define product
class tbl_product_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    stream_name = serializers.CharField(source='stream_ref_id.stream_name', read_only=True)
    channel_name = serializers.CharField(source='channel_ref_id.channel_name', read_only=True)
    labels = serializers.SerializerMethodField()
    class Meta:
        model = tbl_product_mst
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(tbl_product_mst_serializer, self).__init__(*args, **kwargs)

        # if 'labels' in self.fields:
        #     raise RuntimeError(
        #         'You cant have labels field defined '
        #         'while using MyModelSerializer'
        #     )

        self.fields['labels'] = SerializerMethodField()

    def get_labels(self, *args):
        labels = {}
        for field in self.Meta.model._meta.get_fields():
            if field.name in self.fields:
                labels[field.name] = field.verbose_name
                # print("verbose_name")
                # print(field.verbose_name)
        return labels
        # return JsonResponse(labels)

# Nas-digital-Dev
# Define Channel
class tbl_channel_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    product = tbl_product_mst_serializer(many=True, read_only=True)
    stream_name = serializers.CharField(source='stream_ref_id.stream_name', read_only=True)
    class Meta:
        model = tbl_channel_mst
        fields = '__all__'

#Define Stream
class tbl_stream_mst_serializer(serializers.ModelSerializer):
    streams = tbl_channel_mst_serializer(many=True, read_only=True)
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_stream_mst
        fields = '__all__'

# Link BBPOU Bank with Biller

class tbl_biller_bank_link_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_biller_bank_link_details
        fields = '__all__'

class tbl_biller_bank_link_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    bbpou_bank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True)
    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    initialItemRow = tbl_biller_bank_link_details_serializer(many=True)

    class Meta:
        model = tbl_biller_bank_link_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        biller_bank = tbl_biller_bank_link_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_biller_bank_link_details.objects.create(**item, header_ref_id=biller_bank)
        return biller_bank

    def update(self, instance, validated_data):
        object = tbl_biller_bank_link_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')
        is_check = tbl_biller_bank_link_mst.objects.filter(id=validated_data['id'],revision_status='Effective',is_deleted='N', is_active='Y').first()

        if validated_data['revision_status'] == 'Future' and is_check:
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')

            source = tbl_biller_bank_link_mst.objects.create(**validated_data)
            for item in initialItemRow:
                if item.get('id'):
                    del item['id']
                tbl_biller_bank_link_details.objects.create(**item,header_ref_id=source)
            return instance
        else:
            instance.biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            instance.start_date = validated_data.get('start_date', instance.start_date)
            instance.end_date = validated_data.get('end_date', instance.end_date)
            instance.revision_status = validated_data.get('revision_status', instance.revision_status)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
            instance.created_by = validated_data.get('created_by', instance.created_by)
            instance.updated_by = validated_data.get('updated_by', instance.updated_by)

            instance.save()
            keep_details = []

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_biller_bank_link_details.objects.filter(id=init['id']).exists():
                        det = tbl_biller_bank_link_details.objects.get(id=init['id'])
                        det.bank_ref_id = init.get('bank_ref_id',det.bank_ref_id)
                        det.percentage_share = init.get('percentage_share',det.percentage_share)
                        det.created_date_time = init.get('created_date_time',det.created_date_time)
                        det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                        det.created_by = init.get('created_by',det.created_by)
                        det.updated_by = init.get('updated_by',det.updated_by)
                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_biller_bank_link_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det = tbl_biller_bank_link_details.objects.filter(header_ref_id=object.id)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    continue
                else:
                    det_record = tbl_biller_bank_link_details.objects.get(id=d)
                    det_record.is_deleted = 'Y'
                    det_record.save()

            return instance

#Bank Master
class tbl_bank_contact_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_bank_contact_details
        fields = '__all__'

class BankSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    short_name = serializers.CharField(source='country_ref_id.short_name', read_only=True)
    state = serializers.CharField(source='state_ref_id.state', read_only=True)
    city_name = serializers.CharField(source='city_ref_id.city_name', read_only=True)
    master_key = serializers.CharField(source='account_type.master_key', read_only=True)
    initialItemRow = tbl_bank_contact_details_serializer(many=True)

    class Meta:
        model = tbl_bank_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')

        nodal_acc_flag = validated_data.get('nodal_acc_flag', False)
        if nodal_acc_flag == True:
            nodal_acc_flag = tbl_bank_mst.objects.filter(nodal_acc_flag = True).filter(is_deleted = 'N', is_active = 'Y')
            if nodal_acc_flag:
                validated_data['nodal_acc_flag'] = False
                
        bankMaster = tbl_bank_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_bank_contact_details.objects.create(**item, header_ref_id=bankMaster)
        return bankMaster

    def update(self, instance, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        nodal_acc_flag = validated_data.get('nodal_acc_flag', False)
        if nodal_acc_flag == True:
            nodal_acc_flag = tbl_bank_mst.objects.filter(nodal_acc_flag = True).filter(is_deleted = 'N', is_active = 'Y')
            if nodal_acc_flag:
                validated_data['nodal_acc_flag'] = False

        instance.bank_name = validated_data.get('bank_name', instance.bank_name)
        instance.branch_name = validated_data.get('branch_name', instance.branch_name)
        instance.account_number = validated_data.get('account_number', instance.account_number)
        instance.account_type = validated_data.get('account_type', instance.account_type)
        instance.bank_code = validated_data.get('bank_code', instance.bank_code)
        instance.swift_number = validated_data.get('swift_number', instance.swift_number)
        instance.iban_number = validated_data.get('iban_number', instance.iban_number)
        instance.ifsc_code = validated_data.get('ifsc_code', instance.ifsc_code)
        instance.cash_credit_limit = validated_data.get('cash_credit_limit', instance.cash_credit_limit)
        instance.address1 = validated_data.get('address1', instance.address1)
        instance.address2 = validated_data.get('address2', instance.address2)
        instance.address3 = validated_data.get('address3', instance.address3)
        instance.country_ref_id = validated_data.get('country_ref_id', instance.country_ref_id)
        instance.state_ref_id = validated_data.get('state_ref_id', instance.state_ref_id)
        instance.city_ref_id = validated_data.get('city_ref_id', instance.city_ref_id)
        instance.contact_person_mobile_number = validated_data.get('contact_person_mobile_number', instance.contact_person_mobile_number)
        instance.pincode = validated_data.get('pincode', instance.pincode)
        instance.company_ref_id = validated_data.get('company_ref_id', instance.company_ref_id)
        instance.nodal_acc_flag = validated_data.get('nodal_acc_flag', instance.nodal_acc_flag)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.created_by = validated_data.get('created_by', instance.created_by)
        instance.updated_by = validated_data.get('updated_by', instance.updated_by)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)

        instance.save()
        keep_details = []

        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_bank_contact_details.objects.filter(id=init['id']).exists():
                    det = tbl_bank_contact_details.objects.get(id=init['id'])
                    det.contact_person_name = init.get('contact_person_name',det.contact_person_name)
                    det.designation = init.get('designation',det.designation)
                    det.contact_person_mobile_number = init.get('contact_person_mobile_number',det.contact_person_mobile_number)
                    det.email = init.get('email',det.email)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.created_by = init.get('created_by',det.created_by)
                    det.updated_by = init.get('updated_by',det.updated_by)
                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_bank_contact_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_bank_contact_details.objects.filter(header_ref_id=instance)
        det_id = [d.id for d in det]
        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_bank_contact_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.is_active = 'N'
                det_record.save()
        return instance

# Add BBPOU Bank

class tbl_bbpou_bank_contact_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_bbpou_bank_contact_details
        fields = '__all__'

class tbl_bbpou_bank_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    country_name =  serializers.CharField(source='country_ref_id.name', read_only=True)
    state_name = serializers.CharField(source='state_ref_id.state', read_only=True)
    city_name = serializers.CharField(source='city_ref_id.city_name', read_only=True)
    onboarding_status = serializers.CharField(source='onboarding_status_ref_id.master_key', read_only=True)
    initialItemRow = tbl_bbpou_bank_contact_details_serializer(many=True)

    class Meta:
        model = tbl_bbpou_bank_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        bbpoubank = tbl_bbpou_bank_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_bbpou_bank_contact_details.objects.create(**item, header_ref_id=bbpoubank)
        return bbpoubank

    def update(self, instance, validated_data):
        object = tbl_bbpou_bank_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.share_id = validated_data.get('share_id', instance.share_id)
        instance.bbpou_bank_name = validated_data.get('bbpou_bank_name', instance.bbpou_bank_name)
        instance.npci_biller_bbpou_id = validated_data.get('npci_biller_bbpou_id', instance.npci_biller_bbpou_id)
        instance.branch_name = validated_data.get('branch_name', instance.branch_name)
        instance.membership_id = validated_data.get('membership_id', instance.membership_id)
        instance.onboarding_status_ref_id = validated_data.get('onboarding_status_ref_id', instance.onboarding_status_ref_id)
        instance.bank_code = validated_data.get('bank_code', instance.bank_code)
        instance.pan = validated_data.get('pan', instance.pan)
        instance.tan = validated_data.get('tan', instance.tan)
        instance.gst = validated_data.get('gst', instance.gst)
        instance.email = validated_data.get('email', instance.email)
        instance.contact_person_name = validated_data.get('contact_person_name', instance.contact_person_name)
        instance.address1 = validated_data.get('address1', instance.address1)
        instance.address2 = validated_data.get('address2', instance.address2)
        instance.address3 = validated_data.get('address3', instance.address3)
        instance.country_ref_id = validated_data.get('country_ref_id', instance.country_ref_id)
        instance.state_ref_id = validated_data.get('state_ref_id', instance.state_ref_id)
        instance.city_ref_id = validated_data.get('city_ref_id', instance.city_ref_id)
        instance.contact_person_mobile_number = validated_data.get('contact_person_mobile_number', instance.contact_person_mobile_number)
        instance.pincode = validated_data.get('pincode', instance.pincode)
        instance.swift_code = validated_data.get('swift_code', instance.swift_code)
        instance.ifsc_code = validated_data.get('ifsc_code', instance.ifsc_code)
        instance.iban_code = validated_data.get('iban_code', instance.iban_code)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.updated_by = validated_data.get('updated_by', instance.updated_by)

        instance.save()
        keep_details = []

        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_bbpou_bank_contact_details.objects.filter(id=init['id']).exists():
                    det = tbl_bbpou_bank_contact_details.objects.get(id=init['id'])
                    det.contact_person_name = init.get('contact_person_name',det.contact_person_name)
                    det.designation = init.get('designation',det.designation)
                    det.contact_person_mobile_number = init.get('contact_person_mobile_number',det.contact_person_mobile_number)
                    det.email = init.get('email',det.email)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.updated_by = init.get('updated_by',det.updated_by)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_bbpou_bank_contact_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_bbpou_bank_contact_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_bbpou_bank_contact_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()

        return instance

# Network member Master
class tbl_entity_relationship_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    company_name = serializers.CharField(source='company_ref_id.company_name', read_only=True)

    class Meta:
        model = tbl_entity_relationship_mst
        fields = '__all__'

class tbl_company_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    company_type = serializers.CharField(source='company_type_ref_id.role_name', read_only=True)
    ownership_status = serializers.CharField(source='ownership_status_ref_id.master_key', read_only=True)
    location = serializers.CharField(source='location_ref_id.location_name', read_only=True)
    country = serializers.CharField(source='country_ref_id.name', read_only=True)
    state = serializers.CharField(source='state_ref_id.state', read_only=True)
    city = serializers.CharField(source='city_ref_id.city_name', read_only=True)

    class Meta:
        model = tbl_company_mst
        fields = '__all__'

    def get(company):
        return tbl_company_mst.objects.get(pk = company)

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        compannymst = tbl_company_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_company_contact_details.objects.create(**item, header_ref_id=compannymst)
        return compannymst



    def update(self, instance, validated_data):
        object = tbl_company_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.company_id = validated_data.get('company_id', instance.company_id)
        instance.share_id = validated_data.get('share_id', instance.share_id)
        instance.entity_share_id = validated_data.get('entity_share_id', instance.entity_share_id)
        instance.company_name = validated_data.get('company_name', instance.company_name)
        instance.company_shortname = validated_data.get('company_shortname', instance.company_shortname)
        instance.npci_assigned_id = validated_data.get('npci_assigned_id', instance.npci_assigned_id)
        instance.address1 = validated_data.get('address1', instance.address1)
        instance.address2 = validated_data.get('address2', instance.address2)
        instance.address3 = validated_data.get('address3', instance.address3)
        instance.country_ref_id = validated_data.get('country_ref_id', instance.country_ref_id)
        instance.state_ref_id = validated_data.get('state_ref_id', instance.state_ref_id)
        instance.city_ref_id = validated_data.get('city_ref_id', instance.city_ref_id)
        instance.location_ref_id = validated_data.get('location_ref_id', instance.location_ref_id)
        instance.company_type_ref_id = validated_data.get('company_type_ref_id', instance.company_type_ref_id)
        instance.ownership_status_ref_id = validated_data.get('ownership_status_ref_id', instance.ownership_status_ref_id)
        instance.comms_address1 = validated_data.get('comms_address1', instance.comms_address1)
        instance.comms_address2 = validated_data.get('comms_address2', instance.comms_address2)
        instance.comms_address3 = validated_data.get('comms_address3', instance.comms_address3)
        instance.comms_country_ref_id = validated_data.get('comms_country_ref_id', instance.comms_country_ref_id)
        instance.comms_state_ref_id = validated_data.get('comms_state_ref_id', instance.comms_state_ref_id)
        instance.comms_city_ref_id = validated_data.get('comms_city_ref_id', instance.comms_city_ref_id)
        instance.comms_pincode = validated_data.get('comms_pincode', instance.comms_pincode)
        instance.cin = validated_data.get('cin', instance.cin)
        instance.pan = validated_data.get('pan', instance.pan)
        instance.tan = validated_data.get('tan', instance.tan)
        instance.gst = validated_data.get('gst', instance.gst)
        instance.email = validated_data.get('email', instance.email)
        instance.contact_person_name = validated_data.get('contact_person_name', instance.contact_person_name)
        instance.is_holding_company = validated_data.get('is_holding_company', instance.is_holding_company)
        instance.belongs_to_company_id = validated_data.get('belongs_to_company_id', instance.belongs_to_company_id)
        instance.is_this_under_same_management = validated_data.get('is_this_under_same_management', instance.is_this_under_same_management)
        instance.management_belongs_to_company_id = validated_data.get('management_belongs_to_company_id', instance.management_belongs_to_company_id)
        instance.percentage_holding = validated_data.get('percentage_holding', instance.percentage_holding)
        instance.is_group_company = validated_data.get('is_group_company', instance.is_group_company)
        instance.contact_person_mobile_number = validated_data.get('contact_person_mobile_number', instance.contact_person_mobile_number)
        instance.is_this_branch = validated_data.get('is_this_branch', instance.is_this_branch)
        instance.pincode = validated_data.get('pincode', instance.pincode)
        instance.revision_status = validated_data.get('revision_status', instance.revision_status)
        instance.attach_document = validated_data.get('attach_document', instance.attach_document)
        instance.bank_name = validated_data.get('bank_name', instance.bank_name)
        instance.bank_account = validated_data.get('bank_account', instance.bank_account)
        instance.ifsc_code = validated_data.get('ifsc_code', instance.ifsc_code)
        instance.beneficiary_code = validated_data.get('beneficiary_code', instance.beneficiary_code)
        instance.service_model_ref_id = validated_data.get('service_model_ref_id', instance.service_model_ref_id)
        instance.billing_mode_ref_id = validated_data.get('billing_mode_ref_id', instance.billing_mode_ref_id)
        instance.onboarding_ref_id = validated_data.get('onboarding_ref_id', instance.onboarding_ref_id)
        instance.settlement_type_ref_id = validated_data.get('settlement_type_ref_id', instance.settlement_type_ref_id)
        instance.biller_category_ref_id = validated_data.get('biller_category_ref_id', instance.biller_category_ref_id)
        instance.bill_amount_option_ref_id = validated_data.get('bill_amount_option_ref_id', instance.bill_amount_option_ref_id)
        instance.verification_comments = validated_data.get('verification_comments', instance.verification_comments)
        instance.biller_status = validated_data.get('biller_status', instance.biller_status)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.updated_by = validated_data.get('updated_by', instance.updated_by)

        instance.save()
        keep_details = []

        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_company_contact_details.objects.filter(id=init['id']).exists():
                    det = tbl_company_contact_details.objects.get(id=init['id'])
                    det.contact_person_name = init.get('contact_person_name',det.contact_person_name)
                    det.designation = init.get('designation',det.designation)
                    det.contact_person_mobile_number = init.get('contact_person_mobile_number',det.contact_person_mobile_number)
                    det.email = init.get('email',det.email)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.updated_by = init.get('updated_by',det.updated_by)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_company_contact_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_company_contact_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_company_contact_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()

        return instance


class tbl_login_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_login_mst
        fields = '__all__'

# Employee Registation Master
class tbl_employee_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    company_name = serializers.CharField(source='company_ref_id.company_name', read_only=True)
    role_name = serializers.CharField(source='role_ref_id.role_name', read_only=True)
    city_name = serializers.CharField(source='city_ref_id.city_name', read_only=True)
    state_name = serializers.CharField(source='state_ref_id.state', read_only=True)
    country_name = serializers.CharField(source='country_ref_id.name', read_only=True)

    class Meta:
        model = tbl_employee_mst
        fields = '__all__'

# Payment Screen
class tbl_reason_code_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_reason_code_mst
        fields = '__all__'

class tbl_tax_authority_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    short_name = serializers.CharField(source='country_ref_id.short_name', read_only=True)
    zone = serializers.CharField(source='zone_ref_id.master_key', read_only=True)
    tax_type = serializers.CharField(source='tax_type_ref_id.master_key', read_only=True)
    country_name =  serializers.CharField(source='country_ref_id.name', read_only=True)
    class Meta:
        model = tbl_tax_authority_mst
        fields = '__all__'

class tbl_tax_rate_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_tax_rate_details
        fields = '__all__'

class tbl_tax_rate_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    tax_name = serializers.CharField(source='tax_authority_ref_id.tax_name', read_only=True)
    master_key = serializers.CharField(source='tax_type_ref_id.master_key', read_only=True)
    initialItemRow = tbl_tax_rate_details_serializer(many=True)

    class Meta:
        model = tbl_tax_rate_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        taxtype = tbl_tax_rate_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_tax_rate_details.objects.create(**item, header_ref_id=taxtype)
        return taxtype

    def update(self, instance, validated_data):
        object = tbl_tax_rate_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.tax_rate_name = validated_data.get('tax_rate_name', instance.tax_rate_name)
        instance.tax_authority_ref_id = validated_data.get('tax_authority_ref_id', instance.tax_authority_ref_id)
        instance.tax_type_ref_id = validated_data.get('tax_type_ref_id', instance.tax_type_ref_id)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.created_by = validated_data.get('created_by', instance.created_by)
        instance.updated_by = validated_data.get('updated_by', instance.updated_by)
        instance.save()
        keep_details = []

        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_tax_rate_details.objects.filter(id=init['id']).exists():
                    det = tbl_tax_rate_details.objects.get(id=init['id'])
                    det.hsn_sac_no = init.get('hsn_sac_no',det.hsn_sac_no)
                    det.description = init.get('description',det.description)
                    det.from_date = init.get('from_date',det.from_date)
                    det.to_date = init.get('to_date',det.to_date)
                    det.rcm_flag = init.get('rcm_flag',det.rcm_flag)
                    det.cess = init.get('cess',det.cess)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.created_by = init.get('created_by',det.created_by)
                    det.updated_by = init.get('updated_by',det.updated_by)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_tax_rate_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_tax_rate_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_tax_rate_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()

        return instance

class tbl_product_label_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    labels = serializers.SerializerMethodField()
    class Meta:
        model = tbl_product_mst
        # fields = '__all__'
        fields = ['id','channel_ref_id','product_description','product_name','labels','stream_ref_id']

    def __init__(self, *args, **kwargs):
        super(tbl_product_label_mst_serializer, self).__init__(*args, **kwargs)

        self.fields['labels'] = SerializerMethodField()

    def get_labels(self, *args):
        labels = {}
        for field in self.Meta.model._meta.get_fields():
            if field.name in self.fields:
                labels[field.name] = field.verbose_name
        return labels

class GeneralSerializer(serializers.ModelSerializer):
    labels = serializers.SerializerMethodField()

    def _init_(self, *args, **kwargs):
        super(GeneralSerializer, self)._init_(*args, **kwargs)

        if 'labels' in self.fields:
            raise RuntimeError(
                'You cant have labels field defined '
                'while using GeneralSerializer'
            )

        self.fields['labels'] = SerializerMethodField()

    def get_labels(self, *args):
        labels = {}

        for field in self.Meta.model._meta.get_fields():
            if field.name in self.fields:
                labels[field.name] = field.verbose_name

        return labels

    class Meta:
        model = None
        exclude = ('share_id','is_active','is_deleted','created_date_time','created_by','updated_date_time','updated_by', 'sub_application_id','application_id' )

# Transaction Serializers
class tbl_end_customer_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    agentName = serializers.CharField(source='agent_ref_id.company_name', read_only=True)
    country_name =  serializers.CharField(source='country_ref_id.short_name', read_only=True)
    state_name = serializers.CharField(source='state_ref_id.state', read_only=True)
    city_name = serializers.CharField(source='city_ref_id.city_name', read_only=True)

    class Meta:
        model = tbl_end_customer_mst
        fields = '__all__'

class tbl_bbps_transaction_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_bbps_transaction_mst
        fields = '__all__'

class tbl_bbps_transaction_details_serializer(serializers.ModelSerializer):
    class Meta:
        model = tbl_bbps_transaction_details
        fields = '__all__'

class tbl_kyc_document_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_kyc_document_mst
        fields = '__all__'

class tbl_biller_category_mst_serializer(serializers.ModelSerializer):

    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_biller_category_mst
        fields = ['id','cat_id','cat_name','is_deleted']

class tbl_fees_code_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_fees_code_mst
        fields = '__all__'


class tbl_interchange_fee_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_interchange_fee_details
        fields = '__all__'

class tbl_interchange_fee_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    bbpoubank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True)
    settlementfrequency_name = serializers.CharField(source='settlement_frequency_ref_id.master_key', read_only=True)
    npcisettlementtype_name = serializers.CharField(source='settlement_type_ref_id.master_key', read_only=True)
    billingfrequency_name = serializers.CharField(source='billing_frequency_ref_id.master_key', read_only=True)

    initialItemRow = tbl_interchange_fee_details_serializer(many=True)

    class Meta:
        model = tbl_interchange_fee_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        InterchangeMaster = tbl_interchange_fee_mst.objects.create(**validated_data)
        for item in initialItemRow:
            tbl_interchange_fee_details.objects.create(**item, header_ref_id=InterchangeMaster)
        return InterchangeMaster

    def update(self, instance, validated_data):
        object = tbl_interchange_fee_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        if instance.revision_status == 'Effective':
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')
            standardApi = tbl_interchange_fee_mst.objects.create(**validated_data)

            for item in initialItemRow:
                tbl_interchange_fee_details.objects.create(**item, header_ref_id=standardApi)
            return instance

        else:
            interchange_fee = tbl_interchange_fee_mst.objects.get(biller_ref_id=instance.biller_ref_id,revision_status='Effective')
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            interchange_fee.end_date=final_to_date
            interchange_fee.save()

            instance.admin_company_ref_id = validated_data.get('admin_company_ref_id', instance.admin_company_ref_id)
            instance.bbpou_bank_ref_id = validated_data.get('bbpou_bank_ref_id', instance.bbpou_bank_ref_id)
            instance.biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            instance.settlement_frequency_ref_id = validated_data.get('settlement_frequency_ref_id', instance.settlement_frequency_ref_id)
            instance.settlement_type_ref_id = validated_data.get('settlement_type_ref_id', instance.settlement_type_ref_id)
            instance.billing_frequency_ref_id = validated_data.get('billing_frequency_ref_id', instance.billing_frequency_ref_id)
            instance.start_date = validated_data.get('start_date', instance.start_date)
            instance.end_date = validated_data.get('end_date', instance.end_date)
            instance.revision_status = validated_data.get('revision_status', instance.revision_status)
            instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
            instance.application_id = validated_data.get('application_id', instance.application_id)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.is_active = validated_data.get('is_active', instance.is_active)
            instance.created_date_time = validated_data.get('created_date_time', instance.created_date_time)
            instance.created_by = validated_data.get('created_by', instance.created_by)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
            instance.updated_by = validated_data.get('updated_by', instance.updated_by)
            instance.save()
            keep_details = []

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_interchange_fee_details.objects.filter(id=init['id']).exists():
                        det = tbl_interchange_fee_details.objects.get(id=init['id'])
                        det.header_ref_id = init.get('header_ref_id',det.header_ref_id)
                        det.transaction_mode_ref_id = init.get('transaction_mode_ref_id',det.transaction_mode_ref_id)
                        det.fee_type_flag_ref_id = init.get('fee_type_flag_ref_id',det.fee_type_flag_ref_id)
                        det.rate_flag = init.get('rate_flag',det.rate_flag)
                        det.rate = init.get('rate',det.rate)
                        det.percent = init.get('percent',det.percent)
                        det.min_commission = init.get('min_commission',det.min_commission)
                        det.max_commission = init.get('max_commission',det.max_commission)
                        det.tax_rate = init.get('tax_rate',det.tax_rate)
                        det.min_value = init.get('min_value',det.min_value)
                        det.max_value = init.get('max_value',det.max_value)
                        det.min_transaction = init.get('min_transaction',det.min_transaction)
                        det.max_transaction = init.get('max_transaction',det.max_transaction)
                        det.mti_ref_id = init.get('mti_ref_id',det.mti_ref_id)
                        det.payment_mode_ref_id = init.get('payment_mode_ref_id',det.payment_mode_ref_id)
                        det.payment_channel_ref_id=init.get('payment_channel_ref_id',det.payment_channel_ref_id)
                        det.default_flag = init.get('default_flag',det.default_flag)
                        det.fees_code = init.get('fees_code',det.fees_code)
                        det.created_date_time = init.get('created_date_time',det.created_date_time)
                        det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
                        instance.is_active = validated_data.get('is_active', instance.is_active)
                        det.updated_by = init.get('updated_by',det.updated_by)
                        det.created_by = init.get('created_by',det.created_by)

                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_interchange_fee_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det = tbl_interchange_fee_details.objects.filter(header_ref_id=object.id)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    continue
                else:
                    det_record = tbl_interchange_fee_details.objects.get(id=d)
                    det_record.is_deleted = 'Y'
                    det_record.save()

            return instance

class tbl_biller_bank_charges_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_biller_bank_charges_details
        fields = '__all__'

class tbl_biller_bank_charges_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    bbpoubank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True)
    onboardingstatus_name = serializers.CharField(source='onboarding_ref_id.master_key', read_only=True)
    servicemodel_name = serializers.CharField(source='service_model_ref_id.master_key', read_only=True)
    billingmode_name = serializers.CharField(source='billing_mode_ref_id.master_key', read_only=True)
    settlementfrequency_name = serializers.CharField(source='settlement_frequency_ref_id.master_key', read_only=True)
    settlementtype_name = serializers.CharField(source='settlement_type_ref_id.master_key', read_only=True)
    billingfrequency_name = serializers.CharField(source='billing_frequency_ref_id.master_key', read_only=True)
    billercategory_name=serializers.CharField(source='biller_category_ref_id.master_key',read_only=True)
    initialItemRow = tbl_biller_bank_charges_details_serializer(many=True)

    class Meta:
        model = tbl_biller_bank_charges_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')

        BillerChargesMaster = tbl_biller_bank_charges_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_biller_bank_charges_details.objects.create(**item, header_ref_id=BillerChargesMaster)
        return BillerChargesMaster

    def update(self, instance, validated_data):
        object = tbl_biller_bank_charges_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        if instance.revision_status == 'Effective':
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')
            BillerChargesMaster = tbl_biller_bank_charges_mst.objects.create(**validated_data)

            for item in initialItemRow:
                tbl_biller_bank_charges_details.objects.create(**item, header_ref_id=BillerChargesMaster)
            return instance

        else:
            biller_bank = tbl_biller_bank_charges_mst.objects.get(biller_ref_id=instance.biller_ref_id,revision_status='Effective')
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            biller_bank.end_date=final_to_date
            biller_bank.save()

            instance.admin_company_ref_id = validated_data.get('admin_company_ref_id', instance.admin_company_ref_id)
            instance.bbpou_bank_ref_id = validated_data.get('bbpou_bank_ref_id', instance.bbpou_bank_ref_id)
            instance.biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            instance.billing_frequency_ref_id = validated_data.get('billing_frequency_ref_id', instance.billing_frequency_ref_id)
            instance.settlement_frequency_ref_id = validated_data.get('settlement_frequency_ref_id', instance.settlement_frequency_ref_id)
            instance.settlement_type_ref_id = validated_data.get('settlement_type_ref_id', instance.settlement_type_ref_id)
            instance.start_date = validated_data.get('start_date', instance.start_date)
            instance.end_date = validated_data.get('end_date', instance.end_date)
            instance.revision_status = validated_data.get('revision_status', instance.revision_status)
            instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
            instance.application_id = validated_data.get('application_id', instance.application_id)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.is_active = validated_data.get('is_active', instance.is_active)
            instance.created_date_time = validated_data.get('created_date_time', instance.created_date_time)
            instance.created_by = validated_data.get('created_by', instance.created_by)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
            instance.updated_by = validated_data.get('updated_by', instance.updated_by)

            instance.save()
            keep_details = []

        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_biller_bank_charges_details.objects.filter(id=init['id']).exists():
                    det = tbl_biller_bank_charges_details.objects.get(id=init['id'])
                    det.header_ref_id = init.get('header_ref_id',det.header_ref_id)
                    det.rate_flag = init.get('rate_flag',det.rate_flag)
                    det.fee_type_flag_ref_id = init.get('transaction_mode_ref_id',det.transaction_mode_ref_id)
                    det.rate = init.get('rate',det.rate)
                    det.percent = init.get('percent',det.percent)
                    det.min_commission = init.get('min_commission',det.min_commission)
                    det.max_commission = init.get('max_commission',det.max_commission)
                    det.tax_rate = init.get('tax_rate',det.tax_rate)
                    det.min_value = init.get('min_value',det.min_value)
                    det.max_value = init.get('max_value',det.max_value)
                    det.min_transaction = init.get('min_transaction',det.min_transaction)
                    det.max_transaction = init.get('max_transaction',det.max_transaction)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
                    instance.is_active = validated_data.get('is_active', instance.is_active)
                    det.updated_by = init.get('updated_by',det.updated_by)
                    det.created_by = init.get('created_by',det.created_by)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_biller_bank_charges_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_biller_bank_charges_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_biller_bank_charges_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()

        return instance

class tbl_platform_charges_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_platform_charges_details
        fields = '__all__'

class tbl_platform_charges_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    bbpoubank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True)
    onboardingstatus_name = serializers.CharField(source='onboarding_ref_id.master_key', read_only=True)
    servicemodel_name = serializers.CharField(source='service_model_ref_id.master_key', read_only=True)
    billingmode_name = serializers.CharField(source='billing_mode_ref_id.master_key', read_only=True)
    settlementfrequency_name = serializers.CharField(source='settlement_frequency_ref_id.master_key', read_only=True)
    settlementtype_name = serializers.CharField(source='settlement_type_ref_id.master_key', read_only=True)
    billingfrequency_name = serializers.CharField(source='billing_frequency_ref_id.master_key', read_only=True)
    billercategory_name=serializers.CharField(source='biller_category_ref_id.master_key',read_only=True)
    initialItemRow = tbl_platform_charges_details_serializer(many=True)

    class Meta:
        model = tbl_platform_charges_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        PlatformChargesMaster = tbl_platform_charges_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_platform_charges_details.objects.create(**item, header_ref_id=PlatformChargesMaster)
        return PlatformChargesMaster

    def update(self, instance, validated_data):
        object = tbl_platform_charges_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        if instance.revision_status == 'Effective':
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')
            PlatformChargesMaster = tbl_platform_charges_mst.objects.create(**validated_data)
            for item in initialItemRow:
                tbl_platform_charges_details.objects.create(**item, header_ref_id=PlatformChargesMaster)
            return instance

        else:
            platformobj = tbl_platform_charges_mst.objects.get(biller_ref_id=instance.biller_ref_id,revision_status='Effective')
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            platformobj.end_date=final_to_date
            platformobj.save()

            instance.admin_company_ref_id = validated_data.get('admin_company_ref_id', instance.admin_company_ref_id)
            instance.bbpou_bank_ref_id = validated_data.get('bbpou_bank_ref_id', instance.bbpou_bank_ref_id)
            instance.biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            instance.billing_frequency_ref_id = validated_data.get('billing_frequency_ref_id', instance.billing_frequency_ref_id)
            instance.settlement_frequency_ref_id = validated_data.get('settlement_frequency_ref_id', instance.settlement_frequency_ref_id)
            instance.settlement_type_ref_id = validated_data.get('settlement_type_ref_id', instance.settlement_type_ref_id)
            instance.start_date = validated_data.get('start_date', instance.start_date)
            instance.end_date = validated_data.get('end_date', instance.end_date)
            instance.revision_status = validated_data.get('revision_status', instance.revision_status)
            instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
            instance.application_id = validated_data.get('application_id', instance.application_id)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.is_active = validated_data.get('is_active', instance.is_active)
            instance.created_date_time = validated_data.get('created_date_time', instance.created_date_time)
            instance.created_by = validated_data.get('created_by', instance.created_by)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
            instance.updated_by = validated_data.get('updated_by', instance.updated_by)
            instance.save()
            keep_details = []

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_platform_charges_details.objects.filter(id=init['id']).exists():
                        det = tbl_platform_charges_details.objects.get(id=init['id'])
                        det.header_ref_id = init.get('header_ref_id',det.header_ref_id)
                        det.rate_flag = init.get('rate_flag',det.rate_flag)
                        det.fee_type_flag_ref_id = init.get('transaction_mode_ref_id',det.transaction_mode_ref_id)
                        det.rate = init.get('rate',det.rate)
                        det.percent = init.get('percent',det.percent)
                        det.min_commission = init.get('min_commission',det.min_commission)
                        det.max_commission = init.get('max_commission',det.max_commission)
                        det.tax_rate = init.get('tax_rate',det.tax_rate)
                        det.min_value = init.get('min_value',det.min_value)
                        det.max_value = init.get('max_value',det.max_value)
                        det.min_transaction = init.get('min_transaction',det.min_transaction)
                        det.max_transaction = init.get('max_transaction',det.max_transaction)
                        det.created_date_time = init.get('created_date_time',det.created_date_time)
                        det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
                        instance.is_active = validated_data.get('is_active', instance.is_active)
                        det.updated_by = init.get('updated_by',det.updated_by)
                        det.created_by = init.get('created_by',det.created_by)
                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_platform_charges_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det = tbl_platform_charges_details.objects.filter(header_ref_id=object.id)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    continue
                else:
                    det_record = tbl_platform_charges_details.objects.get(id=d)
                    det_record.is_deleted = 'Y'
                    det_record.save()

            return instance

class tbl_source_count_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_source_count_mst
        fields = '__all__'
class FilterDeleteListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(is_deleted='N', is_active='Y', is_user_deleted_field=False)
        return super(FilterDeleteListSerializer, self).to_representation(data)

class SourceDetailsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)

    class Meta:
        model = tbl_source_details
        fields = '__all__'
        list_serializer_class = FilterDeleteListSerializer

class SourceMasterSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)
    file_type = serializers.CharField(source='file_type_ref_id.master_key',read_only=True)
    initialItemRow = SourceDetailsSerializer(many=True)

    class Meta:
        model = tbl_source_mst
        fields='__all__'

    def validate(self, data):
        if self.instance==None: 
            if tbl_source_mst.objects.filter(source_name=data['source_name'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("Source Name Already Exists")
        
        elif self.instance != None:
            if self.instance.id and tbl_source_mst.objects.filter(source_name=data['source_name'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("Source Name Already Exists")

        return data

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        source = tbl_source_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_source_details.objects.create(**item,header_ref_id=source)
        return source

    def update(self, instance, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')

        if validated_data['status'] == 'Permanent Save' and instance.revision_status == 'Effective' and validated_data['version_number'] > 1:
            change_to_date=datetime.strptime(str(validated_data['from_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.to_date=final_to_date
            instance.save()
            validated_data.pop('id')
            source = tbl_source_mst.objects.create(**validated_data)
            for item in initialItemRow:
                if item.get('id'):
                    del item['id']
                tbl_source_details.objects.create(**item,header_ref_id=source)
            return instance
        else:
            instance.source_name=validated_data.get('source_name',instance.source_name)
            instance.file_type_ref_id=validated_data.get('file_type_ref_id',instance.file_type_ref_id)
            instance.status=validated_data.get('status',instance.status)
            instance.application_id=validated_data.get('application_id',instance.application_id)
            instance.sub_application_id=validated_data.get('sub_application_id',instance.sub_application_id)
            instance.from_date=validated_data.get('from_date',(instance.from_date).strftime('%Y-%m-%d'))
            instance.to_date=validated_data.get('to_date',(instance.to_date).strftime('%Y-%m-%d'))
            instance.revision_status=validated_data.get('revision_status',instance.revision_status)
            instance.is_temporary_flag=validated_data.get('is_temporary_flag',instance.is_temporary_flag)
            instance.version_number=validated_data.get('version_number',instance.version_number)
            instance.is_deleted=validated_data.get('is_deleted',instance.is_deleted)
            instance.updated_date_time=validated_data.get('updated_date_time',instance.updated_date_time)
            instance.updated_by=validated_data.get('updated_by',instance.updated_by)
            instance.status=validated_data.get('status',instance.status)
            instance.save()

            keep_details = []
            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_source_details.objects.filter(id=init['id']).exists():
                        det = tbl_source_details.objects.get(id=init['id'])
                        det.field_data_type_ref_id = init.get('field_data_type_ref_id',det.field_data_type_ref_id)
                        det.field_name = init.get('field_name',det.field_name)
                        det.field_length = init.get('field_length',det.field_length)
                        det.is_key_field = init.get('is_key_field',det.is_key_field)
                        det.is_primary_field = init.get('is_primary_field',det.is_primary_field)
                        det.is_user_deleted_field = init.get('is_user_deleted_field',det.is_user_deleted_field)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                        det.updated_by = init.get('updated_by',det.updated_by)
                        det.updated_date_time = init.get('updated_date_time', det.updated_date_time)

                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_source_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det=tbl_source_details.objects.filter(header_ref_id=instance)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    det_record = tbl_source_details.objects.get(id=d)
                    if det_record.is_user_deleted_field == True:
                        det_record.is_deleted='Y'
                        det_record.is_active='N'
                        det_record.save()
                else:
                    det_record=  tbl_source_details.objects.get(id=d)
                    det_record.is_deleted='Y'
                    det_record.is_active='N'
                    det_record.is_user_deleted_field =True
                    det_record.save()
            return instance

# Manage Security Module

class LeftPanelSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(required=False)
    # class Meta:
    #     model = tbl_left_panel
    #     fields = '__all__'

    # def get(self, request):
    #     return tbl_left_panel.objects.filter(is_deleted='N').filter(application_id=request.GET['application_id'])

    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_left_panel
        fields = '__all__'

    def get(form_name):
        left_panel_obj = tbl_left_panel.objects.get(form_name = form_name, is_deleted = 'N')
        return left_panel_obj.id

class AssignPagesRolesSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_assign_pages_roles
        fields = '__all__'

class UserRoleDetailsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_assign_role_user_details
        fields = '__all__'

class AssignUserRolesSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    company_name = serializers.CharField(source='company_ref_id.company_name', read_only=True)
    assigned_to_user_id = serializers.IntegerField(source='assigned_to_user.id', read_only=True)
    initialItemRow = UserRoleDetailsSerializer(many=True)

    class Meta:
        model = tbl_assign_role_user_mst
        fields = '__all__'
    
    def get(user):
        login_data = LoginMasterSerializer.get(user)
        assign_role_user = tbl_assign_role_user_mst.objects.get(assigned_to_user=user,company_ref_id = login_data.company_id_id, is_deleted='N',is_active='Y')
        return tbl_assign_role_user_details.objects.filter(header_ref_id=assign_role_user,is_deleted='N',is_active='Y')

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        role = tbl_assign_role_user_mst.objects.create(**validated_data)
        for item in initialItemRow:
            tbl_assign_role_user_details.objects.create(**item, header_ref_id=role)
        return role

    def update(self, instance, validated_data):
        object = tbl_assign_role_user_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.company_ref_id = validated_data.get('company_ref_id', instance.company_ref_id)
        instance.assigned_to_user = validated_data.get('assigned_to_user', instance.assigned_to_user)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.save()

        updated_data = []
        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_assign_role_user_details.objects.filter(id=init['id']).exists():
                    det = tbl_assign_role_user_details.objects.get(id=init['id'])
                    det.assigned_to_role_ref_id = init.get('assigned_to_role_ref_id',det.assigned_to_role_ref_id)
                    det.created_date_time = init.get('created_date_time', det.created_date_time)
                    det.is_deleted = init.get('is_deleted', det.is_deleted)
                    det.updated_date_time = init.get('updated_date_time', det.updated_date_time)
                    det.application_id = init.get('application_id', det.application_id)
                    det.sub_application_id = init.get('sub_application_id', det.sub_application_id)
                    det.save()
                    updated_data.append(det.id)
                else:
                    continue
            else:
                det = tbl_assign_role_user_details.objects.create(**init, header_ref_id=instance)
                updated_data.append(det.id)

        det = tbl_assign_role_user_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in updated_data:
                continue
            else:
                det_record = tbl_assign_role_user_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()
        return instance
class tbl_biller_invoice_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)

    class Meta:
        model = tbl_biller_invoice_details
        fields = '__all__'

class tbl_biller_invoice_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)   #may be required

    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    currency_code = serializers.CharField(source='transaction_currency_ref_id.currency_code', read_only=True)

    initialItemRow = tbl_biller_invoice_details_serializer(many=True)

    class Meta:
        model = tbl_biller_invoice_mst
        fields='__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        invoice_data = tbl_biller_invoice_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_biller_invoice_details.objects.create(**item,header_ref_id=invoice_data)
        return invoice_data

    def update(self, instance, validated_data):
        object = tbl_biller_invoice_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.invoice_ref_no=validated_data.get('invoice_ref_no',instance.invoice_ref_no)
        instance.admin_company_ref_id=validated_data.get('admin_company_ref_id',instance.admin_company_ref_id)
        instance.bbpou_bank_ref_id=validated_data.get('bbpou_bank_ref_id',instance.bbpou_bank_ref_id)
        instance.biller_ref_id=validated_data.get('biller_ref_id',instance.biller_ref_id)
        instance.biller_category_ref_id=validated_data.get('biller_category_ref_id',instance.biller_category_ref_id)
        instance.transaction_date=validated_data.get('transaction_date',(instance.transaction_date).strftime('%Y-%m-%d'))
        instance.fiscalyear=validated_data.get('fiscalyear',instance.fiscalyear)
        instance.period=validated_data.get('period',instance.period)
        instance.billing_address=validated_data.get('billing_address',instance.billing_address)
        instance.transaction_currency_ref_id=validated_data.get('transaction_currency_ref_id',instance.transaction_currency_ref_id)
        instance.total_basic_amount=validated_data.get('total_basic_amount',instance.total_basic_amount)
        instance.total_tax_amount=validated_data.get('total_tax_amount',instance.total_tax_amount)
        instance.total_amount=validated_data.get('total_amount',instance.total_amount)
        instance.remark=validated_data.get('remark',instance.remark)

        instance.conversion_rate=validated_data.get('conversion_rate',instance.conversion_rate)
        instance.is_active=validated_data.get('is_active',instance.is_active)
        instance.is_deleted=validated_data.get('is_deleted',instance.is_deleted)
        instance.created_date_time=validated_data.get('created_date_time',instance.created_date_time)
        instance.created_by=validated_data.get('created_by',instance.created_by)
        instance.updated_date_time=validated_data.get('updated_date_time',instance.updated_date_time)
        instance.updated_by=validated_data.get('updated_by',instance.updated_by)
        instance.application_id=validated_data.get('application_id',instance.application_id)
        instance.sub_application_id=validated_data.get('sub_application_id',instance.sub_application_id)


        instance.save()

        keep_details = []
        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_biller_invoice_details.objects.filter(id=init['id']).exists():
                    det = tbl_biller_invoice_details.objects.get(id=init['id'])

                    det.total_no_transaction = init.get('total_no_transaction',det.total_no_transaction)
                    det.interchange_fee = init.get('interchange_fee',det.interchange_fee)
                    det.biller_bank_fee = init.get('biller_bank_fee',det.biller_bank_fee)
                    det.platform_fee = init.get('platform_fee',det.platform_fee)
                    det.igst_amount = init.get('igst_amount',det.igst_amount)
                    det.cgst_amount = init.get('cgst_amount',det.cgst_amount)
                    det.sgst_amount = init.get('sgst_amount',det.sgst_amount)
                    det.is_active = init.get('is_active',det.is_active)
                    det.is_deleted = init.get('is_deleted',det.is_deleted)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.created_by = init.get('created_by',det.created_by)
                    det.updated_by = init.get('updated_by',det.updated_by)
                    det.updated_date_time = init.get('updated_date_time', det.updated_date_time)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_biller_invoice_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_biller_invoice_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_biller_invoice_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.save()

        return instance

# Assign Screen To Role
class tbl_assign_screen_to_role_mst_Serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    company_name = serializers.CharField(source='company_ref_id.company_name', read_only=True)
    form_name = serializers.CharField(source='form_ref_id.form_name',read_only=True)
    role_name = serializers.CharField(source='assigned_to_role.role_name',read_only=True)
    class Meta:
        model = tbl_assign_pages_roles
        fields = '__all__'

    def get(self, request):
        return tbl_assign_pages_roles.objects.filter(is_deleted='N',is_active='Y').order_by('-id')

# Assign Roles To Enduser
class RolesToEnduserDetailsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_assign_role_user_mst
        fields = '__all__'

class tbl_bulk_payment_file_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = tbl_bulk_payment_file_details
        fields = '__all__'

class paymentfileSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    initialItemRow = tbl_bulk_payment_file_details_serializer(many=True)
    biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)

    class Meta:
        model = tbl_bulk_payment_file_mst
        fields = '__all__'
    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')

        transaction_date = validated_data['transaction_date']
        biller_ref_id = validated_data['biller_ref_id']
        checkTransactionDate = tbl_bulk_payment_file_mst.objects.filter(biller_ref_id = biller_ref_id, transaction_date = transaction_date).exists()
        if checkTransactionDate == True:
            raise
        
        with transaction.atomic():
            companyMaster = tbl_bulk_payment_file_mst.objects.create(**validated_data)
            for item in initialItemRow:
                customer_ref_no = item['customer_ref_no']
                customer_seq_no = tbl_bulk_payment_file_details.objects.values('customer_seq_no').last()
                if customer_seq_no:
                    customer_seq_no = customer_seq_no['customer_seq_no'] + 1
                else:
                    customer_seq_no = 1

                item['customer_ref_no'] = f"{customer_ref_no}{customer_seq_no}"
                item['customer_seq_no'] = customer_seq_no
                tbl_bulk_payment_file_details.objects.create(**item, header_ref_id=companyMaster)

            return companyMaster

    def update(self, instance, validated_data):
        with transaction.atomic():
            objectt = tbl_bulk_payment_file_mst.objects.get(id=validated_data['id'])
            initialItemRow = validated_data.pop('initialItemRow')

            instance.transaction_date = validated_data.get('transaction_date', instance.transaction_date)
            instance.total_transaction_amount = validated_data.get('total_transaction_amount', instance.total_transaction_amount)
            instance.remark = validated_data.get('remark', instance.remark)
            instance.status = validated_data.get('status', instance.status)
            instance.save()

            find_to_biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            from decimal import Decimal
            from django.db.models import Sum
            total_transaction_amount = validated_data.get('total_transaction_amount', instance.total_transaction_amount)
            payment_amount = tbl_bbps_transaction_mst.objects.filter(biller_ref_id = find_to_biller_ref_id, payment_status='Paid', recon_status = 'Reconciled', remitted_amount = 0).aggregate(total=Sum('payment_amount'))['total'] or Decimal()

            if float(payment_amount)!=float(total_transaction_amount):
                raise

            bbps_trs_mst = tbl_bbps_transaction_mst.objects.filter(biller_ref_id = find_to_biller_ref_id, payment_status='Paid', recon_status = 'Reconciled', remitted_amount = 0).update(bulk_payment_file_id = objectt, remitted_amount = F('payment_amount'))

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_bulk_payment_file_details.objects.filter(id=init['id']).exists():
                        file_details = tbl_bulk_payment_file_details.objects.get(id=init['id'])
                        file_details.transaction_type = init.get('transaction_type',file_details.transaction_type)
                        file_details.beneficiary_code = init.get('beneficiary_code',file_details.beneficiary_code)
                        file_details.beneficiary_accno = init.get('beneficiary_accno',file_details.beneficiary_accno)
                        file_details.transaction_amount = init.get('transaction_amount',file_details.transaction_amount)
                        file_details.beneficiary_name = init.get('beneficiary_name',file_details.beneficiary_name)
                        file_details.drawee_location = init.get('drawee_location',file_details.drawee_location)
                        file_details.print_location = init.get('print_location',file_details.print_location)
                        file_details.beneficiary_add_line1 = init.get('beneficiary_add_line1',file_details.beneficiary_add_line1)
                        file_details.beneficiary_add_line2 = init.get('beneficiary_add_line2',file_details.beneficiary_add_line2)
                        file_details.beneficiary_add_line3 = init.get('beneficiary_add_line3',file_details.beneficiary_add_line3)
                        file_details.beneficiary_add_line4 = init.get('beneficiary_add_line4',file_details.beneficiary_add_line4)
                        file_details.zipcode = init.get('zipcode',file_details.zipcode)
                        file_details.instrument_ref_no = init.get('instrument_ref_no',file_details.instrument_ref_no)
                        file_details.customer_ref_no = init.get('customer_ref_no',file_details.customer_ref_no)
                        file_details.advising_detail1 = init.get('advising_detail1',file_details.advising_detail1)
                        file_details.advising_detail2 = init.get('advising_detail2',file_details.advising_detail2)
                        file_details.advising_detail3 = init.get('advising_detail3',file_details.advising_detail3)
                        file_details.advising_detail4 = init.get('advising_detail4',file_details.advising_detail4)
                        file_details.advising_detail5 = init.get('advising_detail5',file_details.advising_detail5)
                        file_details.advising_detail6 = init.get('advising_detail6',file_details.advising_detail6)
                        file_details.advising_detail7 = init.get('advising_detail7',file_details.advising_detail7)
                        file_details.cheque_no = init.get('cheque_no',file_details.cheque_no)
                        file_details.instrument_date = init.get('instrument_date',file_details.instrument_date)
                        file_details.micr_no = init.get('micr_no',file_details.micr_no)
                        file_details.ifsc_code = init.get('ifsc_code',file_details.ifsc_code)
                        file_details.bene_bank_name = init.get('bene_bank_name',file_details.bene_bank_name)
                        file_details.bene_bank_branch = init.get('bene_bank_branch',file_details.bene_bank_branch)
                        file_details.bene_email = init.get('bene_email',file_details.bene_email)
                        file_details.debit_acc_number = init.get('debit_acc_number',file_details.debit_acc_number)
                        file_details.source_narration = init.get('source_narration',file_details.source_narration)
                        file_details.target_narration = init.get('target_narration',file_details.target_narration)
                        file_details.value_date = init.get('value_date',file_details.value_date)
                        file_details.remark = init.get('remark',file_details.remark)
                        file_details.save()

            return instance
class secure_server_config_details_serializer(serializers.ModelSerializer):
    cloud_storage = serializers.CharField(source='cloud_storage_type.master_key',read_only=True)
    id = serializers.IntegerField(required = False)

    class Meta:
        model = tbl_secure_server_config_details
        fields = '__all__'


class secure_server_config_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required = False)   #may be required

    company_name = serializers.CharField(source='company_ref_id.company_name',read_only=True)

    initialItemRow = secure_server_config_details_serializer(many=True)

    class Meta:
        model = tbl_secure_server_config_mst
        fields='__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        id = tbl_secure_server_config_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_secure_server_config_details.objects.create(**item,header_ref_id=id)
        return id

    def update(self, instance, validated_data):
        object = tbl_secure_server_config_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        instance.company_ref_id=validated_data.get('company_ref_id',instance.company_ref_id)
        instance.application_id=validated_data.get('application_id',instance.application_id)
        instance.sub_application_id=validated_data.get('sub_application_id',instance.sub_application_id)
        instance.is_deleted=validated_data.get('is_deleted',instance.is_deleted)
        instance.updated_date_time=validated_data.get('updated_date_time',instance.updated_date_time)
        instance.updated_by=validated_data.get('updated_by',instance.updated_by)
        instance.save()

        keep_details = []
        for init in initialItemRow:
            if "id" in init.keys():
                if tbl_secure_server_config_details.objects.filter(id=init['id']).exists():
                    det = tbl_secure_server_config_details.objects.get(id=init['id'])
                    det.cloud_storage_type = init.get('cloud_storage_type',det.cloud_storage_type)
                    det.host_bucket_name = init.get('host_bucket_name',det.host_bucket_name)
                    det.username = init.get('username',det.username)
                    det.password = init.get('password',det.password)
                    det.file_key_path = init.get('file_key_path',det.file_key_path)
                    det.application_id = init.get('application_id',det.application_id)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.updated_by = init.get('updated_by',det.updated_by)
                    det.updated_date_time = init.get('updated_date_time', det.updated_date_time)

                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_secure_server_config_details.objects.create(**init,header_ref_id=instance)
                keep_details.append(det.id)

        det=tbl_secure_server_config_details.objects.filter(header_ref_id=object.id)
        det_id = [d.id for d in det]

        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record=  tbl_secure_server_config_details.objects.get(id=d)
                det_record.is_deleted='Y'
                det_record.save()

        return instance

    def get(source):
        return tbl_secure_server_config_mst.objects.get(id = source)

class tbl_partner_charges_details_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = tbl_partner_charges_details
        fields = '__all__'

class tbl_partner_charges_mst_serializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    # biller_name = serializers.CharField(source='biller_ref_id.company_name', read_only=True)
    bbpoubank_name = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True)
    onboardingstatus_name = serializers.CharField(source='onboarding_ref_id.master_key', read_only=True)
    servicemodel_name = serializers.CharField(source='service_model_ref_id.master_key', read_only=True)
    billingmode_name = serializers.CharField(source='billing_mode_ref_id.master_key', read_only=True)
    settlementfrequency_name = serializers.CharField(source='settlement_frequency_ref_id.master_key', read_only=True)
    settlementtype_name = serializers.CharField(source='settlement_type_ref_id.master_key', read_only=True)
    billingfrequency_name = serializers.CharField(source='billing_frequency_ref_id.master_key', read_only=True)
    billercategory_name=serializers.CharField(source='biller_category_ref_id.master_key',read_only=True)
    partner_company_name = serializers.CharField(source='partner_company_ref_id.company_name',read_only=True)
    initialItemRow = tbl_partner_charges_details_serializer(many=True)

    class Meta:
        model = tbl_partner_charges_mst
        fields = '__all__'

    def create(self, validated_data):
        initialItemRow = validated_data.pop('initialItemRow')
        PartnerChargesMaster = tbl_partner_charges_mst.objects.create(**validated_data)

        for item in initialItemRow:
            tbl_partner_charges_details.objects.create(**item, header_ref_id=PartnerChargesMaster)
        return PartnerChargesMaster

    def update(self, instance, validated_data):
        object = tbl_partner_charges_mst.objects.get(id=validated_data['id'])
        initialItemRow = validated_data.pop('initialItemRow')

        if instance.revision_status == 'Effective':
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            instance.end_date=final_to_date
            instance.save()
            validated_data.pop('id')
            PartnerChargesMaster = tbl_partner_charges_mst.objects.create(**validated_data)
            for item in initialItemRow:
                tbl_partner_charges_details.objects.create(**item, header_ref_id=PartnerChargesMaster)
            return instance

        else:
            partnerobj = tbl_partner_charges_mst.objects.get(biller_ref_id=instance.biller_ref_id,revision_status='Effective')
            change_to_date=datetime.strptime(str(validated_data['start_date']),'%Y-%m-%d')
            final_to_date=(change_to_date- timedelta(days=1)).strftime('%Y-%m-%d')
            partnerobj.end_date=final_to_date
            partnerobj .save()

            instance.admin_company_ref_id = validated_data.get('admin_company_ref_id', instance.admin_company_ref_id)
            instance.bbpou_bank_ref_id = validated_data.get('bbpou_bank_ref_id', instance.bbpou_bank_ref_id)
            # instance.biller_ref_id = validated_data.get('biller_ref_id', instance.biller_ref_id)
            instance.billing_frequency_ref_id = validated_data.get('billing_frequency_ref_id', instance.billing_frequency_ref_id)
            # instance.settlement_frequency_ref_id = validated_data.get('settlement_frequency_ref_id', instance.settlement_frequency_ref_id)
            # instance.settlement_type_ref_id = validated_data.get('settlement_type_ref_id', instance.settlement_type_ref_id)
            instance.start_date = validated_data.get('start_date', instance.start_date)
            instance.end_date = validated_data.get('end_date', instance.end_date)
            instance.revision_status = validated_data.get('revision_status', instance.revision_status)
            instance.monthly_max_payable = validated_data.get('monthly_max_payable', instance.monthly_max_payable)
            instance.monthly_min_payable = validated_data.get('monthly_min_payable', instance.monthly_min_payable)
            instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
            instance.application_id = validated_data.get('application_id', instance.application_id)
            instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
            instance.is_active = validated_data.get('is_active', instance.is_active)
            instance.created_date_time = validated_data.get('created_date_time', instance.created_date_time)
            instance.created_by = validated_data.get('created_by', instance.created_by)
            instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
            instance.updated_by = validated_data.get('updated_by', instance.updated_by)
            
            instance.save()
            keep_details = []

            for init in initialItemRow:
                if "id" in init.keys():
                    if tbl_partner_charges_details.objects.filter(id=init['id']).exists():
                        det = tbl_partner_charges_details.objects.get(id=init['id'])
                        det.header_ref_id = init.get('header_ref_id',det.header_ref_id)
                        det.rate_flag = init.get('rate_flag',det.rate_flag)
                        det.fee_type_flag_ref_id = init.get('transaction_mode_ref_id',det.transaction_mode_ref_id)
                        det.rate = init.get('rate',det.rate)
                        det.percent = init.get('percent',det.percent)
                        det.min_commission = init.get('min_commission',det.min_commission)
                        det.max_commission = init.get('max_commission',det.max_commission)
                        det.tax_rate = init.get('tax_rate',det.tax_rate)
                        det.min_value = init.get('min_value',det.min_value)
                        det.max_value = init.get('max_value',det.max_value)
                        det.min_transaction = init.get('min_transaction',det.min_transaction)
                        det.max_transaction = init.get('max_transaction',det.max_transaction)
                        det.biller_category_ref_id = init.get('biller_category_ref_id',det.biller_category_ref_id)
                        det.created_date_time = init.get('created_date_time',det.created_date_time)
                        det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                        det.application_id = init.get('application_id',det.application_id)
                        det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
                        instance.is_active = validated_data.get('is_active', instance.is_active)
                        det.updated_by = init.get('updated_by',det.updated_by)
                        det.created_by = init.get('created_by',det.created_by)
                        det.save()
                        keep_details.append(det.id)
                    else:
                        continue
                else:
                    det = tbl_partner_charges_details.objects.create(**init,header_ref_id=instance)
                    keep_details.append(det.id)

            det = tbl_partner_charges_details.objects.filter(header_ref_id=object.id)
            det_id = [d.id for d in det]

            for d in det_id:
                if d in keep_details:
                    continue
                else:
                    det_record = tbl_partner_charges_details.objects.get(id=d)
                    det_record.is_deleted = 'Y'
                    det_record.save()

            return instance

class AddBillerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
    ownership_status = serializers.CharField(source='ownership_status_ref_id.master_key', read_only=True)
    KYC_initialItemRow = tbl_company_kyc_document_details_serializer(many=True)
    bbpou_bank = serializers.CharField(source='bbpou_bank_ref_id.bbpou_bank_name', read_only=True) 
    biller_category = serializers.CharField(source='biller_category_ref_id.cat_name', read_only=True)    
    company_type = serializers.CharField(source='company_type_ref_id.role_name', read_only=True)
    location = serializers.CharField(source='location_ref_id.location_name', read_only=True)
    country = serializers.CharField(source='country_ref_id.name', read_only=True)
    state = serializers.CharField(source='state_ref_id.state', read_only=True)
    city = serializers.CharField(source='city_ref_id.city_name', read_only=True)
    class Meta:
        model = tbl_company_mst
        fields = '__all__'

    def validate(self, data):
        if self.instance==None: 
            if tbl_company_mst.objects.filter(company_name=data['company_name'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("Biller Already Exists")            
            if tbl_company_mst.objects.filter(company_shortname=data['company_shortname'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("Biller Shortname Already Exists")
            if tbl_company_mst.objects.filter(npci_assigned_id=data['npci_assigned_id'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("NPCI Biller ID Already Exists")
            if tbl_company_mst.objects.filter(pan=data['pan'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("PAN Already Exists")
            if tbl_company_mst.objects.filter(email=data['email'], is_active='Y', is_deleted='N').exists():
                raise serializers.ValidationError("Email ID Already Exists")
            if User.objects.filter(email=data['email'], is_active=True).exists():
                raise serializers.ValidationError("Email User Already Exists")  
        
        elif self.instance != None:
            auth_user = tbl_login_mst.objects.filter(company_id=self.instance.id).values_list("user", flat=True).first()
            if self.instance.id and tbl_company_mst.objects.filter(company_name=data['company_name'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("Biller Already Exists")            
            if self.instance.id and tbl_company_mst.objects.filter(company_shortname=data['company_shortname'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("Biller Shortname Already Exists")            
            if self.instance.id and tbl_company_mst.objects.filter(npci_assigned_id=data['npci_assigned_id'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("NPCI Biller ID Already Exists")
            if self.instance.id and tbl_company_mst.objects.filter(pan=data['pan'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("PAN Already Exists")
            if self.instance.id and tbl_company_mst.objects.filter(email=data['email'], is_active='Y', is_deleted='N').exclude(id=self.instance.id).exists():
                raise serializers.ValidationError("Email ID Already Exists")            
            if self.instance.id and User.objects.filter(email=data['email'], is_active=True).exclude(id=auth_user).exists():
                raise serializers.ValidationError("Email User Already Exists")

        return data

    def create(self, validated_data):                
        parent_company_id = self.context['request'].data.pop('parent_company_id')
        company_role = self.context['request'].data.pop('company_role')
        KYC_initialItemRow = validated_data.pop('KYC_initialItemRow')

        role_data = tbl_role_mst.objects.get(role_name=company_role,is_deleted='N',is_active='Y')
        companyMaster = tbl_company_mst.objects.create(**validated_data, company_type_ref_id=role_data)
        tbl_entity_relationship_mst.objects.create(entity_ref_id_id = parent_company_id, company_ref_id = companyMaster)

        company_shortname = self.context['request'].data.pop('company_shortname')
        email = self.context['request'].data.pop('email')
        application_id = self.context['request'].data.pop('application_id')
        password = make_password('bbpsBillerAdmin@321', hasher='default')

        userAuth = User.objects.create(first_name = company_shortname, last_name=company_shortname, username=email,is_superuser=True,is_staff=False,is_active = True, password=password ,email=email)
        tbl_login_mst.objects.create(user = userAuth,role_ref_id=role_data,company_id = companyMaster, application_id=application_id)
        trum = tbl_assign_role_user_mst.objects.create(assigned_to_user = userAuth, company_ref_id=companyMaster, application_id=application_id)
        tbl_assign_role_user_details.objects.create(header_ref_id = trum,assigned_to_role_ref_id = role_data,application_id=application_id)

        subject = f'Welcome to BBPS-Biller Rhythmflows'
        user_pass = f'\n   Username:- {email} \n   Password:- bbpsBillerAdmin@321'
        server_url = f'\n   Url:- {END_USER_URL[0]}'
        no_reply = 'This is an auto generated email please do not reply to this e-mail'
        message = f'Company Name:- {companyMaster.company_name}\nYour login credentials are as follows. {user_pass}{server_url} \n\n{no_reply}'
        recipient = email
        send_mail(subject, message, EMAIL_HOST_USER, [recipient], fail_silently = False)
        for item in KYC_initialItemRow:
            tbl_kyc_document_details.objects.create(**item, biller_ref_id=companyMaster)
        return companyMaster

    def update(self, instance, validated_data):
        KYC_initialItemRow = validated_data.pop('KYC_initialItemRow')
        if instance.email != validated_data.get('email'):
            auth_user = tbl_login_mst.objects.filter(company_id=instance.id).values_list("user", flat=True).first()
            User.objects.filter(id=auth_user).update(email= validated_data.get('email'), username= validated_data.get('email'))

        instance.npci_assigned_id = validated_data.get('npci_assigned_id', instance.npci_assigned_id)
        instance.email = validated_data.get('email', instance.email)
        instance.company_id = validated_data.get('company_id', instance.company_id)
        instance.share_id = validated_data.get('share_id', instance.share_id)
        instance.entity_share_id = validated_data.get('entity_share_id', instance.entity_share_id)
        instance.company_name = validated_data.get('company_name', instance.company_name)
        instance.company_shortname = validated_data.get('company_shortname', instance.company_shortname)
        instance.address1 = validated_data.get('address1', instance.address1)
        instance.address2 = validated_data.get('address2', instance.address2)
        instance.address3 = validated_data.get('address3', instance.address3)
        instance.country_ref_id = validated_data.get('country_ref_id', instance.country_ref_id)
        instance.state_ref_id = validated_data.get('state_ref_id', instance.state_ref_id)
        instance.city_ref_id = validated_data.get('city_ref_id', instance.city_ref_id)
        instance.location_ref_id = validated_data.get('location_ref_id', instance.location_ref_id)
        instance.company_type_ref_id = validated_data.get('company_type_ref_id', instance.company_type_ref_id)
        instance.ownership_status_ref_id = validated_data.get('ownership_status_ref_id', instance.ownership_status_ref_id)
        instance.cin = validated_data.get('cin', instance.cin)
        instance.pan = validated_data.get('pan', instance.pan)
        instance.tan = validated_data.get('tan', instance.tan)
        instance.gst = validated_data.get('gst', instance.gst)
        instance.email = validated_data.get('email', instance.email)
        instance.contact_person_name = validated_data.get('contact_person_name', instance.contact_person_name)
        instance.is_holding_company = validated_data.get('is_holding_company', instance.is_holding_company)
        instance.belongs_to_company_id = validated_data.get('belongs_to_company_id', instance.belongs_to_company_id)
        instance.is_this_under_same_management = validated_data.get('is_this_under_same_management', instance.is_this_under_same_management)
        instance.management_belongs_to_company_id = validated_data.get('management_belongs_to_company_id', instance.management_belongs_to_company_id)
        instance.percentage_holding = validated_data.get('percentage_holding', instance.percentage_holding)
        instance.is_group_company = validated_data.get('is_group_company', instance.is_group_company)
        instance.contact_person_mobile_number = validated_data.get('contact_person_mobile_number', instance.contact_person_mobile_number)
        instance.is_this_branch = validated_data.get('is_this_branch', instance.is_this_branch)
        instance.pincode = validated_data.get('pincode', instance.pincode)
        instance.revision_status = validated_data.get('revision_status', instance.revision_status)
        instance.biller_category_ref_id=validated_data.get('biller_category_ref_id', instance.biller_category_ref_id)
        instance.verification_comments = validated_data.get('verification_comments', instance.verification_comments)
        instance.sub_application_id = validated_data.get('sub_application_id', instance.sub_application_id)
        instance.application_id = validated_data.get('application_id', instance.application_id)
        instance.is_deleted = validated_data.get('is_deleted', instance.is_deleted)
        instance.updated_date_time = validated_data.get('updated_date_time', instance.updated_date_time)
        instance.updated_by = validated_data.get('updated_by', instance.updated_by)
        instance.save()

        keep_details = []
        for init in KYC_initialItemRow:
            if "id" in init.keys():
                if tbl_kyc_document_details.objects.filter(id=init['id']).exists():
                    det = tbl_kyc_document_details.objects.get(id=init['id'])
                    det.biller_ref_id = init.get('biller_ref_id',det.biller_ref_id)
                    det.kyc_doc_ref_id = init.get('kyc_doc_ref_id',det.kyc_doc_ref_id)
                    det.kyc_document_path = init.get('kyc_document_path',det.kyc_document_path)
                    det.is_active = init.get('is_active',det.is_active)
                    det.is_deleted = init.get('is_deleted',det.is_deleted)
                    det.created_date_time = init.get('created_date_time',det.created_date_time)
                    det.created_by = init.get('created_by',det.created_by)
                    det.updated_date_time = init.get('updated_date_time',det.updated_date_time)
                    det.updated_by = init.get('updated_by',det.updated_by)
                    det.sub_application_id = init.get('sub_application_id',det.sub_application_id)
                    det.application_id = init.get('application_id',det.application_id)
                    det.save()
                    keep_details.append(det.id)
                else:
                    continue
            else:
                det = tbl_kyc_document_details.objects.create(**init,biller_ref_id=instance)
                keep_details.append(det.id)

        det = tbl_kyc_document_details.objects.filter(biller_ref_id=instance)
        det_id = [d.id for d in det]
        for d in det_id:
            if d in keep_details:
                continue
            else:
                det_record = tbl_kyc_document_details.objects.get(id=d)
                det_record.is_deleted = 'Y'
                det_record.is_active = 'N'
                det_record.save()                        
        return instance