from django.urls import include, path
from rest_framework import routers
from . import views
from drf_jwt_2fa.views import obtain_auth_token, obtain_code_token, refresh_auth_token
from django.urls.conf import re_path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import file_upload

router = routers.DefaultRouter()
router.register(r'state', views.StateViewSet)
router.register(r'country_Currency',views.CountryCurrencyViewSet)
router.register(r'city', views.CityViewSet)
router.register(r'location', views.LocationViewSet, basename='location')
router.register(r'company', views.CompanyViewSet)
router.register(r'user', views.UserViewSet, basename='user')
router.register(r'login', views.tbl_login_mst_view, basename='login')
router.register(r'master', views.MasterViewSet, basename='master')
router.register(r'uom', views.UOMViewSet)
router.register(r'reason', views.tbl_reason_mst_view)
router.register(r'channel', views.ChannelViewset, basename='channel')
router.register(r'define-api', views.DefineApisMasterView, basename='Define Api')
router.register(r'source', views.SourceMasterView, basename='source')
router.register(r'standardapimaster', views.tbl_api_definition_standard_mst_view)
router.register(r'standardapidetails', views.tbl_api_definition_standard_details_view)
router.register(r'roleMst', views.tbl_role_mst_view)
router.register(r'role_master', views.tbl_role_master_view)
router.register(r'employee', views.EmployeeViewSet)
router.register(r'workflowmst', views.tbl_workflow_mst_view)
router.register(r'productmst', views.tbl_product_mst_view)
router.register(r'streammst', views.tbl_stream_mst_view)
router.register(r'channelmst', views.tbl_channel_mst_view)
router.register(r'bbpoubankmst', views.tbl_bbpou_bank_mst_view)
router.register(r'bbpoubankandbillermst', views.tbl_biller_bank_link_mst_view, basename='bbpoubankandbillermst')
router.register(r'bank', views.BankMasterView, basename='Bank Master')
router.register(r'companyMst', views.tbl_company_mst_view)
router.register(r'dynamicfields', views.tbl_dynamic_table_fields_mst_view)
router.register(r'reasoncodemst',views.tbl_reason_code_mst_view)
router.register(r'taxauthoritymst',views.tbl_tax_authority_mst_view)
router.register(r'taxratemst',views.tbl_tax_rate_mst_view)
router.register(r'entityrelation', views.tbl_entity_relationship_mst_view)
router.register(r'billercategorydata', views.tbl_biller_category_mst_view)
router.register(r'paymentfile', views.PaymentfileViewSet, basename = 'paymentfile')
#Biller Invoice Master
router.register(r'billerinvoice', views.tbl_biller_invoice_mst_view, basename='Biller Invoice')
# Transaction URLS
router.register(r'endustomermst', views.tbl_end_customer_mst_view)
router.register(r'bbpstransactionmst', views.tbl_bbps_transaction_mst_view)
router.register(r'kycdocumentmst', views.tbl_kyc_document_mst_view)

router.register(r'fees_code_data', views.tbl_fees_code_mst_view)
router.register(r'interchange_fee', views.tbl_interchange_fee_mst_view)
router.register(r'biller_bank_charges', views.tbl_biller_bank_charges_mst_view)
router.register(r'platform_charges', views.tbl_platform_charges_mst_view)
# Manage Security URLs
router.register(r'assign-user-roles', views.AssignUserRolesViewSet)
router.register(r'assign-user-roles-details', views.AssignUserRolesDetailsViewSet)
router.register(r'left-panel', views.LeftPanelViewSet)
router.register(r'assign-roles', views.AssignPagesRolesViewSet)
router.register(r'employee-to-role', views.AssignUserRolesViewSet)
router.register(r'secureserverconfig', views.secure_server_config_mst_view)
#Partner charges URL
router.register(r'partner_charges', views.tbl_partner_charges_mst_view)
router.register(r'addbiller', views.AddBillerView, basename='Add Biller')

urlpatterns = [
    path('', include(router.urls)),
    path('auth/code/', obtain_code_token),
    path('auth/login/', obtain_auth_token),
    path('auth/refresh-token/', refresh_auth_token),
    path('auth/logout/',views.LogoutView.as_view(),name='logout'),
    path(r'reset-password/', views.TokenResetPassword, name="auth reset-password"),         
    path(r'key-verification/', views.KeyVerificationUpadatePassword, name="key-verification"),
    re_path('master/oftype/(?P<master_type>.+)/$', views.MasterViewSet.as_view({'get' : 'list'})),
    re_path('companyMst/oftype/(?P<company_id>.+)/(?P<company_type>.+)/$', views.tbl_company_mst_view.as_view({'get' : 'list'})),
    re_path('companyMstP/oftype/(?P<company_id>.+)/(?P<company_type>.+)/(?P<partner_id>.+)/$', views.tbl_company_mst_view.as_view({'get' : 'list_Part'})),
    re_path('aggregatorcompanyMst/(?P<company_id>.+)/(?P<company_type>.+)/$', views.tbl_company_mst_view.as_view({'get' : 'aggregator_list'})),    
    re_path('partnerMst/oftype/(?P<company_id>.+)/(?P<company_type>.+)/$', views.tbl_company_mst_view.as_view({'get' : 'partner_list'})),
    re_path('companylistMst/', views.tbl_company_mst_view.as_view({'get' : 'company_list'})),    
    re_path('bankmst/oftype/(?P<company_id>.+)/$', views.BankMasterView.as_view({'get' : 'list'})),
    re_path('banklinkmst/oftype/(?P<company_id>.+)/(?P<company_type>.+)/$', views.BankMasterView.as_view({'get' : 'list'})), 
    re_path('employee/oftype/(?P<company_id>.+)/$', views.EmployeeViewSet.as_view({'get' : 'list'})),
    path(r'accesstablenames/', views.accesstablenames.as_view(), name="accesstablenames"),
    path(r'accesstablefieldname/', views.accessfieldname.as_view(), name="accesstablefieldname"),
    path(r'add_biller/', views.add_biller_view.as_view(), name='add_biller'),
    path(r'source/dynamic',views.dynamic_table,name='dynamic'),
    path(r'add_biller/<int:pk>/', views.add_biller_view.as_view(), name='add_biller'),
    path(r'roleMstEmp/', views.get_role_ref_id_view.as_view(), name='roleMstEmp'),
    path(r'getcompanyname/', views.company_name_view.as_view(), name='company_name_view'),
    path(r'product_mst_label/', views.getproduct_mst_labelData.as_view(), name = "product_mst_label"),
    path(r'companyMstData/', views.get_company_data_acc_createdby.as_view(), name='companyMstData'),
    path(r'companyDataacctobiller/', views.get_company_data_of_biller.as_view()),
    path(r'bankdataacctouser/', views.get_bank_data_of_user.as_view(), name='bankData'),
    path(r'bankdataacctobiller/', views.get_bank_data_of_biller.as_view(), name='bankData'),
    path(r'general_mst_label/<app_label>/<model_name>', views.GeneralViewSet.as_view({'get': 'list'})),
    path(r'billercategory/', views.getBillersCategoryData.as_view(), name="billercategory"),
    path(r'companyTypeMstData/', views.get_company_data_acc_company_type.as_view(), name='companyTypeMstData'),    
    path(r'billersdata/', views.getBillersData.as_view(), name="billersdata"),
    path(r'filetype/',views.fileTypeViewSet.as_view(),name='File_Type'),
    path(r'upload/', file_upload.upload_file_data_view.as_view(), name="upload-file-data-view"),
    path(r'insertData/',views.insertDataViewSet.as_view(), name='Insert Data'),
    path(r'downloadStructure/',views.DownloadBillStructureViewSet.as_view(),name='Download Bill Structure'),
    path(r'categoryandownershipdata/', views.getCategoryAndDocumentData.as_view(), name="categoryandownershipdata"),
    re_path('employeeacccompany/', views.get_employee_acc_company.as_view()),
    re_path('get_screen_to_role/',views.get_screen_to_role.as_view()),
    re_path('get_data/',views.get_screen_data.as_view()),
    path('sidebar_biller/', views.sidebar_biller_leftpanel.as_view()),
    path(r'billerTransactionData/', views.GetBillerTransactionDataViewSet.as_view(), name='Biller Transaction Data'),
                                                                                  
    # Biller OU AIP's
    path(r'access_token/', views.AccessTokenOUBank, name='Access Token'),
    url(r'biller_update/',views.GetBillerDataApi.as_view(), name="Get Data From Biller"),
    url(r'fetch_bill/', views.FetchBillApiViewSet.as_view(), name='Fetch Bill'),
    url(r'pay_response/', views.PayBillResponseApiViewSet.as_view(), name='Bay Response'),
    path(r'payment_status/', views.paymentStatus, name='Payment Status'),
                                                                      
    url(r'^ResponseParametrs/([-a-zA-Z0-9_]*)$',views.ResponseParametrs),
    url(r'^BillStatus/([-a-zA-Z0-9_]*)$',views.BillStatus),
    path(r'chargesDataacctobiller/', views.get_charges_data_of_biller.as_view(), name='ChargesDataBiller'),
    path(r'getInvoiceNumber/', views.GetUniqueInvoiceNumberViewSet.as_view(), name='GetInvoiceNumber'),
    path(r'getInvoiceCompany/', views.GetCompanyDataForInvoice.as_view(), name='GetInvoiceCompany'),    
   
    # Dashboard
    path('not_recon/', views.GetNotReconciled.as_view(), name="Get Not Reconciled"),
    path('value_chart/', views.GetTransactionChart.as_view(), name="value chart"),
    path('total_coll/', views.GetTotalCollection.as_view(), name="Get Total Collection"),
    path('bank_bal/', views.GetBankBalance.as_view(), name="Get Bank Balance"),
    path('outstanding/', views.GetOutstanding.as_view(), name="Get Outstanding"),
    path('total_bill/', views.GetNoOfBills.as_view(), name="Get No Bills"), 
    
    path('bbps_data_to_payment_file/', views.BbpsDataForPaymentfileViewSet.as_view(), name="bbps_data_to_payment_file"),
    path('percenge_data_for_payment_file/', views.PercengedataforpaymentfileViewSet.as_view(), name="percenge_data_for_payment_file"),
    path(r'GetPaymentDetailsTabledata/', views.GetPaymentDetailsTabledata.as_view(), name='GetPaymentDetailsTabledata'),
    path(r'get_biller_report_data', views.GetBillerReportData.as_view(), name='getbillerreportdata'),
    re_path('get_table_name/',views.get_table_name_data.as_view()), 
    path(r'get_left_panel_data/',views.get_left_panel_data.as_view()),
    path(r'left_pannel_access/',views.getLeftPanelAccessData.as_view(), name='left Pannel Access'),
    re_path('get_employee_name_data/',views.get_employee_name_data.as_view()), 
    path(r'user_as_company/',views.user_as_company.as_view()),
    path(r'assign_to_screen/<int:id>/', views.tbl_assign_screen_to_role_mst_view.as_view(), name='assign_to_screen'),
    path(r'assign_to_screen/', views.tbl_assign_screen_to_role_mst_view.as_view(), name='assign_to_screen'),
    path(r'upload_credential_file/', file_upload.upload_credential_file_data_view.as_view(), name="upload-credential-file-data-view"),
    path(r'tax/',views.tax_view.as_view()),
    path(r'bbpoubankAggregator/',views.bbpoubanksAsPerAggregator.as_view()),
    path('onboard_type_for_invoice_file/',views.OnboardTypeforinvoicefileViewSet.as_view(), name='onboard_type_for_invoice_file'),
    path(r'uploadKycFileData/', views.UploadFileKYC.as_view(), name="upload"),
    path(r'getKycFilePath/', views.FetchUplodedFile.as_view(), name="getuploadFile"),
    path(r'commissionReport', views.CommisionReportDataView.as_view(), name="commission Report"),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)