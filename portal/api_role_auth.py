from rest_framework.permissions import BasePermission
from BBPS_Biller.settings import CORS_ORIGIN_WHITELIST
from portal.models import ApiRole
from .serializers import AssignUserRolesSerializer, UserSerializer
from django.contrib.auth.models import AnonymousUser

class GenericRolePermission(BasePermission):
    """
    Allow access as per the DB privilege
    """

    def has_permission(self, request, view):
        url: str = request.path_info[1:-1]
        url = url.split("/")[0]
        method = request.method
        uid = request.user
        if type(uid) is AnonymousUser:
            uid = -1  # TODO Add Anonymous user auth

        if request.get_full_path() == '/fetch_bill/' or request.get_full_path() == '/pay_response/' or request.get_full_path() == '/biller_update/':
            if UserSerializer.get(request.user).is_active:
                return True
            return False
        else:
            try:
                request_access_token = request.headers.get('Authorization')
                if request_access_token is not None and 'JWT' in request_access_token:
                    if request.headers.get('Origin') in CORS_ORIGIN_WHITELIST:
                        role_ids = AssignUserRolesSerializer.get(request.user).values_list("assigned_to_role_ref_id", flat=True)
                        allowed_methods_list = ApiRole.objects.filter(api_url=url, role_id_id__in=role_ids).values_list("allowed_methods", flat=True)        
                        for allowed_methods in allowed_methods_list:
                            if method in allowed_methods:
                                return True
                        return False
                    else:
                        return False
                else:
                    return False
            except:
                return False