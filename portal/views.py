from django.shortcuts import get_object_or_404
from rest_framework import viewsets,status,generics
from .models import *
from .serializers import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from rest_framework.response import Response
from url_filter.integrations.drf import DjangoFilterBackend
from rest_framework.views import APIView
import json
from BBPS_Biller.settings import EMAIL_HOST_USER, DATABASES, TIME_ZONE, OU_AGENT_ID, OU_AGENT_KEYWORD

from sqlalchemy import create_engine,Column, Integer, DECIMAL, String, Boolean, BigInteger, Date
engine = create_engine('postgresql://'+ DATABASES['default']['USER'] +':'+ DATABASES['default']['PASSWORD'] +'@'+ DATABASES['default']['HOST'] +':'+ DATABASES['default']['PORT'] +'/'+ DATABASES['default']['NAME'], echo=False)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
Base = declarative_base()

from django.http.response import JsonResponse
from django.apps import apps
from rest_framework.parsers import JSONParser
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
import pandas as pd
import numpy as np
from pathlib import Path
import shutil
import os
import re
from datetime import date,datetime
from django.core.files.storage import default_storage
from BBPS_Biller import settings
import pysftp
from io import StringIO
from django.db import transaction
from django.db import connection
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken, BlacklistedToken
import random,string
import pytz
import base64 
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from cron_script.transaction_status import CheckStatus


Session = sessionmaker(bind=engine)
session = Session()

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

# Create your views here.

class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = tbl_company_mst.objects.filter(is_deleted='N').order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['company_type_ref_id']

    def delete(self):
        self.queryset = tbl_company_mst.objects.filter(self=self).update(is_active='N')
class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class tbl_login_mst_view(viewsets.ModelViewSet):
    serializer_class = LoginMasterSerializer
    queryset = tbl_login_mst.objects.all()
class StateViewSet(viewsets.ModelViewSet):
    serializer_class = StateSerializer
    queryset = tbl_state_mst.objects.filter(is_deleted='N', is_active='Y').order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['country_ref_id']

class CountryCurrencyViewSet(viewsets.ModelViewSet):
    queryset=tbl_country_currency_mst.objects.filter(is_deleted='N').order_by('-id')
    serializer_class=CountryCurrencySerializer

    def delete(self):
        self.queryset = tbl_country_currency_mst.objects.filter(self=self).update(is_deleted='Y')


class CityViewSet(viewsets.ModelViewSet):
    serializer_class = CitySerializer
    queryset = tbl_city_mst.objects.filter(is_deleted='N', is_active='Y').order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['state_ref_id']

class LocationViewSet(viewsets.ModelViewSet):
    serializer_class = LocationSerializer
    queryset = tbl_location_mst.objects.all().filter(is_deleted='N').order_by('-id')

    def delete(self):
        self.queryset = tbl_location_mst.objects.filter(self=self).update(is_deleted='Y')

class tbl_reason_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_reason_mst_serializers
    queryset = tbl_reason_code_mst.objects.all().filter(is_deleted='N').order_by('-id')

    def delete(self):
        self.queryset = tbl_reason_code_mst.objects.filter(self=self).update(is_deleted='Y')

class UOMViewSet(viewsets.ModelViewSet):
    serializer_class = UOMSerializer
    queryset = tbl_uom_mst.objects.filter(is_deleted='N').order_by('-id')

    def delete(self):
        self.queryset = tbl_uom_mst.objects.filter(self=self).update(is_deleted='Y')
#------------------
class MasterViewSet(viewsets.ModelViewSet):
    serializer_class = MasterSerializer
    queryset = tbl_master.objects.filter(is_deleted='N', is_active='Y').order_by('-id')

    def list(self, request, master_type=None):
        if master_type:
            master = tbl_master.objects.filter(master_type = master_type, is_deleted='N', is_active='Y').order_by('-id')
            serializer = self.get_serializer(master, many=True)
            return Response(serializer.data)
        else:
            currency = tbl_master.objects.filter(is_deleted='N', is_active='Y').order_by('-id')
            serializer = self.get_serializer(currency, many=True)
            return Response(serializer.data)

class ChannelViewset(viewsets.ModelViewSet):
    queryset = tbl_channel_mst.objects.filter(is_deleted='N')
    serializer_class = ChannelSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['company_id']       

class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = tbl_employee_mst.objects.filter(is_deleted='N').order_by('-id')

    def list(self, request, company_id=None):
        data = tbl_employee_mst.objects.filter(is_deleted='N',company_ref_id=company_id).order_by('-id')
        serializer_result = self.get_serializer(data, many=True)
        return Response(serializer_result.data,)

class ActivityViewSet(viewsets.ModelViewSet):
    serializer_class = ActivitySerializer
    queryset = tbl_workflow_activity_mst.objects.filter(is_deleted='N')

class DefineApisMasterView(viewsets.ModelViewSet):
    serializer_class = DefineApisMasterSerializer
    queryset = tbl_api_definition_mst.objects.filter(is_deleted=False, is_active=True).order_by('-id')

    def list(self, request):
        login_data = LoginMasterSerializer.get(request.user)
        tenant_obj = tbl_api_definition_mst.objects.filter(parent_tenant_id=login_data.company_id_id, is_deleted=False, is_active=True, revision_status='Effective').order_by('-id')
        serializer = self.get_serializer(tenant_obj, many=True)
        return Response(serializer.data)

    def create(self, request):
        with transaction.atomic():
            login_data = LoginMasterSerializer.get(request.user)
            validated_data = dict(request.data)
            validated_data['created_by'] = request.user.id
            validated_data['created_date_time']= datetime.now(pytz.timezone(TIME_ZONE))
            validated_data['parent_tenant_id']= login_data.company_id_id

            for item in validated_data['api_definition_details']:
                item.update({'created_by':request.user.id,'created_date_time':datetime.now(pytz.timezone(TIME_ZONE))})

            for item in validated_data['api_definition_sub_details']:
                item.update({'created_by':request.user.id,'created_date_time':datetime.now(pytz.timezone(TIME_ZONE))})

            serializer = DefineApisMasterSerializer(data= validated_data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":"Record Created Successfully"},status=status.HTTP_201_CREATED)
            else:
                return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None, partial=True):
        with transaction.atomic():
            login_data = LoginMasterSerializer.get(request.user)
            api_definition_obj = tbl_api_definition_mst.objects.get(id=pk)
            validated_data = dict(request.data)
            validated_data['created_by'] = request.user.id
            validated_data['created_date_time']= datetime.now(pytz.timezone(TIME_ZONE))
            validated_data['parent_tenant_id']= login_data.company_id_id

            for item in validated_data['api_definition_details']:
                del item['id']
                item.update({'created_by':request.user.id,'created_date_time':datetime.now(pytz.timezone(TIME_ZONE))})

            for item in validated_data['api_definition_sub_details']:
                del item['id']
                item.update({'created_by':request.user.id,'created_date_time':datetime.now(pytz.timezone(TIME_ZONE))})
            
            serializer = DefineApisMasterSerializer(api_definition_obj, data=validated_data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":"Record Updated Successfully"},status=status.HTTP_201_CREATED)
            else:
                return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)


class tbl_dynamic_table_fields_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_dynamic_table_fields_mst_serializer
    queryset = tbl_dynamic_table_fields_mst.objects.filter(is_deleted='N')

# Function to create the dynamic table
def api_sql_format(data):
    class User(Base):
        __tablename__ = data['table_name']
        Flag=False
        schema = data['schema']

        for s in schema:
            if s['field_data_type']=='1':
               
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(BigInteger(),primary_key=True)
                else:
                    
                    vars()[s['field_name']] = Column(BigInteger())    
            elif s['field_data_type']=='2':
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(String(int(s['field_length'])),primary_key=True)
                else:
                        vars()[s['field_name']] = Column(String(int(s['field_length'])))
            elif s['field_data_type']=='3':
                vars()[s['field_name']] = Column(Boolean)
            elif s['field_data_type']=='4':
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(DECIMAL(s['field_length']),primary_key=True)
                else:
                        vars()[s['field_name']] = Column(DECIMAL(s['field_length']))
                      
            elif s['field_data_type']=='5':
                    vars()[s['field_name']] = Column(Date)

        if not Flag:
            id = Column(Integer, primary_key=True)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)

def sql_format(data):
    class User(Base):
        __tablename__ = data['table_name']
        
        Flag=False
        schema = data['schema']

        for s in schema:
            if s['field_data_type']=='1':
               
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(BigInteger(),primary_key=True)
                else:
                    
                    vars()[s['field_name']] = Column(BigInteger())    
            elif s['field_data_type']=='2':
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(String(int(s['field_length'])),primary_key=True)
                else:
                        vars()[s['field_name']] = Column(String(int(s['field_length'])))
            elif s['field_data_type']=='3':
                vars()[s['field_name']] = Column(Boolean)
            elif s['field_data_type']=='4':
                if s['is_primary_key']:
                    Flag=True
                    vars()[s['field_name']] = Column(DECIMAL(s['field_length']),primary_key=True)
                else:
                        vars()[s['field_name']] = Column(DECIMAL(s['field_length']))
                      
            elif s['field_data_type']=='5':
                    vars()[s['field_name']] = Column(Date)

        if not Flag:
            id = Column(Integer, primary_key=True)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)

@csrf_exempt
def dynamic_table(request):
    if request.method == "POST":
        with transaction.atomic():
            tables = json.loads(request.body)
            for data in tables:
                schema = data['schema']

                for s in schema:
                    mst_object = tbl_master.objects.get(id = s['field_data_type'])
                    s['field_data_type'] = mst_object.master_value

                b_payment_amount = {'field_name': 'b_payment_amount', 'field_data_type': '4', 'field_length': '10,2', 'is_primary_key': False}
                data['schema'].append(b_payment_amount)  
                details_id = {'field_name': 'details_id', 'field_data_type': '1', 'field_length': '10', 'is_primary_key': False}       
                data['schema'].append(details_id)
                created_date =  {'field_name': 'created_date', 'field_data_type': '5', 'field_length': '10', 'is_primary_key': False}   
                data['schema'].append(created_date)
                updated_date =  {'field_name': 'updated_date', 'field_data_type': '5', 'field_length': '10', 'is_primary_key': False}   
                data['schema'].append(updated_date)

                if data:
                    try:
                        sql_format(data)
                    except:
                        api_sql_format(data)

            return JsonResponse({"Success":True})
 
class tbl_api_definition_standard_details_view(viewsets.ModelViewSet):
    serializer_class = tbl_api_definition_standard_details_serializer
    queryset = tbl_api_definition_standard_details.objects.filter(is_deleted='N')

class tbl_api_definition_standard_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_api_definition_standard_mst_serializer
    queryset = tbl_api_definition_standard_mst.objects.filter(is_deleted='N').order_by('-id')    

class tbl_role_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_role_mst_serializer
    queryset = tbl_role_mst.objects.filter(is_deleted='N')

class tbl_role_master_view(viewsets.ModelViewSet):
    serializer_class = tbl_role_mst_serializer
    queryset = tbl_role_mst.objects.filter(is_deleted='N')

class accesstablenames(APIView):
    def get(self, request): 
        cursor = connection.cursor()
        cursor.execute("""SELECT table_name FROM information_schema.tables
                            WHERE table_schema = 'public'
                            ORDER BY table_name
                        """)
        tablenames = dictfetchall(cursor)
        cursor.close()
        return Response(tablenames, status=status.HTTP_201_CREATED)

class accessfieldname(APIView):
    def post(self, request):
        data = request.body.decode('utf-8')
        cursor = connection.cursor()
        cursor.execute("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = %s",[data])
        fieldnames = dictfetchall(cursor)
        cursor.close()
        return Response(fieldnames, status=status.HTTP_201_CREATED)        

class tbl_workflow_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_workflow_mst_serializer
    queryset = tbl_workflow_mst.objects.filter(is_deleted='N')        

# Add BBPOU Bank
class tbl_bbpou_bank_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_bbpou_bank_mst_serializer
    queryset = tbl_bbpou_bank_mst.objects.filter(is_deleted='N', is_active='Y').order_by('-id')

# Stream Master
class tbl_stream_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_stream_mst_serializer
    queryset = tbl_stream_mst.objects.filter(is_deleted='N')
# Channel Master
class tbl_channel_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_channel_mst_serializer
    queryset = tbl_channel_mst.objects.filter(is_deleted='N')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['stream_ref_id']

# Product Master
class tbl_product_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_product_mst_serializer
    queryset = tbl_product_mst.objects.filter(is_deleted='N')

# Link BBPOU Bank with Biller
class tbl_biller_bank_link_details_view(viewsets.ModelViewSet):
    serializer_class = tbl_biller_bank_link_details_serializer
    queryset = tbl_biller_bank_link_details.objects.filter(is_deleted='N')

class tbl_biller_bank_link_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_biller_bank_link_mst_serializer
    queryset = tbl_biller_bank_link_mst.objects.filter(is_deleted='N', is_active='Y').order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['biller_ref_id']

#Bank
class BankMasterView(viewsets.ModelViewSet):
    serializer_class = BankSerializer
    queryset = tbl_bank_mst.objects.filter(is_deleted='N').order_by('-id')

    def list(self, request, company_id=None, company_type=None):
        if company_id and company_type==None:
            data = tbl_bank_mst.objects.filter(is_deleted='N',company_ref_id=company_id).order_by('-id')
            serializer_result = self.get_serializer(data, many=True)
            return Response(serializer_result.data)
        
        if company_type == 'Biller':
            data = tbl_bank_mst.objects.filter(is_deleted='N',company_ref_id=company_id).order_by('-id')
            serializer_result = self.get_serializer(data, many=True)
            return Response(serializer_result.data)
        
        if company_type == 'Aggregator':
            company_id = tbl_entity_relationship_mst.objects.filter(entity_ref_id=company_id,is_deleted='N',is_active='Y').order_by('-id').values_list('company_ref_id')
            company_id = [item for tup in company_id for item in tup]
            data = tbl_bank_mst.objects.filter(is_deleted='N',company_ref_id__in=company_id).order_by('-id')
            serializer_result = self.get_serializer(data, many=True)
            return Response(serializer_result.data)
       

# view class is to be removed --- #
class tbl_company_mst_view(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = tbl_company_mst.objects.filter(is_deleted='N').order_by('-id')

    def list(self, request, company_id=None, company_type=None):
        if company_type == 'Aggregator':
            role_data = tbl_role_mst.objects.get(role_name='Biller', is_deleted='N', is_active='Y')
            company = tbl_company_mst.objects.filter(company_type_ref_id=role_data.id, is_deleted='N', is_active='Y').order_by('-id')
            serializer_result = TenentNameSerializer(company, many=True)
            return Response(serializer_result.data, status=status.HTTP_200_OK)

        if company_type == 'Biller':
            company= tbl_company_mst.objects.filter(pk=company_id, is_deleted='N', is_active='Y')
            serializer_result = TenentNameSerializer(company, many=True)
            return Response(serializer_result.data, status=status.HTTP_200_OK)

        if company_type == 'Aggregator Partner':
            company= tbl_company_mst.objects.filter(partner_company_ref_id=company_id, is_deleted='N', is_active='Y')
            serializer_result = TenentNameSerializer(company, many=True)
            return Response(serializer_result.data, status=status.HTTP_200_OK)
    
    def list_Part(self, request, **kwargs):
        company_type = self.kwargs['company_type']
        if company_type == 'Aggregator' or company_type == 'Biller-OU':
            role_data = tbl_role_mst.objects.get(role_name='Biller',is_deleted='N',is_active='Y')
            if self.kwargs['partner_id']:
                company= tbl_company_mst.objects.filter(company_type_ref_id=role_data.id,is_deleted='N',is_active='Y',partner_company_ref_id=self.kwargs['partner_id']).order_by('-id')
            else:
                company= tbl_company_mst.objects.filter(company_type_ref_id=role_data.id,is_deleted='N',is_active='Y').order_by('-id')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)
        if company_type == 'Biller':
            company= tbl_company_mst.objects.filter(pk=self.kwargs['company_id'],is_deleted='N',is_active='Y')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)
        if company_type == 'Aggregator Partner':
            company= tbl_company_mst.objects.filter(partner_company_ref_id_id=self.kwargs['company_id'],is_deleted='N',is_active='Y')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)
    
    def aggregator_list(self, request, company_id=None,company_type=None):   
         if company_type == 'Aggregator':
            company= tbl_company_mst.objects.filter(pk=company_id,is_deleted='N',is_active='Y')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)            
         if company_type == 'Biller-OU':
            role_data = tbl_role_mst.objects.get(role_name='Aggregator',is_deleted='N',is_active='Y')
            company= tbl_company_mst.objects.filter(company_type_ref_id=role_data.id,is_deleted='N',is_active='Y').order_by('-id')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data) 

    def partner_list(self, request,company_id=None,company_type=None):
        if company_type == 'Aggregator' or company_type== 'Aggregator User' :
            role_data = tbl_role_mst.objects.get(role_name='Aggregator Partner',is_deleted='N',is_active='Y')
            company= tbl_company_mst.objects.filter(company_type_ref_id=role_data.id,is_deleted='N',is_active='Y').order_by('-id')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data) 
         
        if company_type ==  'Aggregator Partner' or company_type ==  'Aggregator Partner User' :
            company= tbl_company_mst.objects.filter(pk=company_id,is_deleted='N',is_active='Y')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)
        
        if company_type ==  'Biller' or company_type ==  'Biller User' :
            company= tbl_company_mst.objects.filter(pk=company_id,is_deleted='N',is_active='Y')
            serializer = self.get_serializer(company, many=True)
            return Response(serializer.data)    

    def company_list(self, request):
        company= tbl_company_mst.objects.filter(is_deleted='N',is_active='Y')
        serializer = getCompanySerializer(company, many=True)
        return Response(serializer.data)   
    

class tbl_interchange_fee_details_view(viewsets.ModelViewSet):
    serializer_class = tbl_interchange_fee_details_serializer
    queryset = tbl_interchange_fee_details.objects.filter(is_deleted='N').order_by('-id')

class tbl_interchange_fee_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_interchange_fee_mst_serializer
    queryset = tbl_interchange_fee_mst.objects.filter(is_deleted='N').order_by('-id')

class tbl_biller_bank_charges_details_view(viewsets.ModelViewSet):
    serializer_class = tbl_biller_bank_charges_details_serializer
    queryset = tbl_biller_bank_charges_details.objects.filter(is_deleted='N').order_by('-id')

class tbl_biller_bank_charges_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_biller_bank_charges_mst_serializer
    queryset = tbl_biller_bank_charges_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')

class tbl_platform_charges_mst_view(viewsets.ModelViewSet):
    queryset = tbl_platform_charges_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')
    serializer_class = tbl_platform_charges_mst_serializer

class add_biller_view(APIView):
    
    def post(self, request, *args, **kwargs):
        company_serializer = tbl_company_mst_serializer(data=request.data['company_master_form'])
        plain_text_pass = request.data['userForm']['password']
        request.data['userForm']['password'] = make_password(request.data['userForm']['password'],hasher='default')
        user_serializer = UserSerializer(data=request.data['userForm'])

        if company_serializer.is_valid() and user_serializer.is_valid(): 
            company_serializer.save()
            user_serializer.save()
            request.data['entityRelationMstForm']['company_ref_id'] = company_serializer['id'].value
            request.data['entityRelationMstForm']['entity_ref_id'] = company_serializer['id'].value
            entity_serializer_1 = tbl_entity_relationship_mst_serializer(data=request.data['entityRelationMstForm'])
            if entity_serializer_1.is_valid():
                entity_serializer_1.save()

            request.data['entityRelationMstForm']['company_ref_id'] = company_serializer['id'].value
            request.data['entityRelationMstForm']['entity_ref_id'] = request.data['COMPANY_ID']
            entity_serializer_2 = tbl_entity_relationship_mst_serializer(data=request.data['entityRelationMstForm'])
            if entity_serializer_2.is_valid():
                entity_serializer_2.save()

            # request.data['roleMstForm']['company_ref_id'] = company_serializer['id'].value
            # role_serializer = tbl_role_mst_serializer(data=request.data['roleMstForm'])            
            # if role_serializer.is_valid():
            #     role_serializer.save()

            request.data['loginMasterForm']['company_id'] = company_serializer['id'].value
            # request.data['loginMasterForm']['role_ref_id'] = role_serializer['id'].value
            request.data['loginMasterForm']['user'] = user_serializer['id'].value
            login_serializer = tbl_login_mst_serializer(data=request.data['loginMasterForm'])
            if login_serializer.is_valid():
                login_serializer.save()

            # Send Mail Function
            subject = 'Welcome to NAS Digital, Your User ID is created'
            message = 'Namaste ' + request.data['userForm']['username'] + '\n\n' + 'Welcome to NAS Digital ' + 'With a motto of providing Nimble, Affordable and Secure Banking solutions, We’re super happy to see you on board!' + '\n\n' + 'We are sure that the NAS Collect platform will help you seamless experience for collecting your payment thru Bharat Bill Payment System.' +'\n\n' + 'Your User ID for NAS Collect is: ' + request.data['userForm']['username'] + '\n' + 'PASSWORD: ' + plain_text_pass + '\n\n' + 'You’ll be guided through our Support team, our expert in operation and support, to ensure that you get the very best out of our service.' + '\n\n' + 'Take care!' + '\n\n' + 'Warm Regards' + '\n\n' + 'Team - NAS Digital'
            recepient = request.data['company_master_form']['email']
            send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently = False)

            return Response(company_serializer.data, status=status.HTTP_201_CREATED)
        return Response(company_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk):
        saved_company = get_object_or_404(tbl_company_mst.objects.all(),pk=pk)
        company_serializer = tbl_company_mst_serializer(instance=saved_company, data=request.data['company_master_form'])
        if company_serializer.is_valid():
            company_serializer.save()
            return Response(company_serializer.data, status=status.HTTP_201_CREATED)
        return Response(company_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class tbl_kyc_document_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_kyc_document_mst_serializer
    queryset = tbl_kyc_document_mst.objects.filter(is_deleted='N')

class company_name_view(APIView):
    def get(self, request):
        serializer_class = User.objects.filter(username=request.GET['user_name'])
        breakpoint()

class get_employee_acc_company(APIView):
    def get(self, request):
        serializer_class = tbl_employee_mst.objects.filter(is_deleted='N').filter(company_ref_id=request.GET['company_ref_id'])
        serializer_result = tbl_employee_mst_serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_201_CREATED)

class get_role_ref_id_view(APIView):
    def get(self, request):
        serializer_class = tbl_role_mst.objects.filter(is_deleted='N').filter(company_ref_id=request.GET['COMPANY_ID'])
        serializer_result = tbl_role_mst_serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

# Payment Screen
class tbl_reason_code_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_reason_code_mst_serializer
    queryset = tbl_reason_code_mst.objects.filter(is_deleted='N')

class tbl_tax_authority_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_tax_authority_mst_serializer
    queryset = tbl_tax_authority_mst.objects.filter(is_deleted='N').order_by('-id')

    def delete(self):
        self.queryset = tbl_tax_authority_mst.objects.filter(self=self).update(is_deleted='Y')
    

class tbl_tax_rate_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_tax_rate_mst_serializer
    queryset = tbl_tax_rate_mst.objects.filter(is_deleted='N').order_by('-id')

    def delete(self):
        self.queryset = tbl_tax_rate_mst.objects.filter(self=self).update(is_deleted='Y')

class tbl_tax_rate_details_view(viewsets.ModelViewSet):
    serializer_class = tbl_tax_rate_details_serializer
    queryset = tbl_tax_rate_details.objects.filter(is_deleted='N')

class getproduct_mst_labelData(APIView):
    def get(self, request):
        library_result = tbl_product_mst.objects.all().filter(is_deleted='N')
        label_serializer = tbl_product_label_mst_serializer(library_result,many = True)
        return Response(label_serializer.data, status=status.HTTP_201_CREATED)

class get_company_data_acc_createdby(APIView):
    def get(self, request):
        serializer_class = tbl_company_mst.objects.all().filter(is_deleted='N').filter(created_by=request.GET['USER_ID'])
        serializer_result = tbl_company_mst_serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class get_company_data_of_biller(APIView):
    def get(self, request):
        serializer_class = tbl_company_mst.objects.all().filter(is_deleted='N').filter(id=request.GET['COMPANY_ID'])
        serializer_result = tbl_company_mst_serializer(serializer_class, many=True)        
        return Response(serializer_result.data, status=status.HTTP_200_OK)
class get_bank_data_of_user(APIView):
    def get(self, request):
        serializer_class = tbl_bank_mst.objects.all().filter(is_deleted='N').filter(created_by=request.GET['USER_ID'])
        serializer_result = BankSerializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class get_bank_data_of_biller(APIView):
    def get(self, request):
        serializer_class = tbl_bank_mst.objects.all().filter(is_deleted='N').filter(company_ref_id=request.GET['company_ref_id'])
        serializer_result = BankSerializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class get_company_data_acc_company_type(APIView):
    def get(self, request):
        serializer_class = tbl_company_mst.objects.all().filter(is_deleted='N').filter(created_by=request.GET['COMPANY_TYPE_REF_ID'])
        serializer_result = tbl_company_mst_serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class GeneralViewSet(viewsets.ModelViewSet):
    @property
    def model(self):
        return apps.get_model(app_label=str(self.kwargs['app_label']), model_name=str(self.kwargs['model_name']))

    def get_queryset(self):
        model = self.model
        return model.objects.all()[:1]           

    def get_serializer_class(self):
        GeneralSerializer.Meta.model = self.model
        return GeneralSerializer

# Entity Relationship Master
class tbl_entity_relationship_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_entity_relationship_mst_serializer
    queryset = tbl_entity_relationship_mst.objects.filter(is_deleted='N')
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['entity_ref_id']

# Transaction Views # .order_by('-id')
class tbl_end_customer_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_end_customer_mst_serializer
    queryset = tbl_end_customer_mst.objects.filter(is_deleted='N')

class tbl_bbps_transaction_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_bbps_transaction_mst_serializer
    queryset = tbl_bbps_transaction_mst.objects

class getBillersCategoryData(APIView):
    parser_classes = (JSONParser,)
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            cursor.execute("select * from tbl_get_biller_categories_api")
            billerCategory = dictfetchall(cursor)
            return Response(billerCategory, status=status.HTTP_200_OK)

class getBillersData(APIView):
    parser_classes = (JSONParser,)
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            x = str(request.GET['BlrCatId'])
            cursor.execute("select * from tbl_get_billers_api where """" "BlrCatId" = """ + "'" + x + "'")
            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory, status=status.HTTP_200_OK)
        else :
            billerCategory = {'status':False}
            return Response(billerCategory, status=status.HTTP_404_NOT_FOUND)

# join query for category and ownership
class getCategoryAndDocumentData(APIView):
    parser_classes = (JSONParser,)
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            cursor.execute("SELECT a.id as document_id, a.category_type_ref_id_id, a.ownership_ref_id_id, a.abb_document_name, b.id as master_id, b.master_key, b.master_value, b.master_type FROM public.portal_tbl_kyc_document_mst as a INNER JOIN public.portal_tbl_master as b ON a.category_type_ref_id_id = b.id AND a.ownership_ref_id_id = " + request.GET['ownership_ref_id'])
            kycDocData = dictfetchall(cursor)
            cursor.close()
            return Response(kycDocData, status=status.HTTP_200_OK)
        else:
            kycDocData = {'status':False}
            return Response(kycDocData, status=status.HTTP_404_NOT_FOUND)

class tbl_biller_category_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_biller_category_mst_serializer
    queryset = tbl_biller_category_mst.objects.filter(is_deleted='N').order_by('-cat_id')
    
class getbillername(APIView):
    def get(self,request):
        serializer_class = tbl_company_mst.objects.all().filter(is_deleted='N').filter(company_type_ref_id=2)
        serializer_result = CompanySerializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class tbl_fees_code_mst_view(viewsets.ModelViewSet):
    serializer_class = tbl_fees_code_mst_serializer
    queryset = tbl_fees_code_mst.objects.filter(is_deleted='N')

class SourceMasterView(viewsets.ModelViewSet):
    queryset = tbl_source_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')
    serializer_class = SourceMasterSerializer    

    def list(self, request):
        login_data = LoginMasterSerializer.get(request.user)
        tenant_obj = tbl_source_mst.objects.filter(is_deleted='N',is_active='Y', company_ref_id = login_data.company_id_id)
        serializer = self.get_serializer(tenant_obj, many=True)
        return Response(serializer.data)

    def create(self, request):
        with transaction.atomic():
            validated_data = dict(request.data)
            serializer = SourceMasterSerializer(data=validated_data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":"Record Created Successfully"},status=status.HTTP_201_CREATED)
            else:
                return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None, partial=True):
        tenant_obj = tbl_source_mst.objects.get(id=pk)
        validated_data = dict(request.data)
        
        serializer = SourceMasterSerializer(tenant_obj, data=validated_data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":"Record Updated Successfully"},status=status.HTTP_201_CREATED)
        else:
            return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)

class tbl_biller_invoice_mst_view(viewsets.ModelViewSet):
    queryset = tbl_biller_invoice_mst.objects.filter(is_deleted='N').order_by('-id')
    serializer_class = tbl_biller_invoice_mst_serializer

class tbl_source_details_view(viewsets.ModelViewSet):
    queryset = tbl_source_details.objects.filter(is_deleted='N').order_by('-id')
    serializer_class = SourceDetailsSerializer

    def retrieve(self, request, *args, **kwargs):
        header_ref_id = self.kwargs['pk']
        source_details = tbl_source_details.objects.filter(
            header_ref_id=header_ref_id, is_deleted='N')
        ser = SourceDetailsSerializer(source_details, many=True).data
        return Response(ser, status=status.HTTP_200_OK)

class fileTypeViewSet(APIView):
    def post(self, request, *args, **kwargs):
        try:
            company_id= request.data.get("company_id")
            filetypeid_serializers = tbl_source_mst.objects.all().filter(is_deleted='N').filter(company_ref_id=company_id)
            filetypeid_list = list(filetypeid_serializers)
            for row in filetypeid_list:
                filetype_id = row.file_type_ref_id.id

            filetype_serializers = tbl_master.objects.all().filter(is_deleted='N').filter(id=filetype_id)
            filetype_list = list(filetype_serializers)
            for row in filetype_list:
                file_type = row.master_key
            return Response(file_type,status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_200_OK)


class fileUploadViewSet(APIView):
    def post(self, request, *args, **kwargs):
        file = request.FILES['uploadedFile']
        my_file = Path('.//uploads/'+ file.name)       
        if my_file.is_file():
            filename, file_extension = os.path.splitext('.//uploads/'+ file.name)
            name = filename.split('/')[-1]
            modTimesinceEpoc = os.path.getmtime(my_file)
            modificationTime = date.today()
            version= 1
            duplicate_file = Path('.//uploads/'+ name +'_' + str(modificationTime)+ f'-V_{version}' + file_extension)
            while os.path.exists('.//uploads/'+ name +'_' + str(modificationTime)+ f'-V_{version}' + file_extension):
                version = version + 1
            
            os.rename(r'.//uploads/'+ file.name,r'.//uploads/'+ name +'_' + str(modificationTime)+ f'-V_{version}' + file_extension)
            shutil.move('.//uploads/'+ name +'_' + str(modificationTime)+ f'-V_{version}' + file_extension, './/uploads/') 
            file_name = default_storage.save(file.name,file)
            
        else:
            file_name = default_storage.save(file.name,file)
        return Response({'Success':'True'},status=status.HTTP_200_OK)

def get_source_details(source_name_ref_id,col_names):
    col_names = str(col_names)[1:-1] 
    col_names = col_names.replace("'",'')
    cursor = connection.cursor()
    cursor.execute(f"""select {col_names} from portal_tbl_source_mst where id ="""
                            + str(source_name_ref_id) +
                            """ and is_deleted= 'N' and is_active= 'Y' and revision_status = 'Effective'
                            """)
    source_query_result = cursor
    cursor.close()                     
    return source_query_result    

class insertDataViewSet(APIView):
    def post(self, request, *args, **kwargs):
        file_type= request.data.get('filetype')
        file_type = '.'+str(file_type).lower()
        company_id = request.data.get('company_id')    
        master_key = request.data.get('master_key')
        # con = engine.connect()
        cursor = connection.cursor()
        todays_date = datetime.now()
        todays_date = datetime.strftime(todays_date, '%Y-%m-%d')

        cursor.execute(f""" SELECT id, source_name, delimiter_type_ref_id, no_of_header_rows FROM portal_tbl_source_mst WHERE company_ref_id_id = {company_id} AND "is_active" = 'Y' AND "is_deleted" = 'N' AND is_temporary_flag = True AND from_date <= '{todays_date}' AND to_date >= '{todays_date}' LIMIT 1 """)
        delimiterid_list = tuple(cursor)

        if delimiterid_list:
            delimiterid_list = delimiterid_list[0]
            source_id = delimiterid_list[0]
            source_name = delimiterid_list[1]
            cursor.execute(f""" SELECT master_key FROM portal_tbl_master WHERE id = {delimiterid_list[2]} AND is_deleted='N' LIMIT 1 """)
            delimeter_type = tuple(cursor)[0][0]
            skip_header_val = delimiterid_list[3]
        else:            
            return Response('Bill Structure Not Permanently Save', status=status.HTTP_201_CREATED)
        try:
            biller_table_check = f'tbl_{source_name}_details'
            cursor.execute(f""" SELECT EXISTS ( SELECT FROM {biller_table_check} ) """)
            biller_table_check = cursor                
        except:
            return Response('Biller Table Does Not Exist', status=status.HTTP_201_CREATED)

        try:
            biller_table_check = f'tbl_{source_name}_tmp_details'
            cursor.execute(f""" SELECT EXISTS ( SELECT FROM {biller_table_check} ) """)
            biller_table_check = cursor               
        except:
            return Response('Biller Table Does Not Exist', status=status.HTTP_201_CREATED)

        if master_key=='SFTP':
            cursor.execute("SELECT b.host_bucket_name, b.username, b.password, b.file_key_path, b.biller_floder_storage_path FROM public.portal_tbl_secure_server_config_mst as a join public.portal_tbl_secure_server_config_details as b on a.id = b.header_ref_id_id where a.is_deleted = 'N'")
            secure_server = tuple(cursor)
            if secure_server:                
                ip_address = str(secure_server[0][0])
                username = str(secure_server[0][1])
                password = str(secure_server[0][2])
                private_key_path = str(secure_server[0][3])
                folder_name = str(secure_server[0][4])

                try:
                    if file_type == '.xlsx':
                        file_name = source_name + file_type
                    elif file_type == '.csv':
                        file_name = source_name + file_type
                    else:
                        file_name = source_name + file_type

                    parent_path = f'{settings.MEDIA_ROOT}/data'

                    if os.path.exists(settings.MEDIA_ROOT + '/data')==False:
                        parent_dir = settings.MEDIA_ROOT + '/data'
                        os.mkdir(parent_dir)

                    cnopts = pysftp.CnOpts()
                    cnopts.hostkeys = None
                    with pysftp.Connection(ip_address, username=username, password=password, private_key = private_key_path, port=8288, cnopts=cnopts) as sftp:
                        with sftp.cd(folder_name): 
                            if sftp.exists:
                                file_exist = sftp.isfile(f'./sftp/{file_name}')
                                if file_exist == False:
                                    return Response('File Not Found SFTP', status=status.HTTP_201_CREATED)

                                local_filename = f"{parent_path}/{file_name}"
                                sftp.get(f'./sftp/{file_name}', local_filename)                    
                except:
                    return Response('File Not Found SFTP', status=status.HTTP_201_CREATED)

            else:
                return Response('SFTP Credentials Not Found ', status=status.HTTP_201_CREATED)

        if file_type == '.xlsx':
            try:
                dataframe = pd.read_excel(f'{parent_path}/{file_name}', 
                                            skiprows=skip_header_val)
            except:
                dataframe = pd.read_csv(f'{parent_path}/{file_name}', 
                                            skiprows=skip_header_val)
            finally:
                file_exists = os.path.exists(f'{parent_path}/{file_name}')
                if file_exists == True:
                    os.remove(f'{parent_path}/{file_name}')

        elif file_type== '.csv':
            try:
                dataframe = pd.read_csv(f'{parent_path}/{file_name}',
                                    sep = delimeter_type, skiprows=skip_header_val, encoding='cp1252')
            except:
                dataframe = pd.read_csv(f'{parent_path}/{file_name}', sep = delimeter_type, skip_blank_lines=True, 
                                            skiprows=skip_header_val, engine='python')
                dataframe.dropna(how="all", inplace=True)
            finally:
                file_exists = os.path.exists(f'{parent_path}/{file_name}')
                if file_exists == True:
                    os.remove(f'{parent_path}/{file_name}')

        elif file_type == '.json':
            dataframe = pd.read_json(f'{parent_path}/{file_name}')

            file_exists = os.path.exists(f'{parent_path}/{file_name}')
            if file_exists == True:
                os.remove(f'{parent_path}/{file_name}')

        tbl_source_details = 'tbl_' + source_name + '_details'
        tbl_source_tmp_details = 'tbl_' + source_name + '_tmp_details'        

        cursor.execute(f""" SELECT COUNT(*) FROM {tbl_source_details} """)
        details_count = tuple(cursor)[0][0]

        def comma_checker(x):
            try:
                return x.replace(',', '')
            except:
                return x

        def float_column(x):
            try:
                float(x)
                return float(x)
            except:
                return np.nan
        def integer_column(x):
            try:
                return int(x)
            except:
                return np.nan

        def string_column(x):
            try:
                return str(x).split('.0')[0]
            except:
                return np.nan

        def boolean_column(x):
            try:
                if str(x) in ['true', 'false', 't', 'f', 'yes', 'no', 'y', 'n', '1', '0']:
                    return str(x)
                else:
                    return np.nan  
            except:
                return np.nan

        def date_column(x,date_col_format):
            x = str(x)
            date_format_dict = {
                "MM/DD/YYYY":"%m/%d/%Y","DD/MM/YYYY":"%d/%m/%Y","YYYY/MM/DD":"%Y/%m/%d",
                "MM-DD-YYYY":"%m-%d-%Y","DD-MM-YYYY":"%d-%m-%Y","YYYY-MM-DD":"%Y-%m-%d",
                "MMM-DD-YYYY":"%b-%d-%Y","DD-MMM-YYYY":"%d-%b-%Y","YYYY-MMM-DD":"%Y-%b-%d",
                "MMM/DD/YYYY":"%b/%d/%Y","DD/MMM/YYYY":"%d/%b/%Y","YYYY/MMM/DD":"%Y/%b/%d",
                "MMM DD YYYY":"%b %d %Y","DD MMM YYYY":"%d %b %Y","YYYY MMM DD":"%Y %b %d"
                }
            date_format = None
            for key, val in date_format_dict.items():
                if key == date_col_format:
                    date_format = val
            try:
                x = datetime.strptime(x, date_format).strftime('%Y-%m-%d')
            except:
                return np.nan
            return x

        def timestamp_column(x,timestamp_col_format):
            x = str(x)
            timestamp_format_dict = {
                "HH:MM:SS":"%H:%M:%S",
                "HH:MM":"%H:%M",
                "HH:MM:SS AM/PM":"%I:%M:%S %p",
                "HH:MM AM/PM":"%I:%M %p",
                "HH:MM:SSAM/PM":"%I:%M:%S%p",
                "HH:MMAM/PM":"%I:%M%p"
            }
            timestamp_format = None
            for key, val in timestamp_format_dict.items():
                if key == timestamp_col_format:
                    timestamp_format = val
                
            try:
                x=  pd.to_datetime(x, format=timestamp_format)                
            except:

                return np.nan
            return x

        def datetimestamp_column(x,date_time_col_format):
            x = str(x)
            date_time_format_dict = {

                "MM/DD/YYYY HH:MM:SS":"%m/%d/%Y %H:%M:%S","DD/MM/YYYY HH:MM:SS":"%d/%m/%Y %H:%M:%S","YYYY/MM/DD HH:MM:SS":"%Y/%m/%d %H:%M:%S",
                "MM-DD-YYYY HH:MM:SS":"%m-%d-%Y %H:%M:%S","DD-MM-YYYY HH:MM:SS":"%d-%m-%Y %H:%M:%S","YYYY-MM-DD HH:MM:SS":"%Y-%m-%d %H:%M:%S",
                "MMM-DD-YYYY HH:MM:SS":"%b-%d-%Y %H:%M:%S","DD-MMM-YYYY HH:MM:SS":"%d-%b-%Y %H:%M:%S","YYYY-MMM-DD HH:MM:SS":"%Y-%b-%d %H:%M:%S",
                "MMM/DD/YYYY HH:MM:SS":"%b/%d/%Y %H:%M:%S","DD/MMM/YYYY HH:MM:SS":"%d/%b/%Y %H:%M:%S","YYYY/MMM/DD HH:MM:SS":"%Y/%b/%d %H:%M:%S",
                "MMM DD YYYY HH:MM:SS":"%b %d %Y %H:%M:%S","DD MMM YYYY HH:MM:SS":"%d %b %Y %H:%M:%S","YYYY MMM DD HH:MM:SS":"%Y %b %d %H:%M:%S",

                "MM/DD/YYYY HH:MM":"%m/%d/%Y %H:%M","DD/MM/YYYY HH:MM":"%d/%m/%Y %H:%M","YYYY/MM/DD HH:MM":"%Y/%m/%d %H:%M",
                "MM-DD-YYYY HH:MM":"%m-%d-%Y %H:%M","DD-MM-YYYY HH:MM":"%d-%m-%Y %H:%M","YYYY-MM-DD HH:MM":"%Y-%m-%d %H:%M",
                "MMM-DD-YYYY HH:MM":"%b-%d-%Y %H:%M","DD-MMM-YYYY HH:MM":"%d-%b-%Y %H:%M","YYYY-MMM-DD HH:MM":"%Y-%b-%d %H:%M",
                "MMM/DD/YYYY HH:MM":"%b/%d/%Y %H:%M","DD/MMM/YYYY HH:MM":"%d/%b/%Y %H:%M","YYYY/MMM/DD HH:MM":"%Y/%b/%d %H:%M",
                "MMM DD YYYY HH:MM":"%b %d %Y %H:%M","DD MMM YYYY HH:MM":"%d %b %Y %H:%M","YYYY MMM DD HH:MM":"%Y %b %d %H:%M",

                "MM/DD/YYYY HH:MM:SS AM/PM":"%m/%d/%Y %I:%M:%S %p","DD/MM/YYYY HH:MM:SS AM/PM":"%d/%m/%Y %I:%M:%S %p","YYYY/MM/DD HH:MM:SS AM/PM":"%Y/%m/%d %I:%M:%S %p",
                "MM-DD-YYYY HH:MM:SS AM/PM":"%m-%d-%Y %I:%M:%S %p","DD-MM-YYYY HH:MM:SS AM/PM":"%d-%m-%Y %I:%M:%S %p","YYYY-MM-DD HH:MM:SS AM/PM":"%Y-%m-%d %I:%M:%S %p",
                "MMM-DD-YYYY HH:MM:SS AM/PM":"%b-%d-%Y %I:%M:%S %p","DD-MMM-YYYY HH:MM:SS AM/PM":"%d-%b-%Y %I:%M:%S %p","YYYY-MMM-DD HH:MM:SS AM/PM":"%Y-%b-%d %I:%M:%S %p",
                "MMM/DD/YYYY HH:MM:SS AM/PM":"%b/%d/%Y %I:%M:%S %p","DD/MMM/YYYY HH:MM:SS AM/PM":"%d/%b/%Y %I:%M:%S %p","YYYY/MMM/DD HH:MM:SS AM/PM":"%Y/%b/%d %I:%M:%S %p",
                "MMM DD YYYY HH:MM:SS AM/PM":"%b %d %Y %I:%M:%S %p","DD MMM YYYY HH:MM:SS AM/PM":"%d %b %Y %I:%M:%S %p","YYYY MMM DD HH:MM:SS AM/PM":"%Y %b %d %I:%M:%S %p",

                "MM/DD/YYYY HH:MM AM/PM":"%m/%d/%Y %I:%M %p","DD/MM/YYYY HH:MM AM/PM":"%d/%m/%Y %I:%M %p","YYYY/MM/DD HH:MM AM/PM":"%Y/%m/%d %I:%M %p",
                "MM-DD-YYYY HH:MM AM/PM":"%m-%d-%Y %I:%M %p","DD-MM-YYYY HH:MM AM/PM":"%d-%m-%Y %I:%M %p","YYYY-MM-DD HH:MM AM/PM":"%Y-%m-%d %I:%M %p",
                "MMM-DD-YYYY HH:MM AM/PM":"%b-%d-%Y %I:%M %p","DD-MMM-YYYY HH:MM AM/PM":"%d-%b-%Y %I:%M %p","YYYY-MMM-DD HH:MM AM/PM":"%Y-%b-%d %I:%M %p",
                "MMM/DD/YYYY HH:MM AM/PM":"%b/%d/%Y %I:%M %p","DD/MMM/YYYY HH:MM AM/PM":"%d/%b/%Y %I:%M %p","YYYY/MMM/DD HH:MM AM/PM":"%Y/%b/%d %I:%M %p",
                "MMM DD YYYY HH:MM AM/PM":"%b %d %Y %I:%M %p","DD MMM YYYY HH:MM AM/PM":"%d %b %Y %I:%M %p","YYYY MMM DD HH:MM AM/PM":"%Y %b %d %I:%M %p",


                "MM/DD/YYYY HH:MM:SSAM/PM":"%m/%d/%Y %I:%M:%S%p","DD/MM/YYYY HH:MM:SSAM/PM":"%d/%m/%Y %I:%M:%S%p","YYYY/MM/DD HH:MM:SSAM/PM":"%Y/%m/%d %I:%M:%S%p",
                "MM-DD-YYYY HH:MM:SSAM/PM":"%m-%d-%Y %I:%M:%S%p","DD-MM-YYYY HH:MM:SSAM/PM":"%d-%m-%Y %I:%M:%S%p","YYYY-MM-DD HH:MM:SSAM/PM":"%Y-%m-%d %I:%M:%S%p",
                "MMM-DD-YYYY HH:MM:SSAM/PM":"%b-%d-%Y %I:%M:%S%p","DD-MMM-YYYY HH:MM:SSAM/PM":"%d-%b-%Y %I:%M:%S%p","YYYY-MMM-DD HH:MM:SSAM/PM":"%Y-%b-%d %I:%M:%S%p",
                "MMM/DD/YYYY HH:MM:SSAM/PM":"%b/%d/%Y %I:%M:%S%p","DD/MMM/YYYY HH:MM:SSAM/PM":"%d/%b/%Y %I:%M:%S%p","YYYY/MMM/DD HH:MM:SSAM/PM":"%Y/%b/%d %I:%M:%S%p",
                "MMM DD YYYY HH:MM:SSAM/PM":"%b %d %Y %I:%M:%S%p","DD MMM YYYY HH:MM:SSAM/PM":"%d %b %Y %I:%M:%S%p","YYYY MMM DD HH:MM:SSAM/PM":"%Y %b %d %I:%M:%S%p", 

                "MM/DD/YYYY HH:MMAM/PM":"%m/%d/%Y %I:%M%p","DD/MM/YYYY HH:MMAM/PM":"%d/%m/%Y %I:%M%p","YYYY/MM/DD HH:MMAM/PM":"%Y/%m/%d %I:%M%p",
                "MM-DD-YYYY HH:MMAM/PM":"%m-%d-%Y %I:%M%p","DD-MM-YYYY HH:MMAM/PM":"%d-%m-%Y %I:%M%p","YYYY-MM-DD HH:MMAM/PM":"%Y-%m-%d %I:%M%p",
                "MMM-DD-YYYY HH:MMAM/PM":"%b-%d-%Y %I:%M%p","DD-MMM-YYYY HH:MMAM/PM":"%d-%b-%Y %I:%M%p","YYYY-MMM-DD HH:MMAM/PM":"%Y-%b-%d %I:%M%p",
                "MMM/DD/YYYY HH:MMAM/PM":"%b/%d/%Y %I:%M%p","DD/MMM/YYYY HH:MMAM/PM":"%d/%b/%Y %I:%M%p","YYYY/MMM/DD HH:MMAM/PM":"%Y/%b/%d %I:%M%p",
                "MMM DD YYYY HH:MMAM/PM":"%b %d %Y %I:%M%p","DD MMM YYYY HH:MMAM/PM":"%d %b %Y %I:%M%p","YYYY MMM DD HH:MMAM/PM":"%Y %b %d %I:%M%p"
                
                }
            date_time_format = None
            for key, val in date_time_format_dict.items():
                if key == date_time_col_format:
                    date_time_format = val          
            try:
                x=  pd.to_datetime(x, format=date_time_format)
            except:
                return np.nan
            return x

        def initial_insertion(dataframe, details_count, tbl_insert, source_name_ref_id, file_type):
            length_error_list = []
            data_type_error_list = []
            data_remove_list = []

            cursor.execute(f""" SELECT COUNT(*) FROM {tbl_insert} """)
            details_count = tuple(cursor)[0][0]
            details_count += 1 

            dataframe['details_id'] = range(details_count, len(dataframe) +details_count)            
            dataframe['created_date'] = datetime.now()
            dataframe['updated_date'] = datetime.now()
            dataframe['b_payment_amount'] = 0

            cursor.execute(f"""SELECT column_name, data_type, character_maximum_length, numeric_precision
                                                    FROM information_schema.columns
                                                    WHERE table_schema = 'public'
                                                    AND table_name = '{tbl_insert}'""")
            column_details_query = list(cursor)

            column_details = {}
            for column_name, data_type, max_length, numeric_precision in column_details_query:
                column_details[column_name] = [data_type, max_length, numeric_precision]

            for colIndx in column_details:
                if colIndx in list(dataframe.columns):
                    pass
                else:
                    return 'Column Not Match!'
            
            df_list = list(set(dataframe) & set(column_details))
            dataframe = dataframe.filter(df_list)
            
            pk_query = """
            SELECT a.attname
            FROM   pg_index i
            JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                AND a.attnum = ANY(i.indkey)
            WHERE  i.indrelid = 'public.""" + tbl_insert + """'::regclass
            AND    i.indisprimary;
            """
            cursor.execute(pk_query)
            pk_val_details = list(cursor)[0][0]
            pk_df = dataframe[pk_val_details]

            unique_check = dataframe[pk_val_details].is_unique
            if unique_check is False:
                return 'Duplicate Primary Keys'

            def check_length(x):
                return x.astype(str).str.len().max()

            def null_value_checker(null_val_col):
                p_k_list = []
                index_val_list = []
                if dataframe[null_val_col].isnull().any():
                    index = dataframe[null_val_col].index[dataframe[null_val_col].isnull()]
                    index_val_list = list(index)
                    for index_val in index_val_list:
                        p_k_list.append(pk_df[index_val])
                    data_type_error_list.append({'Column_name': null_val_col,'column_rows': p_k_list})
                return index_val_list

            del column_details['details_id']
            del column_details['created_date']
            del column_details['updated_date']
            del column_details['b_payment_amount']

            data_rm_list = []
            for column_name in list(column_details):
                if column_details[column_name][0] =='bigint' or column_details[column_name][0] =='integer':
                    dataframe[column_name]=dataframe[column_name].apply(integer_column)
                    data_rm_list = null_value_checker(column_name)
                    dataframe[column_name] = dataframe[column_name].astype(str).apply(lambda x : x.split('.')[0])
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)   

                elif column_details[column_name][0] == 'real' or column_details[column_name][0] == 'float' or column_details[column_name][0] == 'decimal' or column_details[column_name][0] =='numeric':                    
                    float_val_p_k_list = []
                    try:
                        dataframe[column_name] = dataframe[column_name].astype('float32')
                    except:
                        pass
                    current_precision = column_details[column_name][2]
                    max_float_len = check_length(dataframe[column_name]) - 1
                    if max_float_len > current_precision:
                        float_index_val_list = dataframe[column_name].index[abs(dataframe[column_name].astype(str).str.len()) > current_precision]
                        float_index_val_list = list(float_index_val_list)
                        for float_index_val in float_index_val_list:
                            float_val_p_k_list.append(pk_df[float_index_val])
                        length_error_list.append({'Column_name': column_name,'column_rows': float_val_p_k_list})

                        data_rm_list = float_index_val_list
                        if data_rm_list:
                            data_remove_list.extend(data_rm_list)  
                                
                    dataframe[column_name]=dataframe[column_name].apply(float_column)
                    data_rm_list = null_value_checker(column_name)
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)
                    
                elif column_details[column_name][0] =='character varying' or column_details[column_name][0] == 'text':
                    string_val_p_k_list = []
                    max_string_len = check_length(dataframe[column_name])
                    if max_string_len > column_details[column_name][1]:
                        string_index_val_list = dataframe[column_name].index[abs(dataframe[column_name].astype(str).str.len()) > column_details[column_name][1]]
                        string_index_val_list = list(string_index_val_list)
                        for string_index_val in string_index_val_list:
                            string_val_p_k_list.append(pk_df[string_index_val])
                        length_error_list.append({'Column_name': column_name,'column_rows': string_val_p_k_list})
                        
                        data_rm_list = string_index_val_list
                        if data_rm_list:
                            data_remove_list.extend(data_rm_list)

                    dataframe[column_name]=dataframe[column_name].apply(string_column)
                    data_rm_list = null_value_checker(column_name)
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)
                    dataframe[column_name] = dataframe[column_name].apply(comma_checker)
                
                elif column_details[column_name][0] =='boolean':
                    dataframe[column_name]=dataframe[column_name].apply(boolean_column)
                    null_value_checker(column_name)

                elif column_details[column_name][0] =='date':
                    cursor.execute(f""" SELECT field_length FROM portal_tbl_source_details WHERE "header_ref_id_id" = {source_name_ref_id} AND "field_name" = '{column_name}' AND "is_active" = 'Y' AND "is_deleted" = 'N' """)
                    date_col_format = list(cursor)[0][0]
                    if file_type == '.xlsx':
                        dataframe[column_name] = dataframe[column_name].apply(lambda d: str(d).split(' ')[0])
                    dataframe[column_name] = dataframe.apply(lambda x: date_column(x[column_name],date_col_format), axis=1)
                    data_rm_list = null_value_checker(column_name)
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)

                elif column_details[column_name][0] =='time without time zone' or column_details[column_name][0] == 'time with time zone':
                    cursor.execute(f""" SELECT field_length FROM portal_tbl_source_details WHERE "header_ref_id_id" = {source_name_ref_id} AND "field_name" = '{column_name}' AND "is_active" = 'Y' AND "is_deleted" = 'N' """)
                    timestamp_col_format = list(cursor)[0][0]
                    dataframe[column_name] = dataframe.apply(lambda x: timestamp_column(x[column_name],timestamp_col_format), axis=1)              
                    data_rm_list = null_value_checker(column_name)
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)

                elif column_details[column_name][0] =='timestamp without time zone' or column_details[column_name][0] == 'timestamp with time zone':
                    cursor.execute(f""" SELECT field_length FROM portal_tbl_source_details WHERE "header_ref_id_id" = {source_name_ref_id} AND "field_name" = '{column_name}' AND "is_active" = 'Y' AND "is_deleted" = 'N' """)
                    date_time_col_format = list(cursor)[0][0]
                    dataframe[column_name] = dataframe.apply(lambda x: datetimestamp_column(x[column_name],date_time_col_format), axis=1)
                    data_rm_list = null_value_checker(column_name)
                    if data_rm_list:
                        data_remove_list.extend(data_rm_list)            

            sio = StringIO()
            sio.write(dataframe.to_csv(index=None, header=None))
            sio.seek(0) 
            try: 
                with connection.cursor() as c:   
                    c.copy_from(sio, tbl_insert, columns=dataframe.columns, sep= ',')
                    connection.commit()
                return 'Data Upload Success'
            except Exception as e:
                connection.rollback()
                return 'Data Upload Failed'

        def initial_updation(dataframe, details_count, tbl_source_details, tbl_source_tmp_details, source_id, file_type):
            try:
                pk_details = """
                SELECT a.attname
                FROM   pg_index i
                JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                    AND a.attnum = ANY(i.indkey)
                WHERE  i.indrelid = 'public.""" + tbl_source_details + """'::regclass
                AND    i.indisprimary;
                """
                cursor.execute(pk_details)
                pk_details = list(cursor)[0][0]
                
                cursor.execute(f""" SELECT {pk_details} FROM public.{tbl_source_details} WHERE {pk_details} in ( SELECT {pk_details} from public.{tbl_source_tmp_details});""")
                dontExistRecords = tuple(cursor)

                if dontExistRecords:
                    cursor.execute(f"""SELECT column_name
                                                            FROM information_schema.columns
                                                            WHERE table_schema = 'public'
                                                            AND table_name = '{tbl_source_details}'""")
                    information_schema = tuple(cursor)

                    information_schema_conditions_AA = ""
                    information_schema_conditions_BB = ""

                    for infoSce in information_schema:
                        if infoSce[0]=='b_payment_amount' or infoSce[0]=='details_id' or infoSce[0]=='created_date' or infoSce[0]== pk_details or infoSce[0]=='updated_date':
                            pass
                        else:                  
                            information_schema_conditions_AA += f'{infoSce[0]}, '
                            information_schema_conditions_BB += f'BB.{infoSce[0]}, '

                    information_schema_conditions_AA += f'updated_date'
                    information_schema_conditions_BB += f'Now()'

                    with transaction.atomic():
                        cursor.execute(f""" UPDATE public.{tbl_source_details} AA SET 
                            ({information_schema_conditions_AA}) = 
                            ({information_schema_conditions_BB})
                            FROM public.{tbl_source_tmp_details} BB 
                            WHERE BB.{pk_details} = AA.{pk_details} 
                            """)
                        cursor.execute(f""" DELETE FROM public.{tbl_source_tmp_details} WHERE {pk_details} in ( SELECT {pk_details} from public.{tbl_source_details});""")

                cursor.execute(f""" SELECT {pk_details} FROM public.{tbl_source_tmp_details}""")
                CheckTempTableCount = tuple(cursor)
                if CheckTempTableCount:
                    incoming_dataframe = f"SELECT * FROM public.{tbl_source_tmp_details}"
                    incoming_dataframe = pd.read_sql(incoming_dataframe, engine)
                    cursor.execute(f""" SELECT COUNT(*) FROM {tbl_source_details} """)
                    details_count = tuple(cursor)[0][0]
                    details_count += 1 
                    incoming_dataframe['details_id'] = range(details_count, len(incoming_dataframe) +details_count)
                    sio = StringIO()
                    sio.write(incoming_dataframe.to_csv(index=None, header=None))
                    sio.seek(0)
                    try:
                        with connection.cursor() as c:   
                            c.copy_from(sio, tbl_source_details, columns=incoming_dataframe.columns, sep= ',')
                            connection.commit()
                            cursor.execute(f"""TRUNCATE TABLE {tbl_source_tmp_details};""")
                    except:
                        connection.rollback()
                return 'Update Success'
            except Exception as e:
                return 'Data Upload Failed'


        if details_count == 0:
            returned_resp = initial_insertion(dataframe, details_count, tbl_source_details, source_id, file_type)
            cursor.close()
            connection.close()
            return Response(returned_resp, status=status.HTTP_201_CREATED)
        else:
            cursor.execute(f"""TRUNCATE TABLE {tbl_source_tmp_details};""")
            returned_resp_temp = initial_insertion(dataframe, details_count, tbl_source_tmp_details, source_id, file_type)
            if returned_resp_temp == 'Data Upload Success':
                returned_resp = initial_updation(dataframe, details_count, tbl_source_details, tbl_source_tmp_details, source_id, file_type)
                cursor.close()
                connection.close()
                return Response(returned_resp, status=status.HTTP_201_CREATED)
            else:
                cursor.close()
                connection.close()
                return Response(returned_resp_temp, status=status.HTTP_201_CREATED)

class DownloadBillStructureViewSet(APIView):
    def post(self, request, *args, **kwargs):
        try:        
            filetype = request.data.get('filetype')
            company_id = request.data.get('company_id')
            if filetype == None or company_id == None:
                return Response({'Status':'Failed'},status=status.HTTP_200_OK)

            get_header_id = tbl_source_mst.objects.all().filter(is_deleted='N').filter(company_ref_id=company_id)
            get_header_id_list = list(get_header_id)
            for row in get_header_id_list:
                header_id = row.id

            detail_fiels_serializer = tbl_source_details.objects.all().filter(is_deleted='N').filter(header_ref_id=header_id)
            detail_fiels_list = list(detail_fiels_serializer)
            field_list=[]
            for row in detail_fiels_list:
                field_list.append(row.field_name)
            if filetype == 'CSV':
                return Response(field_list,status=status.HTTP_200_OK)
            elif filetype == 'JSON':
                data = json.dumps(field_list)
                return Response(data,status=status.HTTP_200_OK)
            elif filetype == 'XLSX':
                return Response(field_list,status=status.HTTP_200_OK)        
            return Response(status=status.HTTP_200_OK)        
        except:
            return Response({'Status':'Failed'},status=status.HTTP_404_NOT_FOUND)
        

# Manage Security Module

class AssignUserRolesViewSet(viewsets.ModelViewSet):
    serializer_class = AssignUserRolesSerializer
    queryset = tbl_assign_role_user_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')

    def delete(self):
        self.queryset = tbl_assign_role_user_details.objects.filter(header_ref_id=self).delete()

class AssignUserRolesDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = UserRoleDetailsSerializer
    queryset = tbl_assign_role_user_details.objects.filter(is_deleted='N')   

class LeftPanelViewSet(viewsets.ModelViewSet):
    serializer_class = LeftPanelSerializer
    queryset = tbl_left_panel.objects.filter(is_deleted='N',is_active='Y').order_by('parent_code','child_code','sequence_id','sub_child_code')

    def delete(self):
        self.queryset = tbl_left_panel.objects.filter(self=self).update(is_deleted='Y')

class AssignPagesRolesViewSet(viewsets.ModelViewSet):
    serializer_class = AssignPagesRolesSerializer
    queryset = tbl_assign_pages_roles.objects.filter(is_deleted='N')

# API Integration

@csrf_exempt
def ResponseParametrs(request,*args,**kwargs):
    if request.method=='GET':
        dict_param = { 'Loan_id': {'Display name': 'Loan Id', 'Field1': ''},
                    'NPCI_number': {'Display name': 'NPCI Number', 'Field2': ''},
                    'Payment_mode': {'Display name': 'Payment Mode', 'Field3': ''},
                    'Payment_channel': {'Display name': 'Payment Channel', 'Field4': ''},
                    'Payment_amount': {'Display name': 'Payment Amount', 'Field5': ''}
    
            }
        return JsonResponse(dict_param)
    else:
        return JsonResponse({'Data':'None'})

@csrf_exempt
def BillStatus(request,loan_number_id=0,*args,**kwargs):
    if request.method=='GET':
        pay = tbl_bbps_transaction_mst.objects.all().filter(is_deleted='N').filter(loan_number_id=loan_number_id).first()
        if not pay:
            return JsonResponse({'Failed':'Does not exist'})
        return JsonResponse({'Status':pay.payment_status})
    
            
def fiscal_year_func(todays_date):
    current_date = datetime.strptime(str(todays_date), "%Y-%m-%d")
    if current_date.month >= 4:
        period = current_date.month - 4 + 1
        fiscalyear = current_date.year
    elif current_date.month < 4:
        if current_date.month == 1:
            period = 10
        elif current_date.month == 2:
            period = 11
        elif current_date.month == 3:
            period = 12
        fiscalyear = current_date.year -  1     
    return list([period,fiscalyear])

def get_starting_number(table_name,id,reference_column,application_id):
    try:
        cursor = connection.cursor()
        cursor.execute(f"""SELECT starting_no FROM public.portal_tbl_document_sequencing_mst WHERE company_ref_id_id = {id} AND is_deleted = 'N' AND application_id ='{application_id}'""")
        starting_no_query = list(cursor)
        if len(list(starting_no_query))> 0 :
            starting_no = list(starting_no_query)[0]['invoice_ref_no']
        else:
            starting_no = 1
        last_record=table_name.objects.order_by().filter(is_deleted='N').last()
        
        if last_record is None:  
            starting_seq_ref_no = starting_no
        else:
            res = [re.findall(r'(\w+?)(\d+)',getattr(last_record,reference_column))]
            ree = res[0][0]

            starting_seq_ref_no = ree[0] + str(int(ree[1])+1)
        return starting_seq_ref_no
    except:
        return False
    finally:
        cursor.close()


class GetBillerTransactionDataViewSet(APIView):
    def get(self, request,  *args, **kwargs):
        data=request.GET['fiscal_data']
        data = data.split(',')
        todays_date = datetime.now()
        todays_date = datetime.strftime(todays_date, '%Y-%m-%d')
        company_id = data[2]
        last_day_prev_month = date.today().replace(day=1) - timedelta(days=1)
        start_day_prev_month = date.today().replace(day=1) - timedelta(days=last_day_prev_month.day)

        igst_amount = 0
        cgst_amount = 0
        sgst_amount = 0

        cursor = connection.cursor()
        cursor.execute(f" SELECT id FROM portal_tbl_platform_charges_mst WHERE biller_ref_id_id = {company_id} AND is_deleted = 'N' AND is_active = 'Y' AND start_date <= '{todays_date}' AND end_date >= '{todays_date}' LIMIT 1 ")        
        PlatformChargesMst = tuple(cursor)
        PlatformChargesMst = PlatformChargesMst[0][0]
        
        cursor.execute(f"SELECT * FROM public.portal_tbl_platform_charges_details WHERE header_ref_id_id = {PlatformChargesMst} AND is_deleted = 'N' AND is_active = 'Y' ")
        details = dictfetchall(cursor)
        lst=[]
        newlist=[] 
        newlist2=[]    
        
        for x in range(len(details)): 
            transaction_mode_ref_id_id = details[x]['transaction_mode_ref_id_id']
            cursor.execute(f""" SELECT master_key FROM public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {transaction_mode_ref_id_id} LIMIT 1 """)
            transaction_mode = tuple(cursor)
            transaction_mode = transaction_mode[0][0]
            min_value = details[x]['min_value'] 
            max_value = details[x]['max_value'] 
            cursor.execute("select sum(bm.payment_amount)as payment_amount,count(bm.payment_amount)as total_no_of_transaction,sum(bd.platform_fee) as platform_fee,sum(bd.platform_igst_amount) as platform_igst_amount ,sum(bd.platform_cgst_amount)as platform_cgst_amount,sum(bd.platform_sgst_amount) as platform_sgst_amount from  public.portal_tbl_bbps_transaction_mst as bm inner join public.portal_tbl_bbps_transaction_details as bd on bd.header_ref_id_id=bm.id where bm.biller_ref_id_id = ' " + company_id + "' AND bm.transaction_mode_ref_id_id= ' " +str(transaction_mode_ref_id_id)+"' and bm.payment_amount BETWEEN ' " +str(min_value)+"' AND ' " +str(max_value)+"' and is_invoiced='N' and bm.transaction_date BETWEEN ' " +str(start_day_prev_month)+"' AND ' " +str(last_day_prev_month)+"' ")
            details2 = dictfetchall(cursor)

            payment_amount = details2[0]['payment_amount']
            if payment_amount==None:
                payment_amount=0

            rate = details[x]['rate']           
            percent = details[x]['percent']
            total_no_transaction = details2[0]['total_no_of_transaction']
            platform_igst_amount = details2[0]['platform_igst_amount'] 
            platform_cgst_amount = details2[0]['platform_cgst_amount']
            platform_sgst_amount = details2[0]['platform_sgst_amount']

            if(platform_igst_amount == None):
                igst_amount = 0
            else:
                igst_amount += igst_amount
            
            if(platform_cgst_amount == None):
                cgst_amount = 0
            else:
                cgst_amount += platform_cgst_amount
            
            if(platform_sgst_amount == None):
                sgst_amount = 0
            else:
                sgst_amount += platform_sgst_amount

            total_tax_amnt = igst_amount + cgst_amount + sgst_amount                 
            platform_fee = details2[0]['platform_fee']

            if(platform_fee==None):           
                platform_fee=0
            else:
                n = int(platform_fee)
                lst.append(platform_fee)
                total_basic_amount = sum(lst)            
                total_amnt = total_basic_amount+total_tax_amnt

            transaction_data1={  
                'payment_amount':payment_amount,                           
                'platform_fee':platform_fee,                       
                'transaction_rate':rate,
                'transaction_percent':percent,
                'min_value':min_value,
                'max_value':max_value,
                'transaction_mode_ref_id':transaction_mode_ref_id_id,
                'transaction_mode':transaction_mode,
                'total_no_transaction':total_no_transaction,
                } 

            newlist.append(transaction_data1)

        if(payment_amount!=0): 
            grid2data={
                'total_igst_tax_amount':igst_amount,
                'total_cgst_tax_amount':cgst_amount,
                'total_sgst_tax_amount':sgst_amount,
                'total_tax_amount':total_tax_amnt,
                'total_amount':total_amnt,
                'total_basic_amount':total_basic_amount,                
            }
            newlist2.append(grid2data)
        else:
            newlist2=None 

        alldata={'details1':newlist,
        'details2':newlist2
        }  

        cursor.close()
        return Response(alldata, status=status.HTTP_200_OK)

class get_charges_data_of_biller(APIView):
    def get(self, request):
        serializer_class = tbl_biller_bank_charges_mst.objects.all().filter(is_deleted='N').filter(biller_ref_id=request.GET['COMPANY_ID'])
        serializer_result = tbl_biller_bank_charges_mst_serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)


class GetUniqueInvoiceNumberViewSet(APIView):
    def get(self, request):
        app_id = request.GET['Appliction_ID']

        cursor = connection.cursor()
        cursor.execute(f"""SELECT id FROM public.portal_tbl_transaction_type_mst WHERE transaction_name = 'INVOICE' AND is_deleted = 'N' AND application_id ='{app_id}' """)
        invoice_ref_no = get_starting_number(
            tbl_biller_invoice_mst,
            list(cursor)[0][0],
            'invoice_ref_no',
            app_id)
        if (not invoice_ref_no):invoice_ref_no = "INV1"

        if not str(invoice_ref_no).upper().startswith("INV"):
            invoice_ref_no = f"INV{invoice_ref_no}"
        cursor.close()
        return Response({'invoice_number': invoice_ref_no}, status=status.HTTP_200_OK)

class GetCompanyDataForInvoice(APIView):
    def get(self, request):
        con = engine.connect()
        todays = datetime.now()
        todays = datetime.strftime(todays, '%Y-%m-%d')
        cursor = connection.cursor()
        cursor.execute(f" SELECT A.id, A.company_name FROM public.portal_tbl_company_mst AS A JOIN portal_tbl_biller_bank_charges_mst AS B ON (B.biller_ref_id_id = A.id AND B.end_date>= '{todays}' AND B.start_date<= '{todays}' ) ")
        company_data = cursor
        cursor.close()
        return Response(company_data, status=status.HTTP_200_OK)
        
class tbl_assign_screen_to_role_mst_view(generics.ListCreateAPIView):
    serializer_class = tbl_assign_screen_to_role_mst_Serializer
    queryset = tbl_assign_pages_roles.objects.filter(is_deleted='N',is_active='Y').order_by('-id')    

    def get(self, request, id=None):
        serializer_class = tbl_assign_pages_roles.objects.all().filter(is_deleted='N',is_active='Y',assigned_to_role=id).order_by('-id')
        serializer_result = tbl_assign_screen_to_role_mst_Serializer(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

    def post(self, request, id=None):
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    def put(self, request, id=None):        
        updated_data = []
        assigned_id =[]
        assigned_id_id = []
        assigned_to_role = request.data[0]['assigned_to_role']
        
        cursor = connection.cursor()              
        query1= """SELECT id FROM public.portal_tbl_assign_pages_roles a WHERE a.is_deleted='N' AND a.assigned_to_role='""" + str(assigned_to_role) + """'  ORDER BY -id """ 
        cursor.execute(query1)
        result1=dictfetchall(cursor)
        for i in result1:
            assigned_id_id.append(i['id'])      

        for init in request.data:            
            if "id" in init.keys():             
                if tbl_assign_pages_roles.objects.filter(id=init['id']).exists():
                    assigned_id.append(init['id'])
                    det = tbl_assign_pages_roles.objects.get(id=init['id'])
                    det.read_access = init.get('read_access', det.read_access)
                    det.write_access = init.get('write_access', det.write_access)
                    det.updated_date_time = init.get('updated_date_time', det.updated_date_time)
                    det.delete_access = init.get('delete_access', det.delete_access)
                    det.updated_by = init.get('updated_by', det.updated_by)
                    det.save()
                    updated_data.append(det.id)
                else:
                    continue
            else:
                assign_screen_to_role = tbl_assign_screen_to_role_mst_Serializer(data=init)
                if assign_screen_to_role.is_valid():
                    assign_screen_to_role.save()  
        
        main_list = list(set(assigned_id_id) - set(assigned_id))
        if len(main_list)>=1:
            for init in main_list:
                self.queryset = tbl_assign_pages_roles.objects.filter(id=init).update(is_deleted='Y') 

        cursor.close()
        return JsonResponse(request.data,safe=False,status=status.HTTP_201_CREATED)        
    
    def delete(self, request, id=None):
        self.queryset = tbl_assign_pages_roles.objects.filter(assigned_to_role=id).update(is_deleted='Y')
        return Response({"status":True}, status=status.HTTP_200_OK)

class get_screen_data(APIView):    
    def get(self,request):
        if request.method=='GET':
            cursor = connection.cursor()
            cursor.execute("SELECT id,assigned_to_role,company_ref_id_id as company_ref_id,parent_code,child_code,sub_child_code,read_access,delete_access,write_access,form_ref_id_id as form_ref_id from public.portal_tbl_assign_pages_roles where  is_deleted='N' and assigned_to_role="+request.GET['assigned_to_role'] )
            screenData = dictfetchall(cursor)
            cursor.close()
            return Response(screenData, status=status.HTTP_200_OK)

class get_data_acc_role(APIView):
    
    def get(self, request):
        serializer_class = tbl_assign_pages_roles.objects.filter(is_deleted='N').filter(company_ref_id=request.GET['company_ref_id'])
        serializer_result = tbl_assign_pages_roles(serializer_class, many=True)
        return Response(serializer_result.data, status=status.HTTP_200_OK)

class get_screen_to_role(APIView):
    def get(self,request):
        if request.method=='GET':
            cursor = connection.cursor()
            cursor.execute("SELECT  DISTINCT ON (a.id) a.role_name ,c.company_name,c.id as company_ref_id ,result.assigned_to_role,result.company_ref_id_id,result.id,result from public.portal_tbl_assign_pages_roles result LEFT JOIN  public.portal_tbl_role_mst a on result.assigned_to_role = a.id left join public.portal_tbl_company_mst c on result.company_ref_id_id=c.id")
            ScreenToRoleData = dictfetchall(cursor)
            cursor.close()
            return Response(ScreenToRoleData, status=status.HTTP_200_OK) 

class getrole_acc_to_use(APIView):
    def get(self,request):
        if request.method=='GET':
            cursor = connection.cursor()
            cursor.execute("SELECT distinct result.role_name ,result.company_ref_id_id, result.id from  public.portal_tbl_role_mst  result left JOIN  public.portal_tbl_assign_pages_roles  a on result.id = a.assigned_to_role where result.id is null or  a.assigned_to_role  is null AND result.is_deleted='N'")
            RoleData = dictfetchall(cursor)
            cursor.close()
            return Response(RoleData, status=status.HTTP_200_OK)   

class sidebar_leftpanel(APIView):
    def get(self,request):
        cursor = connection.cursor()
        cursor.execute("select f.*, e.id, e.form_ref_id_id, d.header_ref_id_id,a.application_id, b.application_id, c.application_id, d.application_id, e.application_id, f.application_id from public.portal_tbl_login_mst as a join public.portal_tbl_employee_mst as b on a.end_user_ref_id = b.id and a.company_id = b.company_ref_id_id join public.portal_tbl_assign_role_user_mst as c on b.id = c.assigned_to_employee_id and c.company_ref_id_id = b.company_ref_id_id join public.portal_tbl_assign_role_user_details as d on c.id = d.header_ref_id_id join public.portal_tbl_assign_pages_roles as e on e.assigned_to_role = d.assigned_to_role_ref_id_id and e.company_ref_id_id = c.company_ref_id_id join public.portal_tbl_left_panel as f on e.form_ref_id_id = f.id where a.user_id = %s and a.company_id = %s and a.is_deleted = 'N' and b.is_deleted = 'N' and c.is_deleted = 'N' and d.is_deleted = 'N' and e.is_deleted = 'N' and f.is_deleted = 'N'", (request.GET['AUTH_USER_ID'], request.GET['company_ref_id']))
        sidebar = dictfetchall(cursor)
        cursor.close()
        return Response(sidebar, status=status.HTTP_200_OK)

class sidebar_biller_leftpanel(APIView):
    def get(self,request):
        cursor = connection.cursor()
        cursor.execute("select a.form_ref_id_id,a.id,b.* from public.portal_tbl_assign_pages_roles as a left join public.portal_tbl_left_panel as b on a.form_ref_id_id = b.id where a.company_ref_id_id = {} and a.is_deleted = 'N' and b.is_deleted = 'N' order by a.form_ref_id_id asc".format(request.GET['company_ref_id']))
        sidebar = dictfetchall(cursor)
        cursor.close()
        return Response(sidebar, status=status.HTTP_200_OK)

# Dashboard Data
class GetNotReconciled(APIView):
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            biller_id = str(request.GET['tenent'])
            Month = str(request.GET['month'])
            cursor.execute(f"SELECT SUM(payment_amount) as not_reconciled from portal_tbl_bbps_transaction_mst WHERE recon_status = 'Not Reconciled' AND biller_ref_id_id = {biller_id} AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL') AND (EXTRACT(year FROM  transaction_date), EXTRACT(month FROM transaction_date))=({Month}) ")
            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory, status=status.HTTP_200_OK)
        
class GetTransactionChart(APIView):
    def get(self,request):
        if request.method == 'GET':
            try:
                cursor = connection.cursor()
                biller_id = str(request.GET['tenent'])
                type_tranctions = str(request.GET['type'])

                if type_tranctions == 'Collection':
                    cursor.execute(f"SELECT to_char(transaction_date,'MM') as mon, sum(payment_amount) as amount ,COUNT(transaction_date) AS TotalCount from portal_tbl_bbps_transaction_mst where biller_ref_id_id = {biller_id} GROUP BY to_char(transaction_date, 'MM')")
                    billerCategory = dictfetchall(cursor)
                    cursor.close()
                    return Response(billerCategory, status=status.HTTP_200_OK)

                elif type_tranctions == 'Outstanding':
                    billerCategory = tbl_source_mst.objects.filter(company_ref_id = biller_id, is_deleted='N').order_by('-id').first()
                    table_name = 'tbl_'+str(billerCategory.source_name)+'_details'
                    biller_number = tbl_source_details.objects.filter(header_ref_id=billerCategory.id, is_primary_field=True, is_deleted='N').first()            
                    biller_number = biller_number.field_name
                    master_key = tbl_master.objects.filter(master_key='Amount Due', is_deleted='N').first()
                    amount_due = tbl_source_details.objects.filter(header_ref_id=billerCategory.id, npci_parameter_ref_id=master_key.id, is_deleted='N').first()
                    amount_due = amount_due.field_name

                    cursor.execute(f"select to_char(a.created_date,'MM') as mon , count(*) as totalcount ,sum(a.{amount_due}) as amount from {table_name} as a where a.{biller_number} not in(select a.{biller_number} from  {table_name}  a inner join portal_tbl_bbps_transaction_mst b on a.{biller_number}::varchar=b.bill_number) GROUP BY to_char(a.created_date, 'MM')")
                    query1 = dictfetchall(cursor)

                    cursor.execute(f"select to_char(a.transaction_date,'MM') as mon, count(*) as totalcount, sum(b.{amount_due}) as amount from portal_tbl_bbps_transaction_mst as a INNER JOIN {table_name}  as b ON a.bill_number = b.{biller_number}::varchar   and a.payment_status='SUCCESSFUL' and NOT a.bill_amount=a.payment_amount GROUP BY to_char(a.transaction_date, 'MM')")
                    query2 = dictfetchall(cursor)
                    mon = 0
                    totalcount = 0
                    amount = 0
                    if query1:
                        if query1[0]['totalcount'] is not None:
                            totalcount = query1[0]['totalcount']
                            mon = query1[0]['mon']
                        else:
                            totalcount = 0

                        if query1[0]['amount'] is not None:
                            amount = query1[0]['amount']
                        else:
                            amount = 0

                    if query2:
                        if query2[0]['totalcount'] is not None:
                            totalcount = query2[0]['totalcount'] + totalcount
                            mon = query2[0]['mon']
                        else:
                            totalcount = 0 + totalcount

                        if query2[0]['amount'] is not None:
                            amount = query2[0]['amount'] + amount
                        else:
                            amount = 0 + amount

                    outstanding_data_dict = {}
                    outstanding_data_dict['mon'] = mon
                    outstanding_data_dict['totalcount'] = totalcount
                    outstanding_data_dict['amount'] = amount
                    cursor.close()
                    return Response([outstanding_data_dict], status=status.HTTP_200_OK)
            except:
                return Response(status=status.HTTP_200_OK)
                
class GetTotalCollection(APIView):
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            biller_id = str(request.GET['tenent'])
            month = str(request.GET['month'])
            cursor.execute(f"select SUM(payment_amount) as total_collection from portal_tbl_bbps_transaction_mst WHERE biller_ref_id_id = {biller_id} AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL') AND (EXTRACT(year FROM  transaction_date), EXTRACT(month FROM transaction_date))=({month}) ")
            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory, status=status.HTTP_200_OK)

class GetNoOfBills(APIView):
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            biller_id = str(request.GET['tenent'])
            month = str(request.GET['month'])
            cursor.execute(f" SELECT count(*) FROM portal_tbl_bbps_transaction_mst WHERE biller_ref_id_id = {biller_id}  AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL') AND (EXTRACT(year FROM  transaction_date), EXTRACT(month FROM transaction_date))=({month}) ")
            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory, status=status.HTTP_200_OK)

class GetBankBalance(APIView):
    def get(self,request):
        if request.method == 'GET':
            cursor = connection.cursor()
            biller_id = str(request.GET['tenent'])
            month = str(request.GET['month'])
            cursor.execute(f"select SUM(remitted_amount) as bank_bal from portal_tbl_bbps_transaction_mst where biller_ref_id_id = {biller_id} AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL') AND (EXTRACT(year FROM  transaction_date), EXTRACT(month FROM transaction_date))=({month})")
            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory, status=status.HTTP_200_OK)

class GetOutstanding(APIView):
    def get(self,request):
        if request.method == 'GET':
            try:
                cursor = connection.cursor()
                biller_id = str(request.GET['tenent'])
                billerCategory = tbl_source_mst.objects.filter(company_ref_id=biller_id,is_deleted='N').order_by('-id').first()
                table_name = 'tbl_'+str(billerCategory.source_name)+'_details'
                biller_number = tbl_source_details.objects.filter(header_ref_id=billerCategory.id, is_primary_field=True, is_deleted='N').first()            
                biller_number = biller_number.field_name
                master_key = tbl_master.objects.filter(master_key='Amount Due', is_deleted='N').first()
                amount_due = tbl_source_details.objects.filter(header_ref_id=billerCategory.id, npci_parameter_ref_id=master_key.id, is_deleted='N').first()
                amount_due = amount_due.field_name
            
                # All Count
                cursor.execute(f""" SELECT COUNT(*) AS total_transaction, (SUM({amount_due})-SUM(b_payment_amount)) AS total_value FROM {table_name} WHERE b_payment_amount < {amount_due} """)
                totalCount = dictfetchall(cursor)

                total_transaction = 0
                total_value = 0
                if totalCount:
                    if totalCount[0]['total_transaction'] is not None:
                        total_transaction = totalCount[0]['total_transaction']
                    else:
                        total_transaction = 0

                    if totalCount[0]['total_value'] is not None:
                        total_value = totalCount[0]['total_value']
                    else:
                        total_value = 0

                total_outstanding={}
                total_outstanding['total_trans'] = total_transaction
                total_outstanding['total_value'] = total_value

                cursor.execute(f""" SELECT COUNT(*) AS total_transaction, (SUM({amount_due})-SUM(b_payment_amount)) AS total_value FROM {table_name} WHERE b_payment_amount < {amount_due} AND updated_date  > (now() - INTERVAL' 30 day') """)
                last30Days = dictfetchall(cursor)

                total_transaction = 0
                total_value = 0
                if last30Days[0]['total_transaction'] is not None:
                    total_transaction = last30Days[0]['total_transaction']
                else:
                    total_transaction = 0

                if last30Days[0]['total_value'] is not None:
                    total_value = last30Days[0]['total_value']
                else:
                    total_value = 0

                data_between_0_to_30_day = {}
                data_between_0_to_30_day['total_trans'] = total_transaction
                data_between_0_to_30_day['total_value'] = total_value

                cursor.execute(f""" SELECT COUNT(*) AS total_transaction, (SUM({amount_due})-SUM(b_payment_amount)) AS total_value FROM {table_name} WHERE b_payment_amount < {amount_due} AND updated_date >= now() - INTERVAL' 60 DAY' AND updated_date  < now() - INTERVAL ' 30 DAY' """)
                last3160Days = dictfetchall(cursor)

                total_transaction = 0
                total_value = 0
                if last3160Days[0]['total_transaction'] is not None:
                    total_transaction = last3160Days[0]['total_transaction']
                else:
                    total_transaction = 0

                if last3160Days[0]['total_value'] is not None:
                    total_value = last3160Days[0]['total_value']
                else:
                    total_value = 0

                data_between_31_to_60_day = {}
                data_between_31_to_60_day['total_trans'] = total_transaction
                data_between_31_to_60_day['total_value'] = total_value

                cursor.execute(f""" SELECT COUNT(*) AS total_transaction, (SUM({amount_due})-SUM(b_payment_amount)) AS total_value FROM {table_name} WHERE b_payment_amount < {amount_due} AND updated_date >= now() AND updated_date  < now() - INTERVAL ' 60 DAY' """)
                above61Days = dictfetchall(cursor)

                total_transaction = 0
                total_value = 0
                if above61Days[0]['total_transaction'] is not None:
                    total_transaction = above61Days[0]['total_transaction']
                else:
                    total_transaction = 0

                if above61Days[0]['total_value'] is not None:
                    total_value = above61Days[0]['total_value']
                else:
                    total_value = 0

                data_between_61_to_above_day = {}
                data_between_61_to_above_day['total_trans'] = total_transaction
                data_between_61_to_above_day['total_value'] = total_value

                
                retunRes = {'total_outstanding': total_outstanding,
                            'data_between_0_to_30_day': data_between_0_to_30_day,
                            'data_between_31_to_60_day': data_between_31_to_60_day,
                            'data_between_61_to_above_day': data_between_61_to_above_day                        
                            }
                return Response(retunRes, status=status.HTTP_200_OK)
            except:
                retunRes = {'total_outstanding': 0,
                            'data_between_0_to_30_day': 0,
                            'data_between_31_to_60_day': 0,
                            'data_between_61_to_above_day': 0                        
                            }
                return Response(retunRes, status=status.HTTP_200_OK)
            
            finally:
                cursor.close()

class BbpsDataForPaymentfileViewSet(APIView):
    def get(self,request):
        if request.method == 'GET':
            biller_id=request.GET['id']
            from_date = str(request.GET['From_date'])
            To_date = str(request.GET['to_date'])
            cursor = connection.cursor()

            return_res = {}
            cursor.execute(f""" SELECT b.company_name, sum(a.payment_amount) as total_transaction_amount from portal_tbl_bbps_transaction_mst as a INNER JOIN portal_tbl_company_mst as b ON ( a.biller_ref_id_id = b.id and b.is_deleted='N' and a.payment_status='SUCCESSFUL' and a.transaction_date>= '{from_date}' and a.transaction_date<= '{To_date}' and  a.biller_ref_id_id = '{biller_id}' AND a.recon_status = 'Reconciled' AND a.remitted_amount = 0) group by b.company_name """)
            total_transaction = dictfetchall(cursor)            
            return_res ['total_transaction'] = total_transaction

            cursor.execute("SELECT id, starting_no from portal_tbl_document_sequencing_mst WHERE master_key = 'Payment File' AND is_deleted='N' AND is_deleted='N'")
            CustomerRefNumber = dictfetchall(cursor)
            return_res ['CustomerRefNumber'] = CustomerRefNumber

            cursor.execute(f""" SELECT entity_ref_id_id FROM portal_tbl_entity_relationship_mst WHERE company_ref_id_id = '{biller_id}' AND entity_ref_id_id != '{biller_id}' AND is_deleted='N' """)
            billerEntityRef = dictfetchall(cursor)

            if billerEntityRef:
                entity_ref_id = billerEntityRef[0]['entity_ref_id_id']
                cursor.execute(f"""SELECT b.account_number FROM portal_tbl_company_mst as a INNER JOIN portal_tbl_bank_mst as b ON  a.id = '{entity_ref_id}' AND b.company_ref_id_id = '{entity_ref_id}' AND b.nodal_acc_flag='True' AND a.is_deleted='N' AND b.is_deleted='N'""")                
                DebitAccNo = dictfetchall(cursor)
                return_res ['DebitAccNo'] = DebitAccNo
                cursor.close()
                return Response(return_res, status=status.HTTP_200_OK)
            else:
                cursor.close()
                return Response(status=status.HTTP_400_BAD_REQUEST)


class PercengedataforpaymentfileViewSet(APIView):
    def get(self,request):
        if request.method == 'GET':
            biller_id = request.GET['BILLER_ID']
            cursor = connection.cursor()
            todays = datetime.now()
            todays = datetime.strftime(todays, '%Y-%m-%d')

            cursor.execute(f""" SELECT a.id AS beneficiary_code, a.branch_name AS bene_branch_name, a.bank_name,a.pincode AS zipcode, 
                                a.account_number, a.ifsc_code, b.company_name, c.percentage_share, b.id AS company_id 
                                from portal_tbl_bank_mst as a JOIN portal_tbl_company_mst as b ON 
                                a.company_ref_id_id = b.id  and b.id = {biller_id} and a.is_deleted = 'N' 
                                and b.is_deleted = 'N' JOIN  portal_tbl_biller_bank_link_details  as c 
                                ON c.bank_ref_id_id = a.id and c.is_deleted = 'N' JOIN 
                                portal_tbl_biller_bank_link_mst AS D ON c.header_ref_id_id = D.id AND 
                                D.end_date >= '{todays}' AND D.start_date <= '{todays}' AND D.is_deleted = 'N'""")

            billerCategory = dictfetchall(cursor)
            cursor.close()
            return Response(billerCategory,status=status.HTTP_200_OK)

class PaymentfileViewSet(viewsets.ModelViewSet):
      serializer_class = paymentfileSerializer
      queryset = tbl_bulk_payment_file_mst.objects.all().filter(is_deleted='N', is_active='Y').order_by('-id')

class GetPaymentDetailsTabledata(APIView):
     def get(self,request):
        payment_id=request.GET['payment_id']
        cursor = connection.cursor()
        cursor.execute("select b.beneficiary_code,b.beneficiary_accno,b.transaction_amount,b.customer_ref_no,b.debit_acc_number,b.zipcode from portal_tbl_bulk_payment_file_mst as a inner join portal_tbl_bulk_payment_file_details as b on a.id = b.header_ref_id_id and a.id=' " + payment_id + "' and b.header_ref_id_id= '" + payment_id + "'   where a.is_deleted = 'N' and b.is_deleted = 'N'")
        billerCategory = dictfetchall(cursor)
        cursor.close()
        return Response(billerCategory,status=status.HTTP_200_OK)

class GetBillerReportData(APIView):
    def post(self, request, *args, **kwargs):
            biller_id = str(request.data.get('biller_id'))
            from_date = request.data.get('from_date')
            to_date = request.data.get('to_date')
            status1 = request.data.get('status')
            biller1_name = request.data.get('biller_name') 
            if(status1['master_key']=='Unpaid'):
                cursor = connection.cursor()
                cursor.execute(f""" SELECT billing_mode_ref_id_id FROM portal_tbl_company_mst WHERE id = {biller1_name} AND is_deleted='N' AND is_active='Y' LIMIT 1 """)
                billingModeIddict = dictfetchall(cursor)
                billingModeIddict = list(billingModeIddict)[0]['billing_mode_ref_id_id']
                cursor.execute(f""" SELECT master_key FROM portal_tbl_master WHERE id = {billingModeIddict} AND is_deleted='N' AND is_active='Y' LIMIT 1 """)
                billingModeIddict = dictfetchall(cursor)
                billingModeIddict = billingModeIddict[0]['master_key']
                
                if(billingModeIddict == 'Offline (A)' or billingModeIddict == 'Offline (B)'):
                    import math 
                    cursor.execute(f""" SELECT source_name FROM portal_tbl_source_mst WHERE company_ref_id_id = {biller1_name} AND is_deleted='N' AND is_active='Y' LIMIT 1 """)
                    sourceName = dictfetchall(cursor)
                    sourceName = sourceName[0]['source_name']
                    cursor.execute(f""" SELECT * FROM tbl_{sourceName}_details WHERE b_payment_amount = '{round(math.ceil(0),2)}' ORDER BY details_id ASC """)
                    unpaidDatadict = dictfetchall(cursor)
                    for item in unpaidDatadict:
                        del item['details_id']
                        del item['b_payment_amount']
                        del item['created_date']
                        del item['updated_date']

                    cursor.close()                        
                    return Response(unpaidDatadict, status=status.HTTP_200_OK)
                else:
                    cursor.close()
                    return Response(status=status.HTTP_200_OK)
           
            else:
                if(biller1_name=="All-Biller"):
                    if(status1['master_key']=='Fetch'):
                        cursor = connection.cursor()
                        cursor.execute("SELECT m.utr_number,m.fetch_unique_id,m.transaction_date,m.payment_amount,m.bill_amount,m.customer_name,m.payment_channel,m.payment_date,m.payment_status,m.transaction_mode,m.bill_number,m.due_date,m.bill_period,m.mobile_number,portal_tbl_company_mst.company_name as biller_name ,public.portal_tbl_company_mst.npci_assigned_id  As biller_id,portal_tbl_bbpou_bank_mst.bbpou_bank_name,portal_tbl_country_currency_mst.currency_code FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN  public.portal_tbl_company_mst ON m.biller_ref_id_id = public.portal_tbl_company_mst.id INNER JOIN  public.portal_tbl_bbpou_bank_mst ON m.bbpou_bank_ref_id_id = portal_tbl_bbpou_bank_mst.id INNER JOIN  public.portal_tbl_country_currency_mst ON  m.currency_ref_id_id = public.portal_tbl_country_currency_mst.id  WHERE  m.transaction_date>='" + from_date +  "' and  m.transaction_date<= '" + to_date +  "'")
                        billerCategory1 = dictfetchall(cursor)
                        cursor.close()
                        return Response(billerCategory1,status=status.HTTP_200_OK)

                    if(status1['master_key']=='Paid'):
                        cursor = connection.cursor()
                        cursor.execute("SELECT m.utr_number,m.fetch_unique_id,m.transaction_date,m.payment_amount,m.bill_amount,m.customer_name,m.payment_channel,m.payment_date,m.payment_status,m.transaction_mode,m.bill_number,m.due_date,m.bill_period,m.mobile_number,d.bill_amount,d.interchange_switch_fee,d.interchange_switch_sgst_rate,d.interchange_switch_igst_amount,d.interchange_switch_igst_rate,d.interchange_switch_sgst_amount,d.interchange_switch_cgst_rate,d.interchange_switch_cgst_amount,d.biller_bank_fee,d.biller_bank_sgst_rate,d.biller_bank_sgst_amount,d.biller_bank_igst_rate,d.biller_bank_igst_amount,d.biller_bank_cgst_rate,d.biller_bank_cgst_amount,d.platform_fee,d.platform_sgst_rate,d.platform_sgst_amount,d.platform_igst_rate,d.platform_igst_amount,d.platform_cgst_rate,d.platform_cgst_amount,d.payment_amount,d.partner_igst_amount,d.partner_igst_rate,d.partner_cgst_amount,d.partner_cgst_rate,d.partner_fee,d.partner_sgst_amount,d.partner_sgst_rate,portal_tbl_master.master_key,company_name as biller_name,public.portal_tbl_company_mst.npci_assigned_id  As biller_id,public.portal_tbl_company_mst.npci_assigned_id,portal_tbl_bbpou_bank_mst.bbpou_bank_name,portal_tbl_country_currency_mst.currency_code FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN  public.portal_tbl_master ON m.transaction_mode_ref_id_id = public.portal_tbl_master.id INNER JOIN  public.portal_tbl_company_mst ON m.biller_ref_id_id = public.portal_tbl_company_mst.id INNER JOIN  public.portal_tbl_bbpou_bank_mst ON m.bbpou_bank_ref_id_id = public.portal_tbl_bbpou_bank_mst.id INNER JOIN  public.portal_tbl_country_currency_mst ON m.currency_ref_id_id = public.portal_tbl_country_currency_mst.id INNER JOIN public.portal_tbl_bbps_transaction_details AS d ON d.header_ref_id_id =m.id WHERE  m.payment_status='" + status1['master_key'] + "'and  m.transaction_date>='" + from_date +  "' and  m.transaction_date<= '" + to_date +  "'")
                        billerCategory = dictfetchall(cursor)
                        cursor.close()
                        return Response(billerCategory,status=status.HTTP_200_OK)
                
                elif(biller_id):
                    if(status1['master_key']=='Fetch'):
                        cursor = connection.cursor()
                        cursor.execute(f"""SELECT m.utr_number,m.fetch_unique_id,m.transaction_date,m.payment_amount,m.bill_amount,m.customer_name,m.payment_channel,m.payment_date,m.payment_status,m.transaction_mode,m.bill_number,m.due_date,m.bill_period,m.mobile_number,portal_tbl_company_mst.company_name as biller_name,public.portal_tbl_company_mst.npci_assigned_id  As biller_id,portal_tbl_bbpou_bank_mst.bbpou_bank_name,portal_tbl_country_currency_mst.currency_code FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN  public.portal_tbl_company_mst ON m.biller_ref_id_id = public.portal_tbl_company_mst.id INNER JOIN  public.portal_tbl_bbpou_bank_mst ON m.bbpou_bank_ref_id_id = portal_tbl_bbpou_bank_mst.id INNER JOIN  public.portal_tbl_country_currency_mst ON  m.currency_ref_id_id = public.portal_tbl_country_currency_mst.id  WHERE  m.biller_ref_id_id={biller1_name}  and m.transaction_date>='{from_date}' and  m.transaction_date<= '{to_date }'""")
                        billerCategory1 = dictfetchall(cursor)
                        cursor.close()
                        return Response(billerCategory1,status=status.HTTP_200_OK)

                    if(status1['master_key']=='Paid'):
                        cursor = connection.cursor()
                        cursor.execute(f"""SELECT m.utr_number,m.fetch_unique_id,m.transaction_date,m.payment_amount,m.bill_amount,m.customer_name,m.payment_date,m.transaction_mode,m.bill_number,m.due_date,m.bill_period,d.bill_amount,d.biller_bank_fee,d.biller_bank_sgst_rate,d.biller_bank_sgst_amount,d.biller_bank_igst_rate,d.biller_bank_igst_amount,d.biller_bank_cgst_rate,d.biller_bank_cgst_amount,d.platform_fee,d.platform_sgst_rate,d.platform_sgst_amount,d.platform_igst_rate,d.platform_igst_amount,d.platform_cgst_rate,d.platform_cgst_amount,d.payment_amount,d.partner_igst_amount,d.partner_igst_rate,d.partner_cgst_amount,d.partner_cgst_rate,d.partner_fee,d.partner_sgst_amount,d.partner_sgst_rate,portal_tbl_master.master_key,company_name as biller_name,public.portal_tbl_company_mst.npci_assigned_id  As biller_id,portal_tbl_bbpou_bank_mst.bbpou_bank_name,portal_tbl_country_currency_mst.currency_code FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN  public.portal_tbl_master ON m.transaction_mode_ref_id_id = public.portal_tbl_master.id INNER JOIN  public.portal_tbl_company_mst ON m.biller_ref_id_id = public.portal_tbl_company_mst.id INNER JOIN  public.portal_tbl_bbpou_bank_mst ON m.bbpou_bank_ref_id_id = public.portal_tbl_bbpou_bank_mst.id INNER JOIN  public.portal_tbl_country_currency_mst ON m.currency_ref_id_id = public.portal_tbl_country_currency_mst.id INNER JOIN public.portal_tbl_bbps_transaction_details AS d ON d.header_ref_id_id =m.id WHERE  m.biller_ref_id_id={biller1_name } and m.payment_status='{status1['master_key'] }' and  m.transaction_date>='{from_date }' and  m.transaction_date<= '{ to_date }'""")
                        billerCategory = dictfetchall(cursor)
                        cursor.close()
                        return Response(billerCategory,status=status.HTTP_200_OK)

class get_table_name_data(APIView):
    def get(self,request):
        if request.method=='GET':
            cursor = connection.cursor()
            cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_name NOT like 'auth%' and table_name NOT like 'django%' and table_schema not in ('information_schema','pg_catalog') and table_type='BASE TABLE';")
            ScreenToRoleData = dictfetchall(cursor) 
            cursor.close()         
            return Response(ScreenToRoleData, status=status.HTTP_200_OK)

class get_left_panel_data(APIView):
    def get(self, request):        
        if request.method=='GET':
            username = self.request.query_params.get('username',None)
            cursor = connection.cursor()
            cursor.execute(""" SELECT DISTINCT b.form_name,b.form_link,b.module_path,b.is_parent,b.is_child,b.is_sub_child,b.icon_class,b.sequence_id,b.parent_code,b.child_code from public.portal_tbl_left_panel b join public.portal_tbl_assign_pages_roles a on a.form_ref_id_id=b.id and b.is_deleted='N' and b.is_active='Y' join public.portal_tbl_assign_role_user_details c  on c.assigned_to_role_ref_id_id=a.assigned_to_role and c.is_deleted='N' and c.is_active='Y' join public.portal_tbl_assign_role_user_mst d on d.assigned_to_user_id='""" + username + """' and d.id=c.header_ref_id_id WHERE d.assigned_to_user_id='""" + username + """' and a.is_deleted='N' and a.is_active='Y' and b.is_parent='Y' and b.is_child='N' order by b.sequence_id""")
            result = dictfetchall(cursor)
            for row in result:
                for key, value in row.items():
                    if key == 'parent_code':
                        cursor.execute(""" SELECT DISTINCT b.form_name,b.form_link,b.module_path,b.is_parent,b.is_child,b.is_sub_child,b.icon_class,b.sequence_id,b.parent_code,b.child_code from public.portal_tbl_left_panel b join public.portal_tbl_assign_pages_roles a on a.form_ref_id_id=b.id and b.is_deleted='N' and b.is_active='Y' join public.portal_tbl_assign_role_user_details c on c.assigned_to_role_ref_id_id=a.assigned_to_role and c.is_deleted='N' and c.is_active='Y' join public.portal_tbl_assign_role_user_mst d on d.assigned_to_user_id='""" + username + """' and d.id=c.header_ref_id_id WHERE d.assigned_to_user_id='""" + username + """' and b.parent_code ='""" + value + """' and  a.is_deleted='N' and a.is_active='Y' and b.is_parent='N' and b.is_child='Y' and b.is_sub_child='N' order by b.sequence_id """)
                        result1 = dictfetchall(cursor)
                        for row1 in result1:
                            for key1, value1 in row1.items():
                                if key1 == 'child_code':
                                    cursor.execute(""" SELECT DISTINCT b.form_name,b.form_link,b.module_path,b.is_parent,b.is_child,b.is_sub_child,b.icon_class,b.sequence_id,b.parent_code,b.child_code from public.portal_tbl_left_panel b join public.portal_tbl_assign_pages_roles a on a.form_ref_id_id=b.id and b.is_deleted='N' and b.is_active='Y' join public.portal_tbl_assign_role_user_details c on c.assigned_to_role_ref_id_id=a.assigned_to_role and c.is_deleted='N' and c.is_active='Y' join public.portal_tbl_assign_role_user_mst d on d.assigned_to_user_id='""" + username + """' and d.id=c.header_ref_id_id WHERE d.assigned_to_user_id='""" + username + """' and b.parent_code ='""" + value + """' and b.child_code ='""" + value1 + """' and a.is_deleted='N' and a.is_active='Y' and b.is_parent='N' and b.is_child='N' and b.is_sub_child='Y' order by b.sequence_id """)
                                    result2 = dictfetchall(cursor)
                            row1['submenu_level2'] = result2
                row['submenu_level1'] = result1
            cursor.close()   
            return Response(result, status=status.HTTP_200_OK)
class getLeftPanelAccessData(APIView):
    def get(self, request):
        if request.method=='GET':
            username = self.request.query_params.get('username',None)
            form_name = self.request.query_params.get('form_name',None)
            cursor = connection.cursor()
            cursor.execute(""" SELECT DISTINCT a.read_access, a.write_access,a.delete_access FROM public.portal_tbl_left_panel b join public.portal_tbl_assign_pages_roles a on a.form_ref_id_id=b.id and b.is_deleted='N' and b.is_active='Y' join public.portal_tbl_assign_role_user_details c on c.assigned_to_role_ref_id_id=a.assigned_to_role and c.is_deleted='N' and c.is_active='Y' join public.portal_tbl_assign_role_user_mst d on d.id= c.header_ref_id_id and d.is_deleted='N' and d.is_active='Y' WHERE d.assigned_to_user_id ='""" + username + """' and b.form_name='""" + form_name + """' and a.is_deleted='N' and a.is_active='Y' """)
            result = dictfetchall(cursor)
            cursor.close()        
            return Response(result, status=status.HTTP_200_OK)

class get_employee_name_data(APIView):
    def get(self,request):
        if request.method=='GET':
            cursor = connection.cursor()
            cursor.execute(f"""SELECT first_name,last_name,ptm.user_id FROM public.portal_tbl_employee_mst tem, public.portal_tbl_login_mst ptm WHERE tem.id=ptm.employee_ref_id_id AND ptm.user_id in (SELECT assigned_to_user_id from public.portal_tbl_assign_role_user_mst WHERE is_deleted = 'N')""")
            result1=dictfetchall(cursor)  
            cursor.close()        
            return Response(result1, status=status.HTTP_200_OK)
class user_as_company(APIView):
    def get(self, request):        
        if request.method=='GET':
            comapny_id = self.request.query_params.get('id',None)
            notCondition = self.request.query_params.get('notCondition',None)
            cursor = connection.cursor()
            cursor.execute(f""" SELECT a.id, a.username, CONCAT(c.first_name,' ',c.last_name) as "employee_name" FROM public.auth_user a JOIN public.portal_tbl_login_mst b ON (b.user_id=a.id) JOIN public.portal_tbl_employee_mst c ON (b.employee_ref_id_id=c.id) WHERE b.company_id= {comapny_id} AND a.id {notCondition} IN (SELECT assigned_to_user_id FROM public.portal_tbl_assign_role_user_mst WHERE is_deleted='N' AND is_active='Y' AND company_ref_id_id= {comapny_id}) """)
            result = dictfetchall(cursor)
            cursor.close()
            return Response(result, status=status.HTTP_200_OK)

class secure_server_config_mst_view(viewsets.ModelViewSet):
    queryset = tbl_secure_server_config_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')
    serializer_class = secure_server_config_mst_serializer
class tax_view(APIView):
    def get(self, request):        
        if request.method=='GET':
            tax_rate_name = self.request.query_params.get('tax_rate_name',None)
            cursor = connection.cursor()
            cursor.execute(f""" select b.id as tax_rate_id,b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id and b.is_deleted='N' and b.hsn_sac_no='997119' and '2022-03-31'<=b.to_date and '2022-03-31'>=b.from_date where a.is_deleted='N' and a.tax_rate_name='{tax_rate_name}'""")
            result = dictfetchall(cursor)
            cursor.close()
            return Response(result, status=status.HTTP_200_OK)

class bbpoubanksAsPerAggregator(APIView):
    def get(self, request):        
        if request.method=='GET':
            company_id = self.request.query_params.get('company_id',None)
            biller_bank_id = self.request.query_params.get('biller_bank_id',None)
            if company_id:
                find_biller = tbl_entity_relationship_mst.objects.filter(entity_ref_id=company_id,is_deleted='N',is_active='Y').order_by('-id').values_list('company_ref_id')
                find_biller = [item for tup in find_biller for item in tup]
                find_biller = str(find_biller)[1:-1]
                cursor = connection.cursor()
                cursor.execute(f" SELECT bbl.*, cmt.company_name AS biller_name FROM portal_tbl_biller_bank_link_mst AS bbl JOIN portal_tbl_company_mst cmt ON (bbl.biller_ref_id_id=cmt.id) WHERE bbl.biller_ref_id_id IN ({find_biller}) AND bbl.is_deleted='N' AND bbl.is_active='Y'")
                result = dictfetchall(cursor)
            if biller_bank_id:
                cursor = connection.cursor()
                cursor.execute(f" SELECT A.*, A.bank_ref_id_id As bank_ref_id FROM portal_tbl_biller_bank_link_details AS A WHERE header_ref_id_id = {biller_bank_id} AND is_deleted='N' AND is_active='Y'")
                result = dictfetchall(cursor)
            cursor.close()
            return Response(result, status=status.HTTP_200_OK)
        
class LogoutView(APIView):
    def post(self, request, *args, **kwargs):
        if self.request.data.get('all'):
            token: OutstandingToken
            for token in OutstandingToken.objects.filter(user=request.user):
                _, _ = BlacklistedToken.objects.get_or_create(token=token)                
            return Response(status=status.HTTP_200_OK)        
        refresh_token = self.request.data.get('refresh_token')
        token = RefreshToken(token=refresh_token)
        token.blacklist()
        return Response(status=status.HTTP_200_OK)

def Decrypted_Keyword(interNal, exterNal):
    from cryptography.fernet import Fernet
    Fernet_Obj = Fernet(interNal)
    Decrypted_Key = Fernet_Obj.decrypt(exterNal)
    return Decrypted_Key

        
def get_tokens_for_user_func(user, clientID):
    from cryptography.fernet import Fernet
    access = AccessToken.for_user(user)    
    access['preferred_username'] = 'client-account-771DF886E74566DD'
    access['user'] = 'client-account-771DF886E74566DD'
    access['clientId'] = clientID
    access['typ'] = "Bearer"
    access['azp'] = clientID
    access['acr'] = "1"
    access['scope'] = "profile openid email"
    access['email_verified'] = False
    access['sub'] = Fernet.generate_key().decode()
    access['session_state'] = Fernet.generate_key().decode()    
    realm_access = {"roles": ["client-role","offline_access", "uma_authorization"]}
    access['realm_access'] = realm_access
    resource_access = {"my-test-client": {"roles": ["client-role" ]}}
    access['resource_access'] = resource_access
    return {
        'access': str(access)
        }

def EncryptionData(content, password):
    content = pad(content.encode(),16)
    cipher = AES.new(password.encode('utf-8'), AES.MODE_ECB)
    return base64.b64encode(cipher.encrypt(content)) 

def DecryptionData(content, password):
    content = base64.b64decode(content)
    cipher = AES.new(password.encode('utf-8'), AES.MODE_ECB)
    return unpad(cipher.decrypt(content),16)

@csrf_exempt
def TokenResetPassword(request):
    if request.method=='POST':
        requestData = JSONParser().parse(request)
        try:
            user = User.objects.get(username = requestData.get('username'))
            if user:
                RandomSecretString = ''.join(random.choices(string.ascii_letters + string.digits, k=6))
                user_token = AccessToken.for_user(user)
                user_token['profile_name'] = user.username
                user_token['security_code'] = RandomSecretString
                user_token = str(user_token)     
                try:
                    user_details = UserDetails.objects.get(user_id_id=user.id)
                    retry_count = user_details.retry_count + 1
                    UserDetails.objects.filter(user_id_id=user.id).update(retry_count = retry_count, cool_down_timestamp = datetime.now(pytz.timezone(TIME_ZONE)), security_code = RandomSecretString)
                except:
                    user_details = {
                        'user_id': user,
                        'profile_name': user.username,
                        'retry_count':1,
                        'security_code':RandomSecretString,
                        'mobile_number':None,
                        'cool_down_timestamp': datetime.now(pytz.timezone(TIME_ZONE))
                    }
                    UserDetails.objects.create(**user_details)

                subject = f'{RandomSecretString}: BBPS Account Password Reset'
                validations = 'The code is valid for only 5 Minutes'
                no_reply = 'This is an auto generated email please do not reply to this e-mail'
                message = f'Please use this code to reset the Password. \nHere is your code {RandomSecretString} \n\n{validations}\n{no_reply}'
                recepient = user.email
                send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently = False)
                return JsonResponse({'status':'Success', 'token':user_token})
        except:
            return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)
    else:
        return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)


@csrf_exempt
def KeyVerificationUpadatePassword(request):
    if request.method=='POST':
        requestData = JSONParser().parse(request)
        import jwt
        try:
            user_token = requestData.get('token')               
            decode_token = jwt.decode(user_token, options={"verify_signature": False})
            onetimepass = requestData.get('otp')
            password = requestData.get('password')
            security_code = decode_token.get('security_code')
            profile_name = decode_token.get('profile_name')

            if onetimepass == security_code:
                pass
            else:
                return JsonResponse({'status':'Failed'}) 
            
            user = User.objects.get(username = profile_name)
            user_details = UserDetails.objects.get(user_id_id=user.id)

            if user_details.security_code == security_code:
                pass
            else:
                return JsonResponse({'status':'Failed'}) 

            password = make_password(password, hasher='default')
            User.objects.filter(id=user.id).update(password = password)   
            return JsonResponse({'status':'Success'})     
        except jwt.ExpiredSignatureError:
            return JsonResponse({'status':'Expired'})

@csrf_exempt
def AccessTokenOUBank(request):
    if request.method=='POST':     
        requestData = JSONParser().parse(request)
        try:
            username = Decrypted_Keyword(OU_AGENT_ID[0], requestData.get('clientID').encode()).decode('ASCII')
            password = Decrypted_Keyword(OU_AGENT_KEYWORD[0], requestData.get('secret').encode()).decode('ASCII')
            user = User.objects.get(username = username)
            if user.check_password(password):
                user_token = get_tokens_for_user_func(user, requestData.get('clientID'))
                Response = {
                    "status": 200,
                    "success": True,    
                    "data": {
                        "expiresIn": 300,
                        "token": user_token['access']                    
                    }
                }
                return JsonResponse(Response)
        except:
            return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)
    else:
        return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)

@csrf_exempt
def paymentStatus(request):
    if request.method=='POST':
        try:
            paymentStatus = CheckStatus()
            paymentStatus.paymentstatus()
            return JsonResponse({'status':True}, status=status.HTTP_200_OK)
        except:
            return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)
    else:
        return JsonResponse({'status':'Failed', "errorCode": "401"}, status=status.HTTP_401_UNAUTHORIZED)


def commissionCalculation(ID, cursor):
    # cursor = connection.cursor()
    todays_date = datetime.now()
    todays_date = datetime.strftime(todays_date, '%Y-%m-%d')

    def round_to(value):
        ROUND_TO = 2
        return round(value, ROUND_TO)   

    def calculate_tax_using_gst(gstin_1, gstin_2, amount, tax_percent):
        if gstin_1[:2].lower() == gstin_2[:2].lower():
            cgst_amount = round_to(float(amount) * (float(tax_percent)/2)/100)
            sgst_amount = round_to(float(amount) * (float(tax_percent)/2)/100)
            return [float(cgst_amount), float(sgst_amount)]
        elif gstin_1[:2].lower() != gstin_2[:2].lower():
            igst_amount = round_to(float(amount) * float(tax_percent)/100)
            return [float(igst_amount)]        
    try:
        cursor.execute(f" SELECT * FROM portal_tbl_bbps_transaction_mst WHERE id = {ID} LIMIT 1 ")
        bbps_transaction = dictfetchall(cursor)[0]
        payment_amount = round_to(bbps_transaction['payment_amount'])
        bill_amount = round_to(bbps_transaction['bill_amount'])
        bbpou_bank_ref_id = bbps_transaction['bbpou_bank_ref_id_id']
        admin_company_ref_id = bbps_transaction['admin_company_ref_id_id']
        transaction_mode_ref_id = bbps_transaction['transaction_mode_ref_id_id']       
        biller_ref_id = bbps_transaction['biller_ref_id_id']
        acknowledgementId = bbps_transaction['fetch_unique_id']

        cursor.execute(f" SELECT id FROM portal_tbl_role_mst WHERE role_name = 'Aggregator' AND is_deleted = 'N' AND is_active = 'Y'  LIMIT 1 ")
        find_role_type = tuple(cursor)[0][0]

        cursor.execute(f" SELECT gst FROM portal_tbl_company_mst WHERE company_type_ref_id_id = {find_role_type} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
        gstin_npci = tuple(cursor)[0][0]

        cursor.execute(f" SELECT gst FROM portal_tbl_bbpou_bank_mst WHERE id = {bbpou_bank_ref_id} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
        gstin_bbpou_bank = tuple(cursor)[0][0]

        cursor.execute(f" SELECT gst FROM portal_tbl_company_mst WHERE id = {admin_company_ref_id} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
        gstin_platform = tuple(cursor)[0][0]

        cursor.execute(f" SELECT gst FROM portal_tbl_company_mst WHERE id = {biller_ref_id} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
        gstin_biller = tuple(cursor)[0][0]


        # Calculation NPCI Interchange Fees

        cursor.execute(f" SELECT id FROM portal_tbl_interchange_fee_mst WHERE biller_ref_id_id = {biller_ref_id} AND bbpou_bank_ref_id_id = {bbpou_bank_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND start_date <= '{todays_date}' AND end_date >= '{todays_date}' LIMIT 1 ")
        interchange_mst_charges = tuple(cursor)

        interchange_fee = 0 
        switch_fee = 0
        interchange_fee_gst = 0
        switch_fee_gst = 0
        import math

        # Check NPCI Exist Or Not
        if interchange_mst_charges:
            interchange_mst_charges = interchange_mst_charges[0]
            interchange_mst_id = interchange_mst_charges[0] 

            cursor.execute(f" SELECT id FROM portal_tbl_master WHERE master_key = 'Interchange Fee' AND is_deleted = 'N' AND is_active = 'Y'  LIMIT 1 ")
            interchange_fee_type_ref_id = tuple(cursor)[0][0]

            cursor.execute(f" SELECT * FROM portal_tbl_interchange_fee_details WHERE header_ref_id_id = {interchange_mst_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND fee_type_flag_ref_id_id = {interchange_fee_type_ref_id} AND round(min_value,2) <= round({math.ceil(payment_amount)},2) ORDER BY min_value ASC")
            InterchangeChargesDetails = dictfetchall(cursor)
        
            PreSlabRateInterchangeFeeCharges = 0
            for row in InterchangeChargesDetails:
                Interchange_rate_flag = row['rate_flag']

                cursor.execute(f""" SELECT b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id AND b.is_deleted='N' AND b.hsn_sac_no='997119' AND '2022-03-31'<=b.to_date AND '2022-03-31'>=b.from_date WHERE a.is_deleted='N' AND a.tax_rate_name='GST' AND b.id={row['tax_rate_id']} LIMIT 1""")
                interchange_fee_gst = tuple(cursor)[0][0]
                interchange_fee_gst = round_to(float(interchange_fee_gst))

                if Interchange_rate_flag == 'Y':
                    cursor.execute(f" SELECT rate FROM portal_tbl_interchange_fee_details WHERE header_ref_id_id = {interchange_mst_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND fee_type_flag_ref_id_id = {interchange_fee_type_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) AND round(max_value,2) >= round({math.ceil(payment_amount)},2) LIMIT 1")
                    NPCI_InterchangeFeeRate = tuple(cursor)[0][0]
                    interchange_fee = round_to(float(NPCI_InterchangeFeeRate)) 
                    min_commission_interchange = round_to(row['min_commission'])
                    max_commission_interchange = round_to(row['max_commission'])               
                    break

                elif Interchange_rate_flag == 'N':
                    if float(payment_amount)>float(row['max_value']):
                        CurrentSlabRate =  round_to(float(row['max_value']) - float(PreSlabRateInterchangeFeeCharges))
                        interchange_fee += round_to(float(CurrentSlabRate) * float(row['percent'])/100)                    
                        PreSlabRateInterchangeFeeCharges += round_to(float(row['max_value']))

                    elif float(payment_amount)<=float(row['max_value']):                    
                        CurrentSlabRate =  round_to(float(payment_amount) - float(PreSlabRateInterchangeFeeCharges))
                        interchange_fee += round_to(float(CurrentSlabRate) * float(row['percent'])/100)

                    min_commission_interchange = round_to(row['min_commission'])
                    max_commission_interchange = round_to(row['max_commission'])
            
            if float(min_commission_interchange) == float(0):
                pass
            else:
                if interchange_fee >= min_commission_interchange:
                    pass
                else:
                    interchange_fee = min_commission_interchange

            if float(max_commission_interchange) == float(0):
                pass
            else:
                if interchange_fee <= max_commission_interchange:
                    pass
                else:
                    interchange_fee = max_commission_interchange

            
            # Calculation NPCI Switch Fees

            cursor.execute(f" SELECT id FROM portal_tbl_master WHERE master_key = 'Switch Fee' AND is_deleted = 'N' AND is_active = 'Y'  LIMIT 1 ")
            switch_fee_type_ref_id = tuple(cursor)[0][0]

            cursor.execute(f" SELECT * FROM portal_tbl_interchange_fee_details WHERE header_ref_id_id = {interchange_mst_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND fee_type_flag_ref_id_id = {switch_fee_type_ref_id} AND round(min_value,2) <= round({math.ceil(payment_amount)},2) ORDER BY min_value ASC")
            SwitchChargesDetails = dictfetchall(cursor)

            PreSlabRateSwitchFeeCharges = 0
            for row in SwitchChargesDetails:
                Switch_rate_flag = row['rate_flag']

                cursor.execute(f""" SELECT b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id AND b.is_deleted='N' AND b.hsn_sac_no='997119' AND '2022-03-31'<=b.to_date AND '2022-03-31'>=b.from_date WHERE a.is_deleted='N' AND a.tax_rate_name='GST' AND b.id={row['tax_rate_id']} LIMIT 1""")
                switch_fee_gst = tuple(cursor)[0][0]
                switch_fee_gst = round_to(float(switch_fee_gst))

                if Switch_rate_flag == 'Y':
                    cursor.execute(f" SELECT rate FROM portal_tbl_interchange_fee_details WHERE header_ref_id_id = {interchange_mst_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND fee_type_flag_ref_id_id = {switch_fee_type_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) AND round(max_value,2) >= round({math.ceil(payment_amount)},2) LIMIT 1")
                    NPCI_SwitchFeeRate = tuple(cursor)[0][0]
                    switch_fee = round_to(float(NPCI_SwitchFeeRate))     
                    min_commission_switch = round_to(row['min_commission'])
                    max_commission_switch = round_to(row['max_commission'])                                         
                    break

                elif Switch_rate_flag == 'N':
                    if float(payment_amount)>float(row['max_value']):
                        CurrentSlabRate =  round_to(float(row['max_value']) - float(PreSlabRateSwitchFeeCharges))
                        switch_fee += round_to(float(CurrentSlabRate) * float(row['percent'])/100)                    
                        PreSlabRateSwitchFeeCharges += round_to(float(row['max_value']))

                    elif float(payment_amount)<=float(row['max_value']):                    
                        CurrentSlabRate =  round_to(float(payment_amount) - float(PreSlabRateSwitchFeeCharges))
                        switch_fee += round_to(float(CurrentSlabRate) * float(row['percent'])/100)

                    min_commission_switch = round_to(row['min_commission'])
                    max_commission_switch = round_to(row['max_commission'])

            if float(min_commission_switch) == float(0):
                pass
            else:        
                if switch_fee >= min_commission_switch:
                    pass
                else:
                    switch_fee = min_commission_switch

            if float(max_commission_switch) == float(0):
                pass
            else:
                if switch_fee <= max_commission_switch:
                    pass
                else:
                    switch_fee = max_commission_switch
     

        # Calculation Biller Bank Charges

        cursor.execute(f" SELECT id FROM portal_tbl_biller_bank_charges_mst WHERE biller_ref_id_id = {biller_ref_id} AND bbpou_bank_ref_id_id = {bbpou_bank_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND start_date <= '{todays_date}' AND end_date >= '{todays_date}' LIMIT 1 ")
        biller_bank_charges = tuple(cursor)[0]
        biller_bank_charges_id = biller_bank_charges[0]

        cursor.execute(f" SELECT * FROM portal_tbl_biller_bank_charges_details WHERE header_ref_id_id = {biller_bank_charges_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) ORDER BY min_value ASC")
        BillerBankChargesDetails = dictfetchall(cursor)

        biller_bank_charges = 0
        PreSlabRateBankCharges = 0
        for row in BillerBankChargesDetails:
            rate_flag = row['rate_flag']
            cursor.execute(f""" SELECT b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id AND b.is_deleted='N' AND b.hsn_sac_no='997119' AND '2022-03-31'<=b.to_date AND '2022-03-31'>=b.from_date WHERE a.is_deleted='N' AND a.tax_rate_name='GST' AND b.id={row['tax_rate_id']} LIMIT 1""")
            biller_bank_charges_gst = tuple(cursor)[0][0]
            biller_bank_charges_gst = round_to(float(biller_bank_charges_gst))

            if rate_flag == 'Y':
                cursor.execute(f" SELECT rate FROM portal_tbl_biller_bank_charges_details WHERE header_ref_id_id = {biller_bank_charges_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) AND round(max_value,2) >= round({math.ceil(payment_amount)},2) LIMIT 1")
                biller_bank_rate = tuple(cursor)[0][0]
                biller_bank_charges = round_to(float(biller_bank_rate))
                min_commission_biller_bank = round_to(row['min_commission'])
                max_commission_biller_bank = round_to(row['max_commission'])
                break

            else:
                if float(payment_amount)>float(row['max_value']):
                    CurrentSlabRate =  round_to(float(row['max_value']) - float(PreSlabRateBankCharges))
                    biller_bank_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)                    
                    PreSlabRateBankCharges += round_to(float(row['max_value']))

                elif float(payment_amount)<=float(row['max_value']):                    
                    CurrentSlabRate =  round_to(float(payment_amount) - float(PreSlabRateBankCharges))
                    biller_bank_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)
                min_commission_biller_bank = round_to(row['min_commission'])
                max_commission_biller_bank = round_to(row['max_commission'])

        if float(min_commission_biller_bank) == float(0):
            pass
        else:
            if biller_bank_charges >= min_commission_biller_bank:
                pass
            else:
                biller_bank_charges = min_commission_biller_bank

        if float(max_commission_biller_bank) == float(0):
            pass
        else:
            if biller_bank_charges <= max_commission_biller_bank:
                pass
            else:
                biller_bank_charges = max_commission_biller_bank


        # Calculation Platform Charges

        cursor.execute(f" SELECT id FROM portal_tbl_platform_charges_mst WHERE biller_ref_id_id = {biller_ref_id} AND bbpou_bank_ref_id_id = {bbpou_bank_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND start_date <= '{todays_date}' AND end_date >= '{todays_date}' LIMIT 1 ")        
        PlatformChargesMst = tuple(cursor)[0]        
        platform_charges_id = PlatformChargesMst[0]

        cursor.execute(f" SELECT * FROM portal_tbl_platform_charges_details WHERE header_ref_id_id = {platform_charges_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) ORDER BY min_value ASC")
        PlatformChargesDetails = dictfetchall(cursor)

        platform_charges = 0
        PreSlabRatePlatformCharges = 0

        for row in PlatformChargesDetails:
            rate_flag = row['rate_flag']

            cursor.execute(f""" SELECT b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id AND b.is_deleted='N' AND b.hsn_sac_no='997119' AND '2022-03-31'<=b.to_date AND '2022-03-31'>=b.from_date WHERE a.is_deleted='N' AND a.tax_rate_name='GST' AND b.id={row['tax_rate_id']} LIMIT 1""")
            platform_charges_gst = tuple(cursor)[0][0]
            platform_charges_gst = round_to(float(platform_charges_gst))

            if rate_flag == 'Y':
                cursor.execute(f" SELECT rate FROM portal_tbl_platform_charges_details WHERE header_ref_id_id = {platform_charges_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(payment_amount)},2) AND round(max_value,2) >= round({math.ceil(payment_amount)},2) LIMIT 1")
                platform_charges_rate = tuple(cursor)[0][0]
                platform_charges = round_to(float(platform_charges_rate))     
                min_commission_platform = round_to(row['min_commission'])
                max_commission_platform = round_to(row['max_commission'])           
                break

            else:
                if float(payment_amount)>float(row['max_value']):
                    CurrentSlabRate =  round_to(float(row['max_value']) - float(PreSlabRatePlatformCharges))
                    platform_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)                    
                    PreSlabRatePlatformCharges += round_to(float(row['max_value']))

                elif float(payment_amount)<=float(row['max_value']):                    
                    CurrentSlabRate =  round_to(float(payment_amount) - float(PreSlabRatePlatformCharges))
                    platform_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)
                
                min_commission_platform = round_to(row['min_commission'])
                max_commission_platform = round_to(row['max_commission'])

        if float(min_commission_platform) == float(0):
            pass
        else:
            if platform_charges >= min_commission_platform:
                pass
            else:
                platform_charges = min_commission_platform

        if float(max_commission_platform) == float(0):
            pass
        else:
            if platform_charges <= max_commission_platform:
                pass
            else:
                platform_charges = max_commission_platform


        # Calculate Biller Bank and Aggregator Charges
        # gross_platform_charges = platform_charges
        # platform_charges = round_to(gross_platform_charges - biller_bank_charges)
        # Calculation Partner Charges

        cursor.execute(f" SELECT partner_company_ref_id_id FROM portal_tbl_company_mst WHERE id = {biller_ref_id} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
        Partner_biller_check = tuple(cursor)[0][0]

        partner_charges = 0
        PreSlabRatePartnerCharges = 0
        partner_charges_gst = 0

        if Partner_biller_check:
            cursor.execute(f" SELECT biller_category_ref_id_id FROM portal_tbl_company_mst WHERE id = {biller_ref_id} AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 ")
            billerCategoryReference = tuple(cursor)[0][0]

            cursor.execute(f" SELECT id FROM portal_tbl_partner_charges_mst WHERE partner_company_ref_id_id = '{Partner_biller_check}' AND bbpou_bank_ref_id_id = '{bbpou_bank_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND start_date <= '{todays_date}' AND end_date >= '{todays_date}' LIMIT 1 ")        
            PartnerChargesMst = tuple(cursor)
            if PartnerChargesMst:                
                partner_charges_id = PartnerChargesMst[0][0]

                net_platform_charges = round_to(platform_charges - biller_bank_charges)         
                AggregatorCharges = net_platform_charges

                cursor.execute(f""" SELECT cat_id FROM portal_tbl_biller_category_mst WHERE cat_name = 'All' AND is_active = 'Y' AND is_deleted = 'N' LIMIT 1 """)        
                billerAllCategoryReference = tuple(cursor)[0][0]

                try:
                    cursor.execute(f" SELECT * FROM portal_tbl_partner_charges_details WHERE biller_category_ref_id_id = '{billerCategoryReference}' AND header_ref_id_id = '{partner_charges_id}' AND transaction_mode_ref_id_id = '{transaction_mode_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(AggregatorCharges)},2) ORDER BY min_value ASC")
                    PartnerChargesDetails = dictfetchall(cursor)
                    check_is_exist = PartnerChargesDetails[0]
                except:
                    cursor.execute(f" SELECT * FROM portal_tbl_partner_charges_details WHERE biller_category_ref_id_id = '{billerAllCategoryReference}' AND header_ref_id_id = '{partner_charges_id}' AND transaction_mode_ref_id_id = '{transaction_mode_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(AggregatorCharges)},2) ORDER BY min_value ASC")
                    PartnerChargesDetails = dictfetchall(cursor)

                for row in PartnerChargesDetails:
                    rate_flag = row['rate_flag']

                    cursor.execute(f""" SELECT b.tax_rate from portal_tbl_tax_rate_mst as a join portal_tbl_tax_rate_details as b on b.header_ref_id_id = a.id AND b.is_deleted='N' AND b.hsn_sac_no='997119' AND '2022-03-31'<=b.to_date AND '2022-03-31'>=b.from_date WHERE a.is_deleted='N' AND a.tax_rate_name='GST' AND b.id={row['tax_rate_id']} LIMIT 1""")
                    partner_charges_gst = tuple(cursor)[0][0]
                    partner_charges_gst = round_to(float(partner_charges_gst))

                    if rate_flag == 'Y':
                        cursor.execute(f" SELECT rate FROM portal_tbl_partner_charges_details WHERE header_ref_id_id = {partner_charges_id} AND transaction_mode_ref_id_id = {transaction_mode_ref_id} AND is_deleted = 'N' AND is_active = 'Y' AND round(min_value,2) <= round({math.ceil(AggregatorCharges)},2) AND round(max_value,2) >= round({math.ceil(AggregatorCharges)},2) LIMIT 1")
                        partner_charges_rate = tuple(cursor)[0][0]
                        partner_charges = round_to(float(partner_charges_rate))    
                        min_commission_partner = round_to(row['min_commission'])
                        max_commission_partner = round_to(row['max_commission'])            
                        break

                    else:
                        if float(AggregatorCharges)>float(row['max_value']):
                            CurrentSlabRate =  round_to(float(row['max_value']) - float(PreSlabRatePartnerCharges))
                            partner_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)                    
                            PreSlabRatePartnerCharges += round_to(float(row['max_value']))

                        elif float(AggregatorCharges)<=float(row['max_value']):                    
                            CurrentSlabRate =  round_to(float(AggregatorCharges) - float(PreSlabRatePartnerCharges))
                            partner_charges += round_to(float(CurrentSlabRate) * float(row['percent'])/100)

                        min_commission_partner = round_to(row['min_commission'])
                        max_commission_partner = round_to(row['max_commission']) 

                if float(min_commission_partner) == float(0):
                    pass
                else:
                    if partner_charges >= min_commission_partner:
                        pass
                    else:
                        partner_charges = min_commission_partner

                if float(max_commission_partner) == float(0):
                    pass
                else:
                    if partner_charges <= max_commission_partner:
                        pass
                    else:
                        partner_charges = max_commission_partner

                # Add Partner Charges
                # platform_charges = round_to(AggregatorCharges - partner_charges)
            
        interchange_fee_tax = calculate_tax_using_gst(gstin_npci, gstin_bbpou_bank, interchange_fee, interchange_fee_gst)
        switch_fee_tax = calculate_tax_using_gst(gstin_npci, gstin_bbpou_bank, switch_fee, switch_fee_gst)
        biller_bank_charges_tax = calculate_tax_using_gst(gstin_bbpou_bank, gstin_platform, biller_bank_charges, biller_bank_charges_gst)
        platform_charges_tax = calculate_tax_using_gst(gstin_platform, gstin_biller, platform_charges, platform_charges_gst)
        partner_charges_tax = calculate_tax_using_gst(gstin_platform, gstin_biller, partner_charges, partner_charges_gst)

        interchange_switch_fee = float(interchange_fee) + float(switch_fee)
        biller_bank_fee = float(biller_bank_charges)
        platform_fee = float(platform_charges)
        interchange_switch_fee_tax = []
        interchange_switch_gst = (float(interchange_fee_gst) + float(switch_fee_gst))/2

        if len(interchange_fee_tax) == len(switch_fee_tax):
            for i in range(len(interchange_fee_tax)):
               interchange_switch_fee_tax.append(interchange_fee_tax[i] + switch_fee_tax[i])

        if len(interchange_switch_fee_tax) == 1:
            interchange_switch_igst_rate = interchange_switch_gst
            interchange_switch_igst_amount = interchange_switch_fee_tax[0]
            interchange_switch_sgst_rate = 0.0
            interchange_switch_cgst_rate = 0.0
            interchange_switch_sgst_amount = 0
            interchange_switch_cgst_amount = 0
        elif len(interchange_switch_fee_tax) == 2:
            interchange_switch_sgst_rate = interchange_switch_gst/2
            interchange_switch_cgst_rate = interchange_switch_gst/2 
            interchange_switch_sgst_amount = interchange_switch_fee_tax[0]
            interchange_switch_cgst_amount = interchange_switch_fee_tax[1]
            interchange_switch_igst_rate = 0.0
            interchange_switch_igst_amount = 0

        if len(biller_bank_charges_tax) == 1:
            biller_bank_igst_rate = biller_bank_charges_gst
            biller_bank_igst_amount = biller_bank_charges_tax[0]
            biller_bank_sgst_rate = 0.0
            biller_bank_cgst_rate = 0.0
            biller_bank_sgst_amount = 0
            biller_bank_cgst_amount = 0 
        elif len(biller_bank_charges_tax) == 2:
            biller_bank_sgst_rate = biller_bank_charges_gst/2
            biller_bank_cgst_rate = biller_bank_charges_gst/2 
            biller_bank_sgst_amount = biller_bank_charges_tax[0]
            biller_bank_cgst_amount = biller_bank_charges_tax[1]
            biller_bank_igst_rate = 0.0
            biller_bank_igst_amount = 0

        if len(platform_charges_tax) == 1:
            platform_igst_rate = platform_charges_gst
            platform_igst_amount = platform_charges_tax[0]
            platform_cgst_rate = 0.0
            platform_sgst_rate = 0.0
            platform_sgst_amount = 0
            platform_cgst_amount = 0
        elif len(platform_charges_tax) == 2:
            platform_cgst_rate = platform_charges_gst/2
            platform_sgst_rate = platform_charges_gst/2 
            platform_sgst_amount = platform_charges_tax[0]
            platform_cgst_amount = platform_charges_tax[1]
            platform_igst_rate = 0.0
            platform_igst_amount = 0

        if len(partner_charges_tax) == 1:
            partner_igst_rate = partner_charges_gst
            partner_igst_amount = partner_charges_tax[0]
            partner_cgst_rate = 0.0
            partner_cgst_amount = 0
            partner_sgst_rate = 0.0
            partner_sgst_amount = 0            

        elif len(partner_charges_tax) == 2:
            partner_igst_rate = 0.0
            partner_igst_amount = 0
            partner_cgst_rate = partner_charges_gst/2
            partner_sgst_rate = partner_charges_gst/2 
            partner_sgst_amount = partner_charges_tax[0]
            partner_cgst_amount = partner_charges_tax[1]     

        with transaction.atomic():
            cursor.execute(f" SELECT no_of_transactions, cummulative_payment_amount FROM portal_tbl_bbps_transaction_mst WHERE biller_ref_id_id = {biller_ref_id} AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL') ORDER BY id DESC LIMIT 1 ")
            FindLastTranctions = tuple(cursor)
            if FindLastTranctions:                
                no_of_transactions = FindLastTranctions[0][0]
                cummulative_payment_amount = FindLastTranctions[0][1]
            else:
                no_of_transactions = 0
                cummulative_payment_amount = 0

            no_of_transactions = no_of_transactions + 1 
            cummulative_payment_amount = round_to(float(cummulative_payment_amount) + float(payment_amount))
            FindLastTranctions = tbl_bbps_transaction_mst.objects.get(id=ID)
            FindLastTranctions.cummulative_payment_amount = cummulative_payment_amount
            FindLastTranctions.no_of_transactions = no_of_transactions
            FindLastTranctions.save()

            data_dict = {
                'header_ref_id_id' : ID, 
                'bill_amount' : bill_amount, 
                'payment_amount' : payment_amount, 
                'interchange_switch_fee' : interchange_switch_fee, 
                'interchange_switch_sgst_rate' : interchange_switch_sgst_rate,
                'interchange_switch_sgst_amount' : interchange_switch_sgst_amount,
                'interchange_switch_igst_rate' : interchange_switch_igst_rate,
                'interchange_switch_igst_amount' : interchange_switch_igst_amount,
                'interchange_switch_cgst_amount' : interchange_switch_cgst_amount,
                'interchange_switch_cgst_rate' : interchange_switch_cgst_rate,

                'biller_bank_fee' : biller_bank_fee,
                'biller_bank_sgst_rate' : biller_bank_sgst_rate,
                'biller_bank_sgst_amount' : biller_bank_sgst_amount,
                'biller_bank_igst_rate' : biller_bank_igst_rate,
                'biller_bank_igst_amount' : biller_bank_igst_amount,
                'biller_bank_cgst_rate' : biller_bank_cgst_rate,
                'biller_bank_cgst_amount' : biller_bank_cgst_amount,

                'platform_fee' : platform_fee,
                'platform_sgst_rate' : platform_sgst_rate,
                'platform_sgst_amount' : platform_sgst_amount,
                'platform_igst_rate' : platform_igst_rate,
                'platform_igst_amount' : platform_igst_amount,
                'platform_cgst_rate' : platform_cgst_rate,
                'platform_cgst_amount' : platform_cgst_amount,

                'partner_fee' : partner_charges,
                'partner_sgst_rate' : partner_sgst_rate,
                'partner_sgst_amount' : partner_sgst_amount,
                'partner_igst_rate' : partner_igst_rate, 
                'partner_igst_amount' : partner_igst_amount,
                'partner_cgst_rate' : partner_cgst_rate,
                'partner_cgst_amount' : partner_cgst_amount,
                'created_date_time' : datetime.now(pytz.timezone(TIME_ZONE))
            }
            date_month_condition = f"(EXTRACT('month' from  transaction_date), EXTRACT('year' from transaction_date))=({datetime.now().month},{datetime.now().year})"
            # Biller Receivable Book
            cursor.execute(f" SELECT closing_amount FROM portal_tbl_biller_receivable_ledger_mst WHERE biller_ref_id_id = '{biller_ref_id}' AND {date_month_condition} ORDER BY id DESC LIMIT 1 ")        
            billerReceivableClosing = tuple(cursor)

            if billerReceivableClosing:
                billerReceivableClosing = billerReceivableClosing[0][0]            
            else:
                billerReceivableClosing = 0
            billerReceivableClosing = round_to(billerReceivableClosing)

            billerReceivableLedger = {
                "biller_ref_id_id": biller_ref_id,
                "transaction_type_ref_id_id": ID,
                "transaction_number": acknowledgementId,
                "amount": payment_amount,
                "opening_amount": billerReceivableClosing,
                "closing_amount": round_to(float(billerReceivableClosing) + float(payment_amount)),
                "created_by": 0,
            }

            # Biller Payable Book
            cursor.execute(f" SELECT closing_amount FROM portal_tbl_biller_payable_ledger_mst WHERE biller_ref_id_id = '{biller_ref_id}' AND {date_month_condition} ORDER BY id DESC LIMIT 1 ")        
            billerPayableClosing = tuple(cursor)
            if billerPayableClosing:
                billerPayableClosing = billerPayableClosing[0][0]
            else:
                billerPayableClosing = 0
            billerPayableClosing =  round_to(billerPayableClosing)

            billerPayableAmount = round_to(platform_fee + platform_sgst_amount + platform_igst_amount + platform_cgst_amount)
            billerPayableLedger = {
                "biller_ref_id_id": biller_ref_id,
                "aggregator_ref_id_id": admin_company_ref_id,
                "transaction_type_ref_id_id": ID,
                "transaction_number": acknowledgementId,
                "amount": billerPayableAmount,
                "opening_amount": billerPayableClosing,
                "closing_amount": round_to(float(billerPayableClosing) + float(billerPayableAmount)),
                "created_by": 0
            }

            # Platform Receivable Book
            cursor.execute(f" SELECT closing_amount FROM portal_tbl_platform_receivable_ledger_mst WHERE admin_company_ref_id_id = '{admin_company_ref_id}' AND biller_ref_id_id = '{biller_ref_id}'  AND {date_month_condition} ORDER BY id DESC LIMIT 1 ")        
            platformReceivableClosing = tuple(cursor)
            if platformReceivableClosing:
                platformReceivableClosing = platformReceivableClosing[0][0]
            else:
                platformReceivableClosing = 0
            platformReceivableClosing =  round_to(platformReceivableClosing)        

            platformReceivableLedger = {
                "admin_company_ref_id_id": admin_company_ref_id,
                "biller_ref_id_id": biller_ref_id,
                "transaction_type_ref_id_id": ID,
                "transaction_number": acknowledgementId,
                "amount": billerPayableAmount,
                "opening_amount": platformReceivableClosing,
                "closing_amount": round_to(float(platformReceivableClosing) + float(billerPayableAmount)),
                "created_by": 0,
            }        

            # platform Payable to OU Book
            cursor.execute(f" SELECT closing_amount FROM portal_tbl_platform_payable_ledger_mst WHERE admin_company_ref_id_id = '{admin_company_ref_id}' AND bbpou_bank_ref_id_id = '{bbpou_bank_ref_id}' AND {date_month_condition} ORDER BY id DESC LIMIT 1 ")        
            platformPayableOUClosing = tuple(cursor)
            
            if platformPayableOUClosing:
                platformPayableOUClosing = platformPayableOUClosing[0][0]
            else:
                platformPayableOUClosing = 0

            platformPayableOUClosing =  round_to(platformPayableOUClosing)

            bbpouPayableAmount = round_to(biller_bank_fee + biller_bank_sgst_amount + biller_bank_igst_amount + biller_bank_cgst_amount)
            platformPayableOULedger = {
                "admin_company_ref_id_id": admin_company_ref_id,
                "bbpou_bank_ref_id_id": bbpou_bank_ref_id,
                "partner_ref_id_id": None,
                "transaction_type_ref_id_id": ID,
                "transaction_number": acknowledgementId,
                "amount": bbpouPayableAmount,
                "opening_amount": platformPayableOUClosing,
                "closing_amount": round_to(float(platformPayableOUClosing) + float(bbpouPayableAmount)),
                "created_by": 0
            }              

            #platform Payable to Partner Book

            if Partner_biller_check:
                cursor.execute(f" SELECT closing_amount FROM portal_tbl_platform_payable_ledger_mst WHERE admin_company_ref_id_id = '{admin_company_ref_id}' AND partner_ref_id_id = '{Partner_biller_check}' AND {date_month_condition} ORDER BY id DESC LIMIT 1 ")        
                platformPayablePartClosing = tuple(cursor)
                if platformPayablePartClosing:
                    platformPayablePartClosing = platformPayablePartClosing[0][0]
                else:
                    platformPayablePartClosing = 0
                platformPayablePartClosing =  round_to(platformPayablePartClosing)

                partnerPayableAmount = round_to(partner_charges + partner_sgst_amount + partner_igst_amount + partner_cgst_amount)    

                platformPayablePartLedger = {
                    "admin_company_ref_id_id": admin_company_ref_id,
                    "bbpou_bank_ref_id_id": None,
                    "partner_ref_id_id": Partner_biller_check,
                    "transaction_type_ref_id_id": ID,
                    "transaction_number": acknowledgementId,
                    "amount": partnerPayableAmount,
                    "opening_amount": platformPayablePartClosing,
                    "closing_amount": round_to(float(platformPayablePartClosing) + float(partnerPayableAmount)),
                    "created_by": 0
                } 

            tbl_biller_receivable_ledger_mst.objects.create(**billerReceivableLedger)
            tbl_biller_payable_ledger_mst.objects.create(**billerPayableLedger)
            tbl_platform_receivable_ledger_mst.objects.create(**platformReceivableLedger)
            tbl_platform_payable_ledger_mst.objects.create(**platformPayableOULedger)
            if Partner_biller_check:
                tbl_platform_payable_ledger_mst.objects.create(**platformPayablePartLedger)
            tbl_bbps_transaction_details.objects.create(**data_dict)
    except:
        raise

class GetBillerDataApi(APIView):
    def post(self, request, *args, **kwargs):
        try:
            cursor = connection.cursor()            
            tableData = json.loads(request.body)
            biller_id = tableData.pop('biller_id')
            current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            cursor.execute(f"""SELECT id FROM portal_tbl_company_mst WHERE npci_assigned_id = '{biller_id}' AND is_active = 'Y' AND is_deleted = 'N' LIMIT 1 """)
            comp_ref_id = tuple(cursor)[0][0]     

            cursor.execute(f"""SELECT source_name, id FROM public.portal_tbl_source_mst WHERE company_ref_id_id = '{comp_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 """)
            source_mst = tuple(cursor)[0]
            table_name = source_mst[0]
            source_ref_id = source_mst[1]
            table_name = f'tbl_{table_name}_details'

            cursor.execute(f""" SELECT field_name FROM public.portal_tbl_source_details WHERE is_primary_field = 'True' AND header_ref_id_id = '{source_ref_id}' LIMIT 1 """)
            is_primary_field = tuple(cursor)[0][0]
            is_primary_value = tableData.get(is_primary_field)

            cursor.execute(f""" SELECT COUNT(*) FROM {table_name} """)
            details_count = tuple(cursor)
            if details_count:
                details_count = details_count[0][0]
                details_count += 1
            else:
                details_count = 1

            tableData['details_id'] = details_count
            tableData['updated_date'] = current_date
            tableData['b_payment_amount'] = 0
            tableData['created_date'] = current_date

            values = tuple(tableData.values())
            keys = tuple(tableData.keys())
            keys = str(tuple(keys)).replace("'", '"')
            values = str(tuple(values))

            cursor.execute(f"INSERT INTO {table_name} {keys} VALUES {values}")
            return Response({'status':"SUCCESS", "acknowledgementId":is_primary_value})      
        except:
            return JsonResponse({'status':'FAILURE'}, status=status.HTTP_401_UNAUTHORIZED)
        finally:
            cursor.close()
        
class FetchBillApiViewSet(APIView):
    def get(self, request, *args, **kwargs): 
        try:
            cursor = connection.cursor()
            customerParams = request.data.get('customerParams')
            npci_biller_id = request.data.get('billerId')

            for count, value in enumerate(customerParams):                
                bill_number_key = value['name']
                bill_number = value['value']

            todays_date = datetime.now()
            todays_date = datetime.strftime(todays_date, '%Y-%m-%d')

            cursor.execute(f""" SELECT id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND npci_assigned_id = '{npci_biller_id}' LIMIT 1 """)
            biller_query_data = tuple(cursor)
            if biller_query_data:
                biller_id = biller_query_data[0][0]
            else:
                cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
                error_code = tuple(cursor)[0]
                return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

            cursor.execute(f""" SELECT partner_company_ref_id_id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = '{biller_id}' LIMIT 1 """)
            partnerCompanyReference = tuple(cursor)[0][0]
            
            cursor.execute(f""" SELECT billing_mode_ref_id_id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {biller_id} LIMIT 1 """)
            billing_mode = tuple(cursor)[0][0]
            cursor.execute(f""" SELECT master_key FROM public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {billing_mode} LIMIT 1 """)
            billing_mode = tuple(cursor)[0][0]

            try:
                cursor.execute(f""" SELECT biller_category_ref_id_id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {biller_id} LIMIT 1 """)
                biller_cat = tuple(cursor)[0][0]
            except:
                biller_cat = 1

            try:
                cursor.execute(""" SELECT id FROM public.portal_tbl_country_currency_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND currency_code = 'INR' LIMIT 1 """)
                currency_id = tuple(cursor)[0][0]
            except:
                currency_id = 1

            try:
                cursor.execute(f""" SELECT bbpou_bank_ref_id_id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {biller_id} LIMIT 1 """)
                bbpoubank_id = tuple(cursor)[0][0]
            except:
                bbpoubank_id = 1

            try:
                cursor.execute(f""" SELECT entity_ref_id FROM public.portal_tbl_entity_relationship_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND company_ref_id_id = {biller_id} AND entity_ref_id != {biller_id} LIMIT 1 """)
                admin_ref_id = tuple(cursor)[0][0]
            except:
                admin_ref_id = 1

            if billing_mode == 'Online':
                raise                 
            else:
                cursor.execute(f""" SELECT id, source_name FROM public.portal_tbl_source_mst WHERE company_ref_id_id = '{biller_id}' AND is_deleted = 'N' AND is_active = 'Y' AND from_date <= '{todays_date}' AND to_date >= '{todays_date}' LIMIT 1 """)
                source_ref_id = tuple(cursor)
                biller_table_name = f'tbl_{source_ref_id[0][1]}_details'
                source_ref_id = source_ref_id[0][0]
                cursor.execute(f""" SELECT npci_parameter_ref_id_id, field_name FROM public.portal_tbl_source_details WHERE header_ref_id_id = '{source_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' """ )
                source_details = tuple(cursor)

                billerTableWhereCondition = ''
                customerParametersFields = ''
                for count, value in enumerate(customerParams):
                    if count!=0:
                        billerTableWhereCondition = f'{billerTableWhereCondition} AND '
                        customerParametersFields = f'{customerParametersFields},'
                    cursor.execute(f""" SELECT id, field_name FROM portal_tbl_source_details WHERE "header_ref_id_id" = '{source_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND actual_field_name = '{value['name']}' """ )
                    fieldNames = tuple(cursor)
                    customerParametersFields = f'{customerParametersFields}{fieldNames[0][0]}'
                    fieldNames = f'"{fieldNames[0][1]}"'
                    billNumberValue = f"'{value['value']}'"
                    billerTableWhereCondition = f'{billerTableWhereCondition}{fieldNames} = {billNumberValue}'

                cursor.execute(f""" SELECT field_name FROM public.portal_tbl_source_details WHERE header_ref_id_id = '{source_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND actual_field_name = '{bill_number_key}' """ )
                actual_field_name = tuple(cursor)
                if actual_field_name:
                    bill_number_key = actual_field_name[0][0]

                else:
                    cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
                    error_code = tuple(cursor)[0]
                    return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})                
                
                Additional_Parameter = []
                for source_indx in source_details:
                    cursor.execute(f""" SELECT master_key FROM public.portal_tbl_master WHERE id = '{source_indx[0]}' AND is_deleted = 'N' AND is_active = 'Y'  LIMIT 1 """ )                
                    npci_column_names = (tuple(cursor)[0][0])
                    if(npci_column_names == "Amount Due"):
                        Amount_Due = source_indx[1]
                    elif(npci_column_names == "Bill Number"):
                        Bill_Number = source_indx[1]
                    elif(npci_column_names == "Bill Period"):
                        Bill_Period = source_indx[1]
                    elif(npci_column_names == "Due Date"):
                        Due_Date = source_indx[1]
                    elif(npci_column_names == "Bill Date"):
                        Bill_Date = source_indx[1]
                    elif(npci_column_names == "Customer Name"):
                        Customer_Name = source_indx[1]
                    elif(npci_column_names == "Additional Parameter"):
                        Additional_Parameter.append(source_indx[1])

                try:
                    cursor.execute(f""" SELECT COUNT(*) FROM public.{biller_table_name} """ ) 
                    biller_table_count = tuple(cursor)
                    if biller_table_count[0][0]<=0:
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})
                    else:
                        pass
                except:
                    cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
                    error_code = tuple(cursor)[0]
                    return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

                cursor.execute(f""" SELECT * FROM public.{biller_table_name} WHERE {bill_number_key} = '{bill_number}' AND {Amount_Due} > b_payment_amount LIMIT 1 """ )
                source_data = dictfetchall(cursor)

                if source_data:
                    amtDue_value = source_data[0].get(Amount_Due)
                    billNumber_value = source_data[0].get(Bill_Number)
                    billPeriod_value = source_data[0].get(Bill_Period) 
                    billDate_value = source_data[0].get(Bill_Date) 
                    dueDate_value = source_data[0].get(Due_Date)
                    customerName_Value = source_data[0].get(Customer_Name)
                    b_payment_amount_value = source_data[0].get('b_payment_amount')
                    if b_payment_amount_value == None:
                        b_payment_amount_value = 0

                    # Create Additional Parameters
                    additionalInfo = []
                    if len(Additional_Parameter)>0:
                        for adPrmtr in Additional_Parameter:
                            if source_data[0].get(adPrmtr) == None:
                                pass
                            else:
                                cursor.execute(f""" SELECT actual_field_name FROM portal_tbl_source_details WHERE header_ref_id_id = '{source_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND field_name = '{adPrmtr}' """ )
                                fieldNames = tuple(cursor)
                                fieldNames = fieldNames[0][0]
                                addParameters = {
                                    "name": fieldNames,
                                    "value": source_data[0].get(adPrmtr)
                                }
                                additionalInfo.append(addParameters)
                    else:
                        additionalInfo = []
                else:
                    cursor.execute(f""" SELECT {Amount_Due} FROM public.{biller_table_name} WHERE {bill_number_key} = '{bill_number}' AND {Amount_Due} = b_payment_amount LIMIT 1 """ )
                    checkBBPSChannel = tuple(cursor)
                    if checkBBPSChannel:
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM4' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})
                    else:
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

                cursor.execute(f"""SELECT starting_no FROM public.portal_tbl_document_sequencing_mst WHERE master_key = 'Fatch Bill' AND is_deleted='N' """)
                starting_no = tuple(cursor)[0][0]

                cursor.execute(f""" SELECT fatch_increment_no FROM public.portal_tbl_bbps_transaction_mst ORDER BY id DESC LIMIT 1 """)
                fatch_increment_no = tuple(cursor)
                if fatch_increment_no:
                    fatch_increment_no = fatch_increment_no[0][0]   
                    fatch_increment_no = fatch_increment_no + 1
                else:
                    fatch_increment_no = 1             
                
                fatch_increment_length = str(fatch_increment_no)
                fatch_increment_length = 8 - len(fatch_increment_length)
                fatch_increment_length = int(fatch_increment_length)
                starting_no = str(starting_no)
                for i in range(fatch_increment_length):
                    starting_no = starting_no + '0'
                starting_no = starting_no + str(fatch_increment_no)

                res_data = { 
                            "status": "SUCCESS",
                            "errorCode" : "000",
                            "customerName": customerName_Value,
                            "amountDue": amtDue_value,
                            "billDate": billDate_value,
                            "dueDate": dueDate_value,
                            "billNumber": billNumber_value,
                            "billPeriod": billPeriod_value
                            }

                fiscal_data = fiscal_year_func(date.today())

                data_dict = {
                            'biller_category_ref_id':biller_cat,
                            'biller_ref_id_id':biller_id,
                            'partner_company_ref_id_id':partnerCompanyReference,
                            'admin_company_ref_id_id':admin_ref_id,
                            'bbpou_bank_ref_id_id':bbpoubank_id,
                            'currency_ref_id_id':currency_id, 

                            'bill_number': billNumber_value,
                            'bill_amount': amtDue_value,
                            'customer_name': customerName_Value,                            
                            'bill_date': billDate_value, 
                            'due_date': dueDate_value, 
                            'bill_period': billPeriod_value, 
                            'customer_param': customerParametersFields,

                            'payment_status':'Unpaid',                            
                            'fatch_increment_no': fatch_increment_no,
                            'fetch_unique_id':starting_no, 
                            'fiscal_year': fiscal_data[1], 
                            'period': fiscal_data[0],

                            'transaction_date':date.today(),
                            'created_date_time':datetime.now(pytz.timezone(TIME_ZONE)),
                            'cummulative_payment_amount': 0, 
                            'no_of_transactions': 0, 
                            'remitted_amount': 0,

                            'mobile_number':None,                             
                            'response_code_ref_id': None,
                            'utr_number':None,
                            'payment_amount': None,
                            'payment_channel': None,
                            'payment_date': None,
                            'transaction_mode': None,
                            }

                with transaction.atomic():               
                    tbl_bbps_transaction_mst.objects.create(**data_dict)
                    res_data.update({'acknowledgementId': starting_no})
                    res_data.update({'additionalInfo': additionalInfo})                    
                    return Response(res_data)
        except:
            cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM1' LIMIT 1 """)
            error_code = tuple(cursor)[0]
            return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})
        finally:
            cursor.close()

class PayBillResponseApiViewSet(APIView):
    def post(self, request, *args, **kwargs):
        try:
            # con = engine.connect()
            cursor = connection.cursor()

            npci_biller_id = request.data.get('billerId')
            utr_number = request.data.get('paymentrefno')
            payment_mode_ref_id = request.data.get('payment_mode_ref_id')
            payment_channel = request.data.get('payment_channel')
            payment_amount = request.data.get('amount')
            record_id = request.data.get('acknowledgementId')
            transaction_type = request.data.get('transaction_type')

            customerParams = request.data.get('customerParams')
            for count, value in enumerate(customerParams):                
                bill_number_key = value['name']
                customer_param = value['value']

            if transaction_type == None:
                transaction_type = 'BOTH'

            payment_amount = float(payment_amount)
            if payment_amount<=0:
                raise
            
            todays_date = datetime.now()
            todays_date = datetime.strftime(todays_date, '%Y-%m-%d')     

            cursor.execute(f""" SELECT biller_ref_id_id, bill_number,id FROM public.portal_tbl_bbps_transaction_mst WHERE fetch_unique_id = '{record_id}' LIMIT 1 """)
            biller_id_ = tuple(cursor)


            biller_id = tuple(biller_id_)[0][0]
            bill_number = tuple(biller_id_)[0][1]
            bbpsTransactionReference = tuple(biller_id_)[0][2]

            cursor.execute(f""" SELECT payment_status FROM public.portal_tbl_bbps_transaction_mst WHERE fetch_unique_id = '{record_id}' AND (payment_status = 'Paid' OR payment_status = 'SUCCESSFUL')  LIMIT 1 """)
            payment_status = tuple(cursor)
            if payment_status:
                cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM6' LIMIT 1 """)
                error_code = tuple(cursor)[0]
                return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})
            
            cursor.execute(f""" SELECT id FROM public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND npci_assigned_id = '{npci_biller_id}' LIMIT 1 """)
            biller_query_data = tuple(cursor)
            biller_query_data = biller_query_data[0][0]
            if (biller_id == biller_query_data):
                pass
            else:
                raise

            cursor.execute(f""" SELECT id FROM public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND master_key = '{payment_mode_ref_id}' AND master_type = 'Payment Mode' LIMIT 1 """)
            payment_mode_ref_id = tuple(cursor)
            if payment_mode_ref_id:
                payment_mode_ref_id = payment_mode_ref_id[0][0]
            else:
                payment_mode_ref_id = None           

            cursor.execute(f""" SELECT id FROM public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND master_key = '{transaction_type}' AND master_type = 'Transaction Mode' LIMIT 1 """)
            transaction_mode_ref_id = tuple(cursor)
            transaction_mode_ref_id = transaction_mode_ref_id[0][0]

            cursor.execute(f""" SELECT billing_mode_ref_id_id from public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {biller_id} LIMIT 1 """)
            billing_mode = tuple(cursor)[0][0]
            cursor.execute(f""" SELECT master_key from public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {billing_mode} LIMIT 1 """)
            billing_mode = tuple(cursor)[0][0]

            if billing_mode == 'Online':
                raise
            else:
                cursor.execute(f""" SELECT source_name, id FROM public.portal_tbl_source_mst WHERE company_ref_id_id = '{biller_id}' AND is_deleted = 'N' AND is_active = 'Y' AND from_date <= '{todays_date}' AND to_date >= '{todays_date}' LIMIT 1 """)
                source_mst = tuple(cursor)
                source_ref_id = source_mst[0][1]
                table_name = source_mst[0][0]
                table_name = f'tbl_{table_name}_details'

                cursor.execute(f""" SELECT field_name FROM public.portal_tbl_source_details WHERE header_ref_id_id = '{source_ref_id}' AND is_deleted = 'N' AND is_active = 'Y' AND actual_field_name = '{bill_number_key}' """ )
                actual_field_name = tuple(cursor)
                if actual_field_name:
                    Customer_Param_Field = actual_field_name[0][0]
                else:
                    raise

                cursor.execute(f""" SELECT npci_parameter_ref_id_id, field_name FROM portal_tbl_source_details WHERE is_deleted = 'N' AND is_active = 'Y' AND header_ref_id_id = '{source_ref_id}' """ )                
                source_details = tuple(cursor)
                for sourceIndex in source_details:
                    cursor.execute(f""" SELECT master_key FROM public.portal_tbl_master WHERE id = '{sourceIndex[0]}' AND is_deleted = 'N' AND is_active = 'Y' LIMIT 1 """ )                
                    npci_column_names = (tuple(cursor)[0][0])
                    if(npci_column_names == "Amount Due"):
                        Amount_Due_Field = sourceIndex[1]

                    elif(npci_column_names == "Bill Number"):
                        Invoice_Number_Field = sourceIndex[1]

                dynamicTblWhere = f"{Customer_Param_Field} = '{customer_param}' AND {Invoice_Number_Field} = '{bill_number}'"
                cursor.execute(f""" SELECT {Amount_Due_Field} FROM public.{table_name} WHERE {dynamicTblWhere}  LIMIT 1 """)
                balance_amount = tuple(cursor)

                if balance_amount:
                    balance_amount = balance_amount[0][0]
                else:
                    raise                

                cursor.execute(f""" SELECT b_payment_amount FROM public.{table_name} WHERE {dynamicTblWhere} LIMIT 1 """)
                b_payment_amount = tuple(cursor)[0][0]
                if b_payment_amount == None:
                    b_payment_amount = 0

                balance_amount = float(balance_amount)
                b_payment_amount = float(b_payment_amount)
                payment_amount = float(payment_amount)

                cursor.execute(f""" SELECT bill_amount_option_ref_id_id from public.portal_tbl_company_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {biller_id} LIMIT 1 """)
                Bill_Amount_Type = tuple(cursor)[0][0]
                cursor.execute(f""" SELECT master_key from public.portal_tbl_master WHERE is_deleted = 'N' AND is_active = 'Y' AND id = {Bill_Amount_Type} LIMIT 1 """)
                Bill_Amount_Type = tuple(cursor)[0][0]

                if Bill_Amount_Type == 'EXACT':
                    if (b_payment_amount == payment_amount):
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM6' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

                    if ((b_payment_amount + payment_amount) == balance_amount):
                        finalBBPS_Update = {'utr_number': utr_number,
                                                    'payment_mode_ref_id_id': payment_mode_ref_id,
                                                    'payment_channel': payment_channel,
                                                    'payment_amount': payment_amount,
                                                    'updated_date_time': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_date': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_status': 'Paid',
                                                    'transaction_mode_ref_id_id': transaction_mode_ref_id,
                                                    'transaction_mode': transaction_type
                                                    }
                        with transaction.atomic():
                            tbl_bbps_transaction_mst.objects.filter(bill_number = bill_number, fetch_unique_id = record_id).update(**finalBBPS_Update)
                            commissionCalculation(bbpsTransactionReference, cursor)
                            cursor.execute(f""" UPDATE public.{table_name} SET b_payment_amount = {b_payment_amount + payment_amount} WHERE {dynamicTblWhere} """)
                            return Response({'status':"SUCCESS","errorCode": "000", "acknowledgementId":record_id})
                    else:
                        raise                        

                elif Bill_Amount_Type == 'EXACT_UP':
                    if (b_payment_amount != 0):
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM6' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

                    if ((b_payment_amount + payment_amount) >= balance_amount):
                        finalBBPS_Update = {'utr_number': utr_number,
                                                    'payment_mode_ref_id_id': payment_mode_ref_id,
                                                    'payment_channel': payment_channel,
                                                    'payment_amount': payment_amount,
                                                    'updated_date_time': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_date': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_status': 'Paid',
                                                    'transaction_mode_ref_id_id': transaction_mode_ref_id,
                                                    'transaction_mode': transaction_type
                                                    }
                        with transaction.atomic():
                            tbl_bbps_transaction_mst.objects.filter(bill_number = bill_number, fetch_unique_id = record_id).update(**finalBBPS_Update)
                            commissionCalculation(bbpsTransactionReference, cursor)
                            cursor.execute(f""" UPDATE public.{table_name} SET b_payment_amount = {b_payment_amount + payment_amount} WHERE {dynamicTblWhere} """) 
                            return Response({'status':"SUCCESS", "errorCode": "000", "acknowledgementId":record_id})
                    else:
                        raise

                elif Bill_Amount_Type == 'EXACT_DOWN':
                    if (b_payment_amount != 0):
                        cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM6' LIMIT 1 """)
                        error_code = tuple(cursor)[0]
                        return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})

                    if ((b_payment_amount + payment_amount) <= balance_amount):
                        finalBBPS_Update = {'utr_number': utr_number,
                                                    'payment_mode_ref_id_id': payment_mode_ref_id,
                                                    'payment_channel': payment_channel,
                                                    'payment_amount': payment_amount,
                                                    'updated_date_time': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_date': datetime.now(pytz.timezone(TIME_ZONE)),
                                                    'payment_status': 'Paid',
                                                    'transaction_mode_ref_id_id': transaction_mode_ref_id,
                                                    'transaction_mode': transaction_type
                                                    }
                        with transaction.atomic():
                            tbl_bbps_transaction_mst.objects.filter(bill_number = bill_number, fetch_unique_id = record_id).update(**finalBBPS_Update)
                            commissionCalculation(bbpsTransactionReference, cursor)
                            cursor.execute(f""" UPDATE public.{table_name} SET b_payment_amount = {b_payment_amount + payment_amount} WHERE {dynamicTblWhere} """) 
                            return Response({'status':"SUCCESS", "errorCode": "000", "acknowledgementId":record_id})
                    else:
                        raise

                elif Bill_Amount_Type == 'ANY':
                    finalBBPS_Update = {'utr_number': utr_number,
                                                'payment_mode_ref_id_id': payment_mode_ref_id,
                                                'payment_channel': payment_channel,
                                                'payment_amount': payment_amount,
                                                'updated_date_time': datetime.now(pytz.timezone(TIME_ZONE)),
                                                'payment_date': datetime.now(pytz.timezone(TIME_ZONE)),
                                                'payment_status': 'Paid',
                                                'transaction_mode_ref_id_id': transaction_mode_ref_id,
                                                'transaction_mode': transaction_type
                                                }
                    with transaction.atomic():
                        tbl_bbps_transaction_mst.objects.filter(bill_number = bill_number, fetch_unique_id = record_id).update(**finalBBPS_Update)
                        commissionCalculation(bbpsTransactionReference, cursor)
                        cursor.execute(f""" UPDATE public.{table_name} SET b_payment_amount = {b_payment_amount + payment_amount} WHERE {dynamicTblWhere} """) 
                        return Response({'status':"SUCCESS", "errorCode": "000", "acknowledgementId":record_id})
            
        except:
            cursor.execute(f""" SELECT npci_error_code, error_description, npci_error_category FROM public.portal_tbl_npci_error_code_mst WHERE rhythm_error_code = 'RHYTHM5' LIMIT 1 """)
            error_code = tuple(cursor)[0]
            return Response({"status": "FAILURE", 'errorCode':f'{error_code[0]}','errorMessage':f'{error_code[1]}'})
        finally:
            cursor.close()

class OnboardTypeforinvoicefileViewSet(APIView):
    def get(self,request):
        role1=0
        if request.method == 'GET':
            biller_id = request.GET['BILLER_ID']
            cursor = connection.cursor()
            cursor.execute(f""" SELECT onboarding_ref_id_id FROM portal_tbl_company_mst WHERE id = {biller_id} AND is_deleted='N' AND is_active='Y' """)
            onboardtype = dictfetchall(cursor)
            onboarding_ref_id=onboardtype[0]['onboarding_ref_id_id']
            cursor.execute(f""" SELECT master_key FROM portal_tbl_master WHERE id = {onboarding_ref_id}  AND is_deleted='N' AND is_active='Y' """)        
            onboardtype1 = dictfetchall(cursor)            
            
            master_key = onboardtype1[0].get('master_key')
            if master_key == "Direct":
                cursor.execute("SELECT entity_ref_id_id FROM portal_tbl_entity_relationship_mst where company_ref_id_id ='" + biller_id +"'  and entity_ref_id_id != '" + biller_id +  "' and is_deleted='N' ")
                entityref = dictfetchall(cursor)
                entity_ref_id = entityref[0]['entity_ref_id_id']
                cursor.execute(f"""SELECT company_name FROM portal_tbl_company_mst WHERE id = {entity_ref_id}  AND is_deleted='N' AND is_active='Y' """)
                compname = dictfetchall(cursor)  
                cursor.close()                
                return Response(compname,status=status.HTTP_200_OK)  
            elif master_key == "Indirect":
                cursor.execute(f"""SELECT bbpou_bank_ref_id_id FROM portal_tbl_company_mst WHERE id = {biller_id}  AND is_deleted='N' AND is_active='Y' """)
                bbpourefid = dictfetchall(cursor) 
                bbpou_bank_ref_id = bbpourefid[0]['bbpou_bank_ref_id_id']
                cursor.execute(f"""SELECT bbpou_bank_name as company_name FROM portal_tbl_bbpou_bank_mst WHERE id = {bbpou_bank_ref_id}  AND is_deleted='N' AND is_active='Y' """)
                bbpouname = dictfetchall(cursor)
                cursor.close()
                return Response(bbpouname,status=status.HTTP_200_OK)

class tbl_partner_charges_mst_view(viewsets.ModelViewSet):
    queryset = tbl_partner_charges_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')
    serializer_class = tbl_partner_charges_mst_serializer

    def list(self, request):
        if request.GET["role"] =='Aggregator Partner' or request.GET["role"] =='Aggregator Partner User' :
            partner_obj = tbl_partner_charges_mst.objects.filter(partner_company_ref_id = request.GET["cid"]).order_by('-id')
            serializer = tbl_partner_charges_mst_serializer(partner_obj, many=True)
            return Response(serializer.data)

        if  request.GET["role"] =='Aggregator' or  request.GET["role"]=='Aggregator User':
            partner_obj = tbl_partner_charges_mst.objects.filter(admin_company_ref_id = request.GET["cid"]).order_by('-id')
            serializer = tbl_partner_charges_mst_serializer(partner_obj, many=True)
            return Response(serializer.data)

class UploadFileKYC(APIView):
    def post(self, request):            
        res = 'Success'
        return Response(res, status=status.HTTP_201_CREATED)

class FetchUplodedFile(APIView):
    def post(self, request):
        kyc_path=request.data.get('kyc_path')      
        return Response(kyc_path, status=status.HTTP_201_CREATED)

class AddBillerView(viewsets.ModelViewSet):
    serializer_class = AddBillerSerializer
    queryset = tbl_company_mst.objects.filter(is_deleted='N',is_active='Y').order_by('-id')

    def list(self, request):
        login_data = LoginMasterSerializer.get(request.user)
        if 'tenant_type' in self.request.GET:
            if request.GET["tenant_type"] == 'Aggregator' or request.GET["tenant_type"] == 'Biller-OU':
                role_data = tbl_role_mst.objects.get(role_name='Biller', is_deleted='N', is_active='Y')                
                company = tbl_company_mst.objects.filter(company_type_ref_id=role_data.id, is_deleted='N', is_active='Y').order_by('-id')
                serializer = self.get_serializer(company, many=True)
                return Response(serializer.data)
            
            if request.GET["tenant_type"] == 'Biller':
                company= tbl_company_mst.objects.filter(id=login_data.company_id_id, is_deleted='N', is_active='Y')
                serializer = self.get_serializer(company, many=True)
                return Response(serializer.data)
            
            if request.GET["tenant_type"] == 'Aggregator Partner':
                company= tbl_company_mst.objects.filter(partner_company_ref_id=login_data.company_id_id, is_deleted='N',is_active='Y')
                serializer = self.get_serializer(company, many=True)
                return Response(serializer.data)

    def create(self, request):
        login_data = LoginMasterSerializer.get(request.user)
        with transaction.atomic():
            validated_data = dict(request.data)
            validated_data['company_id'] = login_data.company_id_id
            validated_data['entity_share_id'] = f"{validated_data['company_shortname']}_{login_data.company_id_id}"
            serializer = AddBillerSerializer(context={"request": request}, data=validated_data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":"Record Created Successfully"},status=status.HTTP_201_CREATED)
            else:
                return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None, partial=True):
        tenant_obj = tbl_company_mst.objects.get(id=pk)
        with transaction.atomic():
            validated_data = dict(request.data)
            serializer = AddBillerSerializer(tenant_obj, data=validated_data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":"Record Updated Successfully"},status=status.HTTP_201_CREATED)
            else:
                return Response({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors},status=status.HTTP_400_BAD_REQUEST)

class CommisionReportDataView(APIView):
    def post(self, request, *args, **kwargs):
            role_name = str(request.data.get('role_name'))
            from_date = request.data.get('from_date')
            to_date = request.data.get('to_date')
            biller_id = request.data.get('biller_ref_id')
            partner_id = request.data.get('partner_company_ref_id')

            if(role_name =='Biller-OU'):
                cursor = connection.cursor()
                cursor.execute("SELECT m.utr_number,m.fetch_unique_id,m.payment_amount,m.bill_amount,m.transaction_date,m.payment_date,d.biller_bank_fee,d.biller_bank_sgst_amount,d.biller_bank_igst_amount,d.biller_bank_cgst_amount FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN   public.portal_tbl_bbps_transaction_details AS d ON d.header_ref_id_id =m.id WHERE  m.payment_status='SUCCESSFUL' and  m.transaction_date>='" + from_date +  "' and  m.transaction_date<= '" + to_date + "'")
                commission = dictfetchall(cursor)
                cursor.close()
                return Response(commission,status=status.HTTP_200_OK)
            
            elif(role_name =='Aggregator'):
                cursor = connection.cursor()
                cursor.execute("SELECT m.utr_number,m.fetch_unique_id,m.payment_amount,m.bill_amount,m.transaction_date,m.payment_date,d.platform_fee,d.platform_sgst_amount,d.platform_igst_amount,d.platform_cgst_amount FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN   public.portal_tbl_bbps_transaction_details AS d ON d.header_ref_id_id =m.id WHERE  m.payment_status='SUCCESSFUL' and  m.transaction_date>='" + from_date +  "' and  m.transaction_date<= '" + to_date + "'")
                agg_commission = dictfetchall(cursor)
                cursor.close()
                return Response(agg_commission,status=status.HTTP_200_OK)
            
            elif(role_name == 'Biller'):
                cursor = connection.cursor()
                cursor.execute(f"""SELECT m.transaction_date,d.payment_amount ,d.bill_amount,round((d.payment_amount - ((((d.platform_fee+d.biller_bank_fee+d.interchange_switch_fee+d.partner_fee)/100)*18)+(d.platform_fee+d.biller_bank_fee+d.interchange_switch_fee+d.partner_fee))),2) As biller_commission_amount FROM portal_tbl_bbps_transaction_mst As m JOIN portal_tbl_bbps_transaction_details As d ON m.id = d.header_ref_id_id WHERE m.biller_ref_id_id = {biller_id} and m.transaction_date>='{from_date}'and  m.transaction_date<= '{to_date}' """)
                biller_commission = dictfetchall(cursor)
                cursor.close()
                return Response(biller_commission,status=status.HTTP_200_OK)
            
            elif(role_name == 'Aggregator Partner'):
                cursor = connection.cursor()
                cursor.execute(f"""SELECT m.utr_number,m.fetch_unique_id,m.payment_amount,m.bill_amount,m.transaction_date,m.payment_date,d.partner_fee,d.partner_sgst_amount,d.partner_igst_amount,d.partner_cgst_amount FROM public.portal_tbl_bbps_transaction_mst AS m INNER JOIN public.portal_tbl_bbps_transaction_details AS d ON d.header_ref_id_id =m.id WHERE  m.payment_status='SUCCESSFUL' and m.partner_company_ref_id_id = {partner_id}  and m.biller_ref_id_id = {biller_id}  and m.transaction_date>='{from_date}' and  m.transaction_date<= '{to_date }'""")
                partner_commission = dictfetchall(cursor)
                cursor.close()
                return Response(partner_commission,status=status.HTTP_200_OK)
       
