from rest_framework import status
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
import os
from django.core.files.storage import default_storage
import shutil
from django.db import connection
import pysftp
from BBPS_Biller import settings
from datetime import datetime

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


#Below function upload_file_data_view is only for Linux OS
class upload_file_data_view(APIView):
    def post(self, request):
        file = request.FILES['uploadedFile']
        company_id = request.data.get('company_id')
        fileformat = request.data.get('format').lower()
        fileformat = '.'+str(fileformat).lower()

        cursor = connection.cursor()
        cursor.execute(f"SELECT source_name FROM public.portal_tbl_source_mst WHERE is_deleted = 'N' AND is_active = 'Y' AND company_ref_id_id = {company_id} LIMIT 1 ")
        source_name = dictfetchall(cursor)
        source_name = source_name[0]['source_name']

        today_date = datetime.today()
        d1 = today_date.strftime("%d/%m/%Y")
        t1 = today_date.strftime("%H:%M:%S")
        today = ''.join(char for char in str(d1) if char.isalnum())
        todaytime = ''.join(char for char in str(t1) if char.isalnum())        
        cloud_name = request.data.get('cloud_name')
        to_upload = source_name + fileformat

        if(cloud_name=='SFTP'):
            cursor.execute("SELECT b.host_bucket_name, b.username, b.password, b.file_key_path, b.biller_floder_storage_path FROM public.portal_tbl_secure_server_config_mst as a join public.portal_tbl_secure_server_config_details as b on a.id = b.header_ref_id_id where a.is_deleted = 'N'")
            secure_server = dictfetchall(cursor)
            if secure_server != []:
                for i in secure_server:
                    ip_address = str(i['host_bucket_name'])
                    username = str(i['username'])
                    password = str(i['password'])
                    private_key_path = str(i['file_key_path'])
                    cloud_location_name = str(i['biller_floder_storage_path'])

            credential_path = settings.MEDIA_ROOT + private_key_path
            if os.path.exists(credential_path)==False:
                cursor.close()
                return Response('Credential Not Found', status=status.HTTP_201_CREATED)

            if os.path.exists(settings.MEDIA_ROOT + '/sftp')==False:
                parent_dir = settings.MEDIA_ROOT + '/sftp'
                os.mkdir(parent_dir)

            if os.path.exists(settings.MEDIA_ROOT + '/sftp/' + to_upload)==True:
                os.remove(settings.MEDIA_ROOT +'/sftp/'+ to_upload)
            
            default_storage.save(settings.MEDIA_ROOT +'/sftp/'+ to_upload,file)

            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None

            with pysftp.Connection(ip_address, username=username, password=password, private_key=credential_path, port=8288, cnopts=cnopts) as sftp:
                with sftp.cd(cloud_location_name): 
                    if sftp.exists:
                        local_filename = settings.MEDIA_ROOT + "/sftp/" + to_upload
                        sftp.put(local_filename, ('./sftp/'+to_upload))
                        sftp_backup = sftp.isdir('./sftp_backup')
                        if sftp_backup == True:
                            biller_folder = sftp.isdir(f'./sftp_backup/{source_name}')
                            if biller_folder == True:
                                pass
                            else:
                                sftp.mkdir(f'./sftp_backup/{source_name}')

                            biller_date_folder = sftp.isdir(f'./sftp_backup/{source_name}/{today}')
                            if biller_date_folder == True:
                                pass
                            else:
                                sftp.mkdir(f'./sftp_backup/{source_name}/{today}')

                            biller_date_folder_to_upload = sftp.isfile(f'./sftp_backup/{source_name}/{today}/{to_upload}')

                            if biller_date_folder_to_upload == False:
                                sftp.put(local_filename, (f'./sftp_backup/{source_name}/{today}/{to_upload}'))
                            else:
                                sftp.put(local_filename, (f'./sftp_backup/{source_name}/{today}/{todaytime}_{to_upload}'))

                        os.remove(settings.MEDIA_ROOT +'/sftp/'+ to_upload)

            cursor.close()
            return Response('Success', status=status.HTTP_201_CREATED)
        
class upload_credential_file_data_view(APIView):
    def post(self, request):
        file = request.FILES['uploadedFile']
        try:
            if os.path.exists(settings.MEDIA_ROOT + '/' + file.name)==False and os.path.exists(settings.MEDIA_ROOT + '/storage-credential-files/' + file.name)==False:
                file_name = default_storage.save(file.name, file)
                shutil.move('./uploads/'+ file_name , './uploads/storage-credential-files/')
                res = 'Success'
            else:
                res = 'Already Exists'
            return Response(res, status=status.HTTP_201_CREATED)
        except:
            res = 'Already Exists'
            return Response(res, status=status.HTTP_201_CREATED)
